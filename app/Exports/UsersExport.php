<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UsersExport implements FromCollection, WithHeadings, ShouldAutoSize
{
	protected $data;
	protected $headings;
	public function __construct($data,$headings)
    {
        $this->data = $data;
        $this->headings = $headings;
    }
    function collection()
    {
        dd($this->data[0]->DOJ);
        return $this->data;
    }
    function headings(): array
    {
        return $this->headings;
    }
}