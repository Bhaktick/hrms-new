<?php
namespace App\Repositories\Type;


use App\Repositories\Type\TypeInterface;
use App\Models\Type;
use Auth;
class TypeRepository implements TypeInterface
{
    public $type;
    function __construct(Type $type) {
	$this->type = $type;
    }
    public function get()
    {
        return $this->type->get();
    }
    public function find($id)
    {
        return $this->type->find($id);
    }


    // public function delete($id)
    // {
    //     return $this->Type->delete($id);
    // }

     public function where($filters = '')
    {
        $query = $this->type->query(); 
        if (isset($filters['where'])) {
          $query->where($filters['where']);
        }

        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }

        return $query->get();
    }
    public function create($data)
    {
        return $this->type->create($data);
    }

    public function update($data)
    {
        return $this->type->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->type->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }

}
?>