<?php
namespace App\Repositories\Setting;
use Illuminate\Support\ServiceProvider;
class SettingRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Setting\SettingInterface', 'App\Repositories\Setting\SettingRepository');
    }
}
?>