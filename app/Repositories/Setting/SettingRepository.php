<?php
namespace App\Repositories\Setting;


use App\Repositories\Setting\SettingInterface;
use App\Models\Setting;
use Auth;
use App\Models\Type;



class SettingRepository implements SettingInterface
{
    public $setting;


    function __construct(Setting $setting) {
	$this->setting = $setting;
    }
    public function get()
    {
        return $this->setting->get();
    }
    public function find($id)
    {
        return $this->setting->find($id);
    }


    // public function delete($id)
    // {
    //     return $this->setting->delete($id);
    // }

     public function where($filters = '')
    {
        $query = $this->setting->query(); 
        if (isset($filters['where'])) {
          $query->where($filters['where']);
        }

        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }

        return $query->orderBy('created_at', 'desc')->get();
    }
    public function create($data)
    {
        return $this->setting->create($data);
    }

    public function update($data)
    {
        return $this->setting->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->setting->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }

}
?>