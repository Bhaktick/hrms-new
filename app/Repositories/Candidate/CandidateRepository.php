<?php
namespace App\Repositories\Candidate;


use App\Repositories\Candidate\CandidateInterface;
use App\Models\Candidate;
use Auth;
use Illuminate\Support\Arr; // for array helpers

class CandidateRepository implements CandidateInterface
{
    public $candidate;
    function __construct(Candidate $candidate) {
    $this->candidate = $candidate;
    }
    public function get()
    {
       return $this->candidate->orderBy('created_at', 'desc')->get();
    }
    public function find($id)
    {
        return $this->candidate->find($id);
    }


    // public function delete($id)
    // {
    //     return $this->Role->delete($id);
    // }
    public function select($filters = '')
    {
        $select = array();

        $query = $this->candidate->query(); 
        if (isset($filters['select'])) {
            $i=0;
            foreach ($filters['select'] as $key => $value) {
            $select = Arr::add($select,$i,$value);
                // $query->select($value);
                $i++;
                

            }
        }
         return $query->select($select)->orderBy('created_at', 'desc')->get();

    }
    public function where($filters = '')
    {
        $query = $this->candidate->query(); 
        if (isset($filters['where'])) {
            // dd($filters['where']);
          $query->where($filters['where']);
        }
        if(isset($filters['notwhere']))
        {
                        // dd($filters['notwhere'][0],$filters['notwhere'][1],$filters['notwhere'][2]);
            $query->where($filters['notwhere'][0],$filters['notwhere'][1],$filters['notwhere'][2]);
        }
        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }
        return $query->orderBy('created_at', 'desc')->get();
    }
    public function create($data)
    {
        return $this->candidate->create($data);
    }

    public function update($data)
    {
        return $this->candidate->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->candidate->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }

}
?>