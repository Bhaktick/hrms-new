<?php
namespace App\Repositories\Candidate;
use Illuminate\Support\ServiceProvider;
class CandidateRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Candidate\CandidateInterface', 'App\Repositories\Candidate\CandidateRepository');
    }
}
?>