<?php
namespace App\Repositories\Admin;
use Illuminate\Support\ServiceProvider;
class AdminRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
            $this->app->bind('App\Repositories\Admin\AdminInterface', 'AdminRepositories\Admin\AdminRepository');
    }
}
?>