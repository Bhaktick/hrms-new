<?php
namespace App\Repositories\Admin;


use App\Repositories\Admin\AdminInterface;
use App\Models\Admin;
use Auth;
class AdminRepository implements AdminInterface
{
    public $admin;
    function __construct(Admin $admin) {
	$this->admin = $admin;
    }
    public function get()
    {
        return $this->admin->get();
    }
    public function find($id)
    {
        return $this->admin->find($id);
    }


    // public function delete($id)
    // {
    //     return $this->Role->delete($id);
    // }

     public function where($filters = '')
    {
        $query = $this->admin->query(); 
        if (isset($filters['where'])) {
          $query->where($filters['where']);
        }

        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }

        return $query->orderBy('created_at', 'desc')->get();
    }
    public function create($data)
    {
        return $this->admin->create($data);
    }

    public function update($data)
    {
        return $this->admin->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->admin->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }

}
?>