<?php
namespace App\Repositories\Balance;


use App\Repositories\Balance\BalanceInterface;
use App\Models\leave_balance;
use Auth;
class BalanceRepository implements BalanceInterface
{
    public $leave_bal;
    function __construct(leave_balance $leave_bal) {
    $this->leave_bal = $leave_bal;
    }
    public function get()
    {
        return $this->leave_bal->orderBy('created_at', 'desc')->get();
        // dd($hey);
    }
    public function find($id)
    {
        $query = $this->leave_bal->query(); 
        $query->where('Emp_id',$id)->first();
        return $query->get();
        
    }


    // public function delete($id)
    // {
    //     return $this->Role->delete($id);
    // }

     public function where($filters = '')
    {
        $query = $this->leave_bal->query(); 
        if (isset($filters['where'])) {
          $query->where($filters['where']);
        }

        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }
        return $query->orderBy('created_at', 'desc')->get();
    }
    public function create($data)
    {
        return $this->leave_bal->create($data);
    }

    public function update($data)
    {
        return $this->leave_bal->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->leave_bal->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }
    

}
?>