<?php
namespace App\Repositories\Role;


use App\Repositories\Role\RoleInterface;
use App\Models\Role;
use Auth;
class RoleRepository implements RoleInterface
{
    public $role;
    function __construct(Role $role) {
	$this->role = $role;
    }
    public function get()
    {
        return $this->role->get();
    }
    public function find($id)
    {
        return $this->role->find($id);
    }


    // public function delete($id)
    // {
    //     return $this->Role->delete($id);
    // }

     public function where($filters = '')
    {
        $query = $this->role->query(); 
        if (isset($filters['where'])) {
          $query->where($filters['where']);
        }

        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }

        return $query->get();
    }
    public function create($data)
    {
        return $this->role->create($data);
    }

    public function update($data)
    {
        return $this->role->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->role->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }

}
?>