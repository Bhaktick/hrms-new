<?php
namespace App\Repositories\HolidayList;
use Illuminate\Support\ServiceProvider;
class HolidayListRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\HolidayList\HolidayListInterface', 'App\Repositories\HolidayList\HolidayListRepository');
    }
}
?>