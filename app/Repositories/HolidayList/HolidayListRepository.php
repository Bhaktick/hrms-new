<?php
namespace App\Repositories\HolidayList;


use App\Repositories\HolidayList\HolidayListInterface;
use App\Models\HolidayList;
use Auth;
class HolidayListRepository implements HolidayListInterface
{
    public $holiday;
    function __construct(HolidayList $holiday) {
        $this->holiday = $holiday;
    }
    public function get()
    {
        return $this->holiday->orderBy('date', 'asc')->get();
        // dd($hey);
    }
    public function find($id)
    {
        return $this->holiday->find($id);
    }


    // public function delete($id)
    // {
    //     return $this->Role->delete($id);
    // }

     public function where($filters = '')
    {
        $query = $this->holiday->query(); 
        if (isset($filters['where'])) {
          $query->where($filters['where']);
        }

        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }
        return $query->orderBy('date', 'asc')->get();
    }
    public function create($data)
    {
        return $this->holiday->create($data);
    }

    public function update($data)
    {
        return $this->holiday->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->holiday->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }
    

}
?>