<?php
namespace App\Repositories\User;


use App\Repositories\User\UserInterface;
use App\User;
use Auth;
use Illuminate\Support\Arr; // for array helpers
use App\Models\Type;
use Carbon\Carbon; // for date and time 
use DateTime;
use Carbon\CarbonPeriod;
use DateInterval;


class UserRepository implements UserInterface
{
    public $user;


    function __construct(User $user) {
	$this->user = $user;
    }
    public function get()
    {
        return $this->user->orderBy('created_at', 'desc')->get();
    }

    public function getType()
    {
        $res = $this->user->find(Auth::user()->id);
        // dd($res->type);
        $usertype = type::find($res->type);
        $usertype = $usertype->name;
        // dd($usertype->name);
        return $usertype;
    }
    public function getEmpBirthday()
    {
        $HBD = $this->user->whereDay('DOB', '=', date('d'))->whereMonth('DOB', '=', date('m'))->get();
        return $HBD; 
    }

    public function getEmpJoin()
    {
        $JoinDate = $this->user->whereDay('DOJ', '=', date('d'))->whereMonth('DOJ', '=', date('m'))->get();
        return $JoinDate; 
    }


    public function find($id)
    {
        return $this->user->find($id);
    }


    // public function delete($id)
    // {
    //     return $this->user->delete($id);
    // }
    public function select($filters = '')
    {
        $select = array();

        $query = $this->user->query(); 
        if (isset($filters['select'])) {
            $i=0;
            foreach ($filters['select'] as $key => $value) {
            $select = Arr::add($select,$i,$value);
                // $query->select($value);
                $i++;
            }
        }
         return $query->select($select)->orderBy('created_at', 'desc')->get();

    }
    public function where($filters = '')
    {
        $query = $this->user->query(); 
        if (isset($filters['where'])) {
          $query->where($filters['where']);
        }

        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }

        return $query->orderBy('created_at', 'desc')->get();
    }
    public function create($data)
    {
        return $this->user->create($data);
    }

    public function update($data)
    {
        return $this->user->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->user->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }

}
?>