<?php
namespace App\Repositories\Leave;


use App\Repositories\Leave\LeaveInterface;
use App\Models\Leave;
use Auth;
class LeaveRepository implements LeaveInterface
{
    public $leave;
    function __construct(Leave $leave) {
    $this->leave = $leave;
    }
    public function get()
    {
        return $this->leave->orderBy('applied_date', 'desc')->get();
        // dd($hey);
    }
    public function find($id)
    {
        return $this->leave->find($id);
    }


    // public function delete($id)
    // {
    //     return $this->Role->delete($id);
    // }

     public function where($filters = '')
    {
        $query = $this->leave->query(); 
        if (isset($filters['where'])) {
          $query->where($filters['where']);
        }

        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }
        return $query->orderBy('applied_date', 'desc')->get();
    }
    public function create($data)
    {
        return $this->leave->create($data);
    }

    public function update($data)
    {
        return $this->leave->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->leave->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }
    

}
?>