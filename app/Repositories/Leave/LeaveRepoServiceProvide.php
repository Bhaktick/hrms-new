<?php
namespace App\Repositories\Leave;
use Illuminate\Support\ServiceProvider;
class LeaveRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Leave\LeaveInterface', 'App\Repositories\Leave\LeaveRepository');
    }
}
?>