<?php
namespace App\Repositories\Leave;
interface LeaveInterface {


    public function get();


    public function find($id);


    // public function delete($id);

    public function where($filters = ''); // contains where whereIN Group by

    public function create($data);

    public function update($data);

    public function delete($where,$where_in = '');
    // public function select($where_in = '');




}
?>