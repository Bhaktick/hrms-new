<?php
namespace App\Repositories\Event;


use App\Repositories\Event\EventInterface;
use App\Models\Event;
use Auth;
class EventRepository implements EventInterface
{
    public $event;
    function __construct(Event $event) {
        $this->event = $event;
    }
    public function get()
    {
        return $this->event->orderBy('created_at', 'desc')->get();
        // dd($hey);
    }
    public function find($id)
    {
        return $this->event->find($id);
    }


    // public function delete($id)
    // {
    //     return $this->Role->delete($id);
    // }

     public function where($filters = '')
    {
        $query = $this->event->query(); 
        if (isset($filters['where'])) {
          $query->where($filters['where']);
        }

        if (isset($filters['whereIn'])) {
            foreach ($filters['whereIn'] as $key => $value) {
                $query->whereIn($key,$value);
            }
        }
        if (isset($filters['groupBy'])) {
            $query->groupBy($filters['groupBy']);
        }
        return $query->orderBy('created_at', 'desc')->get();
    }
    public function create($data)
    {
        return $this->event->create($data);
    }

    public function update($data)
    {
        return $this->event->update($data);
    }

    public function delete($where,$where_in = '')
    {
        $r_tags = $this->event->where('id',$where);

        if ($where_in != '') {
            foreach ($where_in as $key => $value) {
                $r_tags->whereIn($key,$value);
            }
        }
        return $r_tags->delete();    
    }
    

}
?>