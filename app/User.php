<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'email', 'password', 'Designation', 'type', 'Role', 'DOJ', 'DOB', 'Contact', 'Another_cont', 'Email_password', 'SkypeID', 'Skype_password', 'TeamloggerID', 'Team_password', 'Education', 'Course', 'Address', 'Personal_Email', 'Salary', 'Experience', 'Inc_Time_Period', 'Last_Inc_Date', 'Last_Inc_Amount', 'Bank_name', 'Acc_holder_name', 'Acc_no', 'IFSC','Gender','Doc_status','Doc_code','Status','relieving_date','NDA_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function type()
    {
        return $this->hasMany('type');
    }

    public function getDojAttribute()
    {
        return date_format( date_create($this->attributes['DOJ']),'d-m-Y');
    }
}
