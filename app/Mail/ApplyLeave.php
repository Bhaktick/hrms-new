<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApplyLeave extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->details['mode'] == 0)
        {

        return $this->subject('HRMS-Application to Leave ')->view('layouts.Leave.LeaveMail');
        }
        elseif($this->details['mode'] == 1)
        {
            return $this->subject('HRMS-Your Leave Is Noted')->view('layouts.Leave.LeaveMail');
        }
    }
}
