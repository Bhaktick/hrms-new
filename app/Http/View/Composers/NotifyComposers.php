<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Repositories\Leave\LeaveRepository;
use App\Repositories\Event\EventRepository;
use Illuminate\Support\Facades\Auth; // for login user data
use Carbon\Carbon; // for date and time 
use DateTime;
use Carbon\CarbonPeriod;
use DateInterval;

class NotifyComposers
{
    protected $leave;

    protected $event;
    /**
     * Create a new categories composer.
     *
     * @param  CategoryRepository $categories
     * @return void
     */
    public function __construct(LeaveRepository $leave,EventRepository $event)
    {
        $this->leave = $leave;
        $this->event = $event;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Auth::user()->Role == 2 || Auth::user()->Role == 1) // for HR and Admin Pending Notification
        {
            $pending = $this->leave->where([ 'where' => ['status' => 0,'Emp_type' => 3] ]);//data of pending Leave Requests //status = "Pending"
            $Approve = $this->leave->where([ 'where' => ['Notify_status' => 0,'status' => 1,'Emp_id' => Auth::user()->id] ]);//data of Approve Leave Requests
            $notApprove = $this->leave->where([ 'where' => ['Notify_status'=> 0,'status' => 2,'Emp_id' => Auth::user()->id] ]);//data of Not Approve Leave Requests
            $date =Carbon::today()->format('Y-m-d');
            $event = $this->event->where(['where' => ['date' => $date]]);//data of today's Event
            $id = array_merge($Approve->pluck('id')->toArray(),$notApprove->pluck('id')->toArray());
            // if(empty($pending) || empty($Approve) || empty($notApprove) || empty($event))
            // {
            //     $notify = [ 'count' => "0", 'LeaveCount' => "0",'RequestCount' => "0",'id' => ""];
            // }
            // else
            // {
                if(Auth::user()->Role == 2)
                {
                    $count = count($pending) + count($Approve) + count($notApprove) + count($event);
                }
                else
                {
                    $pending = $this->leave->where([ 'where' => ['status' => 0] ,'whereIn' => ['Emp_type' => [2,3] ] ]);//data of pending Leave Requests //status = "Pending"
                    $count = count($pending);
                }

                 $notify = [ 'count' => $count, 'LeaveCount' => count($Approve) + count($notApprove),'RequestCount' => count($pending),'id' => $pending->pluck('id')->toArray(),'EventCount' => count($event)];//counting for badge value   
              
            // }
             

            $view->with('notify', $notify);
        }
        else if(Auth::user()->Role == 3)//for Leave action Notification
        {
            $Approve = $this->leave->where([ 'where' => ['Notify_status' => 0,'status' => 1,'Emp_id' => Auth::user()->id] ]);//data of Approve Leave Requests
            $notApprove = $this->leave->where([ 'where' => ['Notify_status' => 0,'status' => 2,'Emp_id' => Auth::user()->id] ]);//data of Not Approve Leave Requests
            $event = $this->event->where(['where' => ['date' => Carbon::today()->format('d-m-Y')]]);//data of today's Event
            // dd($Approve,$notApprove);
      
            $id = array_merge($Approve->pluck('id')->toArray(),$notApprove->pluck('id')->toArray());
            // dd($id);
            // if(empty($Approve) || empty($notApprove))
            // {
            //     $notify = [ 'count' => "0", 'LeaveCount' => "0",'RequestCount' => "0",'id' => ""];
            // }
            // else
            // {
                $notify = [ 'count' => count($Approve) + count($notApprove) + count($event), 'LeaveCount' => count($Approve) + count($notApprove),'id' => $id,'EventCount' => count($event)];//counting for badge value 
            // }
            
            $view->with('notify', $notify);
        }
        
        
    }
}