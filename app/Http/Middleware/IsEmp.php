<?php
  
namespace App\Http\Middleware;
  
use Closure;
   
class IsEmp 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->Role == 3){
            return $next($request);
        }
   
        return redirect('NoAccess');
    }
}