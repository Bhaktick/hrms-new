<?php
  
namespace App\Http\Middleware;
  
use Closure;
   
class IsHR 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->Role == 2){
            return $next($request);
        }
               // dd(auth()->user()->Role);

        return redirect('NoAccess');
    }
}