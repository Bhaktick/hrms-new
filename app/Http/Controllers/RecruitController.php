<?php

namespace App\Http\Controllers;
use App\Repositories\User\UserInterface;
use App\Repositories\Leave\LeaveInterface;
use App\Repositories\Setting\SettingInterface;
use App\Repositories\Type\TypeInterface;
use App\Repositories\Role\RoleInterface;
use App\Repositories\Admin\AdminInterface;
use App\Repositories\Balance\BalanceInterface;
use App\Repositories\Candidate\CandidateInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;//for password hashing
use DataTables; //for Yajra
use App\DataTables\UsersDataTable;
use Carbon\Carbon; // for date and time 
use DateTime;
use Excel;
use App\Exports\UsersExport;
use Carbon\CarbonPeriod;
use DateInterval;


class RecruitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $user;

    private $setting;

    private $type;

    private $leave;

    private $leave_bal;
    private $candidate;

    public function __construct(
        UserInterface $user,
        SettingInterface $setting,
        TypeInterface $type,
        LeaveInterface $leave,
        BalanceInterface $leave_bal,
        CandidateInterface $candidate
    )
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->setting = $setting;
        $this->type = $type;
        $this->leave = $leave;
        $this->leave_bal = $leave_bal;
        $this->candidate = $candidate;
    }
   public function AddCandidate($code,Request $request)
   {
    //$code =0 means add candidate and $code =1 means edit candidate 
   		$test=$request->validate([
            'Name' => 'required|max:255',
            'Email' => 'nullable|email',
            'Contact' => 'required|numeric|digits:10',
            'Designantion' => 'required|max:100',
            'Type' => 'required',
            ]);
            if(!$test)
            {
                return redirect('HR.AddCandidate')
                        ->withErrors($validator)
                        ->withInput();
            }

            if($request->Type == "Experienced" && empty($request->Experience) )
            {
            	return redirect()->back()->with('warning',__('success.Experience_required'));
            }
            else
            {
            	if($request->Type == "Experienced")
            	{
                	$exp = $request->Experience;
                    if(!is_numeric($exp))
                    {
                        return redirect()->back()->with('warning',__('success.Experience_numeric'));
                    }
            	}
            	else
            	{
            		$exp = 0;
            	}
            }
            if($code == 0)
            {
                $this->candidate->create([
                	'can_name' => $request->Name,
    	            'can_email' => $request->Email,
    	            'can_contact' => $request->Contact,
    	            'can_designantion' => $request->Designantion,
    	            'can_experience' => $exp,
                ]);
                return redirect()->route('HR.candidateList')->with('success',__('success.Candidate_saved'));

            }
            else
            {
                $edit = $this->candidate->find($request->can_id);
                $edit->update([
                    'can_name' => $request->Name,
                    'can_email' => $request->Email,
                    'can_contact' => $request->Contact,
                    'can_designantion' => $request->Designantion,
                    'can_experience' => $exp,
                ]);
                return redirect()->route('HR.candidateList')->with('success',__('success.Candidate_edited'));

            }

   }
   public function EditSelected($id,Request $request)
   {
        //code to edit joining date and  notice period...
        $test=$request->validate([
            'notice_period' => 'required|numeric|size:2',
            'Joining_Date' => 'required',
            ]);
            if(!$test)
            {
                return redirect('HR.EditSelected')
                        ->withErrors($validator)
                        ->withInput();
            }
            $edit = $this->candidate->find($id);
            $edit->update([
                    'notice_period' => $request->notice_period,
                    'joining_date' => $request->Joining_Date,
                ]);
                
        return redirect()->route('HR.Selected')->with('success',__('success.Selected_edited'));
   }
   public function getCandidateList(Request $request)
   {
   		if ($request->ajax()) {
            $Candidate = $this->candidate->where(['where' => ['interview_date' => null] ] );// data of Candidate that are left to schedule for technical Round 
            return Datatables::of($Candidate)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    if(Auth::user()->Role == 2)
                    {
                        $actionBtn = "<a href='".route('HR.CallEditCandidate',['id' => $row->id])."'><i class='fas fa-edit p-2'></i></a><a href='".route('HR.RemoveCandidate',['id' => $row->id])."'><i class='fas fa-trash p-2'></i></a>";
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditCandidate',['id' => $row->id])."'><i class='fas fa-eye'></i></a>";
                    }
                        return $actionBtn;
                })
                ->addColumn('schedule', function($row){  
                        //option is not approve and rejected list then
                    if(Auth::user()->Role == 2)
                    {
                        $actionBtn = "<a href='".route('HR.scheduleForm',['id' => $row->id,'Itype' => 0])."' class='btn btn-primary'>Technical</a>";
                        return $actionBtn;
                    }
                })
                ->rawColumns(['action','schedule'])
                ->make(true); 

        }
   }
   public function getSelectedList(Request $request)
   {
        if ($request->ajax()) {
            $Candidate = $this->candidate->where(['where' => ['hr_status' => 1] ] );// data of Candidate that are selected

            return Datatables::of($Candidate)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    if(Auth::user()->Role == 2)
                    {
                        $actionBtn = "<a href='".route('HR.CallEditSelected',['id' => $row->id])."'><i class='fas fa-edit p-2'></i></a>";
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditSelected',['id' => $row->id])."'><i class='fas fa-eye'></i></a>";
                    }
                        return $actionBtn;
                })
                ->addColumn('Jdate',function ($row){
                    if($row->DOJ != null)
                    {
                         $Join_Date = date_format( date_create($row->DOJ),'d-m-Y');
                         return $Join_Date;
                    }
                    else
                    {
                        return $row->DOJ;
                    }
                    
                })
                ->rawColumns(['action','schedule','Jdate'])
                ->make(true); 

        }
   }
   public function SaveSchedule($id,$Itype,Request $request)
   {
        $can = $this->candidate->find($id);
        $test=$request->validate([
            'Interviewer' => 'required',
            'Date' => 'required',
            'Time' => 'required',
            'type' => 'required',
            ]);
            if(!$test)
            {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }

        if($Itype == 0)//Technical interview
        {
            $can->update([
                'interviewer' => $request->Interviewer,
                'interview_date' => $request->Date,
                'interview_time' => $request->Time,
                'interview_type' => $request->type,
            ]);
            return redirect()->route('HR.TechnicalSchedule')->with('success',__('success.Technical_success'));
        }
        else if($Itype == 1)//Practical interview
        {
            $can->update([
                'practical_interviewer' => $request->Interviewer,
                'practical_date' => $request->Date,
                'practical_time' => $request->Time,
                'practical_type' => $request->type,
            ]);
            return redirect()->route('HR.PracticalSchedule')->with('success',__('success.Practical_success'));
        }    
   }
   public function EditSchedule($id,$Itype,Request $request)
   {
        $can = $this->candidate->find($id);
        
        if($Itype == 0)//Technical interview
        {
            $test=$request->validate([
            'TInterviewer' => 'required',
            'TDate' => 'required',
            'TTime' => 'required',
            'type' => 'required',
            ]);
            if(!$test)
            {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }
            $can->update([
                'interviewer' => $request->TInterviewer,
                'interview_date' => $request->TDate,
                'interview_time' => $request->TTime,
                'interview_type' => $request->type,
            ]);
            return redirect()->route('HR.TechnicalSchedule')->with('success',__('success.EditSchedule_success'));
        }
        else if($Itype == 1)//both interview
        {
            $test=$request->validate([
            'PInterviewer' => 'required',
            'PDate' => 'required',
            'PTime' => 'required',
            'Ptype' => 'required',
            ]);
            if(!$test)
            {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }
            $can->update([
                'practical_interviewer' => $request->PInterviewer,
                'practical_date' => $request->PDate,
                'practical_time' => $request->PTime,
                'practical_type' => $request->Ptype,
            ]);
            return redirect()->route('HR.PracticalSchedule')->with('success',__('success.EditSchedule_success'));
        }    
   }
   public function swipeResult($id,$Itype,$result)
   {
        $swipe = $this->candidate->find($id);

        if($result == 1)//selected
        {
            $result = 2;//rejected swipe
        }
        else if($result == 2)//rejected
        {
            $result = 1;//selected swipe
        }
        if($Itype == 0)//technical status
        {
            $swipe->update([
                'technical_status' => $result,
            ]);
            return redirect()->route('HR.TechnicalSchedule')->with('success',__('success.Technical_swipe'));
        }
        else if($Itype == 1)//Practical status
        {
            $swipe->update([
                'practical_status' => $result,
            ]);
            return redirect()->route('HR.PracticalSchedule')->with('success',__('success.Practical_swipe'));
        }
        else if($Itype == 2)//Final HR status 
        {
            $swipe->update([
                'hr_status' => $result,
            ]);
            return redirect()->route('HR.CandidateStatus')->with('success',__('success.HR_swipe'));
        }
   }
   public function CallScheduleForm($id,$Itype)
   {
        $candidate = $this->candidate->find($id);
        $List = $this->user->where([ 'where' => ['type'=> 3] ]);
        return view('HR.Recruit.Schedule',compact('Itype','candidate','List'));
   }
   public function ResultModel($id,$mode)
   {
        $candidate = $this->candidate->find($id);
        return view('HR.Recruit.ResultModel',compact('candidate','mode'));
   }
   public function CallEditSchedule($id,$Itype)
   {
        $candidate = $this->candidate->find($id);
        $List = $this->user->where([ 'where' => ['type'=> 3] ]);
        return view('HR.Recruit.EditSchedule',compact('Itype','candidate','List'));
   }
   public function CallEditSelected($id)
   {
        $candidate = $this->candidate->find($id);
        return view('HR.Recruit.EditSelected',compact('candidate'));
   }
   public function CallEditCandidate($id)
   {
        $candidate = $this->candidate->find($id);
        return view('HR.Recruit.EditCandidate',compact('candidate'));
   }
   public function RemoveCandidate($id)
   {
        $candidateRmv = $this->candidate->delete($id);
        return redirect()->route('HR.candidateList')->with('success',__('success.Candidate_Removed'));
   }
   public function SaveFinal($id,Request $request)
   {
        $test=$request->validate([
            'remark' => 'max:255',
            'status' => 'required',
            ]);
            if(!$test)
            {
                return redirect('HR.ResultModel')
                        ->withErrors($validator)
                        ->withInput();
            }
        $save = $this->candidate->find($id);
        $save->update([
            'remark' => $request->remark,
            'hr_status' => $request->status,
        ]);
        return redirect()->route('HR.CandidateStatus')->with('success',__('success.HR_Result'));

   }
   public function setResult($id,$Itype,$result)
   {
        $SetResult = $this->candidate->find($id);
        if($Itype == 0)// Technical
        {
            $SetResult->update([
                'technical_status' => $result,
            ]);
            return redirect()->route('HR.TechnicalSchedule')->with('success',__('success.Technical_Result'));
        }
        else if($Itype == 1)// Practical
        {
            $SetResult->update([
                'practical_status' => $result,
            ]);
            return redirect()->route('HR.PracticalSchedule')->with('success',__('success.Practical_Result'));
        }
   }
   public function getCandidateStatus(Request $request)
   {
        if ($request->ajax()) {
            $Candidate = $this->candidate->where(["notwhere" => [ "hr_status" ,'!=', null] ]);
            $user =$this->user;

            return Datatables::of($Candidate)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    if(Auth::user()->Role == 2)
                    {
                        $actionBtn = "<a href='".route('HR.ResultModel',['id' => $row->id,'mode' => 1])."' class='btn btn-primary'><i class='fas fa-lg fa-edit '></i></a>";
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditCandidate',['id' => $row->id])."'><i class='fas fa-eye'></i></a>";
                    }
                })
                ->addColumn('delete', function($row){

                    if(Auth::user()->Role == 2)
                    {
                        $actionBtn = "<a href='".route('HR.RemoveCandidate',['id' => $row->id])."' class='btn btn-primary'><i class='fas fa-lg text-danger fa-trash p-2'></i></a>";
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditCandidate',['id' => $row->id])."'><i class='fas fa-eye'></i></a>";
                    }
                })
                ->addColumn('Technical', function($row) use ($user){
                    $interviewer = $this->user->find($row->interviewer);
                    if($interviewer != null)
                    {
                        $Technical = $interviewer->name."#".$interviewer->id;
                        return $Technical;
                    }
                })
                ->addColumn('Practical', function($row) use ($user){
                    $interviewer = $this->user->find($row->interviewer);
                    if($interviewer != null)
                    {
                        $Practical = $interviewer->name."#".$interviewer->id;
                        return $Practical;
                    }
                })
                ->addColumn('Ttype', function($row){
                     if($row->interview_type == 0)
                    {
                        return "Online";
                    }
                    else if($row->interview_type == 1)
                    {
                        return "Offline";
                    }
                })
                ->addColumn('Ptype', function($row){
                     if($row->practical_type == 0)
                    {
                        return "Online";
                    }
                    else if($row->practical_type == 1)
                    {
                        return "Offline";
                    }
                })
                ->addColumn('Tstatus', function($row){
                     if($row->technical_status == 1)//selected
                    {
                        return "<i class='fas fa-lg fa-check-circle text-success' data-toggle='tooltip' title='Selected'></i>";
                    }
                    else if($row->technical_status == 2)//rejected
                    {
                        return "<i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'></i>";
                    }
                })
                ->addColumn('Pstatus', function($row){
                     if($row->practical_status == 1)//selected
                    {
                        return "<i class='fas fa-lg fa-check-circle text-success' data-toggle='tooltip' title='Selected'></i>";
                    }
                    else if($row->practical_status == 2)//rejected
                    {
                        return "<i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'></i>";
                    }
                })
                ->addColumn('Hstatus', function($row){
                     if($row->hr_status == 1)//selected
                    {
                        return "<i class='fas fa-lg fa-check-circle text-success' data-toggle='tooltip' title='Selected'></i><a href='".route('HR.swipeResult',['id' => $row->id,'Itype' => 2,'result' => 1])."' class='btn btn-primary'>Swipe</a>";
                    }
                    else if($row->hr_status == 2)//rejected
                    {
                        return "<i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'></i><a href='".route('HR.swipeResult',['id' => $row->id,'Itype' => 2,'result' => 2])."' class='btn btn-primary'>Swipe</a>";
                    }
                })
                ->addColumn('Tdate',function ($row){
                    if($row->interview_date != null)
                    {
                         $Tech_Date = date_format( date_create($row->interview_date),'d-m-Y');
                         return $Tech_Date;
                    }
                    else
                    {
                        return $row->interview_date;
                    }
                })
                ->addColumn('Pdate',function ($row){
                    if($row->practical_date != null)
                    {
                         $Prac_Date = date_format( date_create($row->practical_date),'d-m-Y');
                         return $Prac_Date;
                    }
                    else
                    {
                        return $row->practical_date;
                    }
                })
                ->rawColumns(['action','delete','Technical','Practical','Ttype','Ptype','Tstatus','Pstatus','Hstatus','Tdate','Pdate'])
                ->make(true); 
            }
   }
   public function getTScheduleList(Request $request)
   {
   		if ($request->ajax()) {
            $Candidate = $this->candidate->where(["notwhere" => [ "interview_date" ,'!=', null], 'where' => ['practical_date' => null] ] );
           $user =$this->user;
            return Datatables::of($Candidate)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    if(Auth::user()->Role == 2)
                    {
                        if($row->technical_status != null)
                        {
                            $actionBtn = "<a href='".route('HR.scheduleForm',['id' => $row->id,'Itype' => 1])."' class='btn btn-primary'>Schedule_Practical</a>";
                        }
                        else if($row->technical_status == null && Carbon::today() >= $row->interview_date)
                        {
                            $actionBtn = "<a href='".route('HR.setResult',['id' => $row->id,'Itype' => 0,'result' => 1])."'><i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Select'></i></a><a href='".route('HR.setResult',['id' => $row->id,'Itype' => 0,'result' => 2])."' ><i class='fas fa-times-circle fa-lg text-danger' data-toggle='tooltip' title='Reject'></i></a>";
                        }
                        else if($row->technical_status == null)
                        {
                            $actionBtn ="";
                        }
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditSchedule',['id' => $row->id,'Itype' => 0])."'><i class='fas fa-eye'></i></a>";
                    }
                        return $actionBtn;
                })
                ->addColumn('Type', function($row){
                    if($row->interview_type == 0)
                    {
                        return "Online";
                    }
                    else if($row->interview_type == 1)
                    {
                        return "Offline";
                    }
                })
                ->addColumn('Edit', function($row){
                    return "<a href='".route('HR.CallEditSchedule',['id' => $row->id,'Itype' => 0])."' ><i class='fas fa-edit'></i></a>";
                })
                ->addColumn('Interviewer', function($row) use ($user){
                    $interviewer = $this->user->find($row->interviewer);
                    if($interviewer != null)
                    {
                         $name = $interviewer->name."#".$interviewer->id;
                        return $name;
                    }
                })
                ->addColumn('result', function($row){
                    if($row->technical_status == null)
                    {
                        $result = "<i class='fas fa-2x fa-question-circle text-info' data-toggle='tooltip' title='Result Pending'></i>";
                    }
                    else if($row->technical_status == 1)//selected
                    {
                        $result = "<i class='fas fa-2x fa-user-check text-success' data-toggle='tooltip' title='Selected'></i><a href='".route('HR.swipeResult',['id' => $row->id,'Itype' => 0,'result' => 1])."' class='btn btn-primary'>Swipe</a>";
                    }
                    else if($row->technical_status == 2)//rejected
                    {
                        $result = "<i class='fas fa-2x fa-user-times text-danger' data-toggle='tooltip' title='Rejected'></i><a href='".route('HR.swipeResult',['id' => $row->id,'Itype' => 0,'result' => 2])."' class='btn btn-primary'>Swipe</a>";
                    }
                    
                    return $result;
                })
                ->addColumn('Tdate',function ($row){
                    if($row->interview_date != null)
                    {
                     $Tech_Date = date_format( date_create($row->interview_date),'d-m-Y');
                     return $Tech_Date;

                    }
                    else
                    {
                        return $row->interview_date;
                    }
                })
                ->rawColumns(['action','Type','Edit','Interviewer','result','Tdate'])
                ->make(true);
   		}
   	}
    public function getPScheduleList(Request $request)
   {
        if ($request->ajax()) {
            $Candidate = $this->candidate->where(["notwhere" => [ "practical_date" ,'!=', null], 'where' => ['hr_status' => null,] ] );
            $user = $this->user;

            return Datatables::of($Candidate)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    if(Auth::user()->Role == 2)
                    {
                        if($row->practical_status != null)
                        {
                            $actionBtn = "<a href='".route('HR.ResultModel',['id' => $row->id,'mode' => 0])."' class='btn btn-primary'>Final Result</a>";
                        }
                        else if($row->practical_status == null && Carbon::today() >= $row->practical_date)
                        {
                            $actionBtn = "<a href='".route('HR.setResult',['id' => $row->id,'Itype' => 1,'result' => 1])."'><i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Select'></i></a><a href='".route('HR.setResult',['id' => $row->id,'Itype' => 1,'result' => 2])."' ><i class='fas fa-times-circle fa-lg text-danger' data-toggle='tooltip' title='Reject'></i></a>";
                        }
                        else if($row->practical_status == null)
                        {
                            $actionBtn ="";
                        }
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditSchedule',['id' => $row->id,'Itype' => 0])."'><i class='fas fa-eye'></i></a>";
                    }
                        return $actionBtn;
                })
                ->addColumn('Interviewer', function($row) use ($user){

                    $interviewer = $this->user->find($row->practical_interviewer);
                    if($interviewer != null)
                    {
                        $name = $interviewer->name."#".$interviewer->id;
                        return $name;
                    }
                })
                ->addColumn('result', function($row){
                    if($row->practical_status == null)
                    {
                        $result = "<i class='fas fa-2x fa-question-circle text-info' data-toggle='tooltip' title='Result Pending'></i>";
                    }
                    else if($row->practical_status == 1)//selected
                    {
                        $result = "<i class='fas fa-2x fa-user-check text-success' data-toggle='tooltip' title='Selected'></i><a href='".route('HR.swipeResult',['id' => $row->id,'Itype' => 1,'result' => 1])."' class='btn btn-primary'>Swipe</a>";
                    }
                    else if($row->practical_status == 2)//rejected
                    {
                        $result = "<i class='fas fa-2x fa-user-times text-danger' data-toggle='tooltip' title='Rejected'></i><a href='".route('HR.swipeResult',['id' => $row->id,'Itype' => 1,'result' => 2])."'class='btn btn-primary'>Swipe</a>";
                    }
                    
                    return $result;
                })
                ->addColumn('Type', function($row){
                    if($row->practical_type == 0)
                    {
                        return "Online";
                    }
                    else if($row->practical_type == 1)
                    {
                        return "Offline";
                    }
                })
                ->addColumn('Edit', function($row){
                    return "<a href='".route('HR.CallEditSchedule',['id' => $row->id,'Itype' => 1])."' ><i class='fas fa-edit'></i></a>";
                })
                ->addColumn('Pdate',function ($row){
                    if($row->practical_date != null)
                    {
                     $Prac_Date = date_format( date_create($row->practical_date),'d-m-Y');
                     return $Prac_Date;

                    }
                    else
                    {
                        return $row->practical_date;
                    }
                })
                ->rawColumns(['action','Type','Interviewer','result','Edit','Pdate'])
                ->make(true);
        }
    }

    public function ExportCandidate($format)
    {
        $heading = array("Id", "Candidate Name", "Candidate Email Id", "Candidate Designation","Candidate Contact No","Candidate Experience","Technical Interviewer" , "Technical Interview Date","Technical Interview Time","Technical Interview Type","Practical Interviewer" , "Practical Interview Date","Practical Interview Time","Practical Interview Type", "Technical Interview Status", "Practical Interview Status", "HR Round Status", "Notice Period", "Joining Date", "Remark" );
        $data = $this->candidate->select(['select' => ['id','can_name','can_email','can_designantion','can_contact','can_experience','interviewer','interview_date','interview_time','interview_type','practical_interviewer','practical_date','practical_time','practical_type','technical_status','practical_status','hr_status','notice_period','joining_date','remark']]);
        if($format == 'CSV')
        {
            return Excel::download(new UsersExport($data,$heading), 'CandidateData.csv');
        }
        else if($format == 'Excel')
        {
            return Excel::download(new UsersExport($data,$heading), 'CandidateData.xlsx');
        }
    }
}