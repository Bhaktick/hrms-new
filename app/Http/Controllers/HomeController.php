<?php

namespace App\Http\Controllers;
use App\Repositories\User\UserInterface;
use App\Repositories\Leave\LeaveInterface;
use App\Repositories\Setting\SettingInterface;
use App\Repositories\Type\TypeInterface;
use App\Repositories\Role\RoleInterface;
use App\Repositories\Admin\AdminInterface;
use App\Repositories\Balance\BalanceInterface;
use App\Repositories\Event\EventInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;//for password hashing
use DataTables; //for Yajra
use App\DataTables\UsersDataTable;
use Carbon\Carbon; // for date and time 
use DateTime;
use Carbon\CarbonPeriod;
use DateInterval;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user;

    private $setting;

    private $type;

    private $leave;

    private $leave_bal;

    private $event;


    public function __construct(
        UserInterface $user,
        SettingInterface $setting,
        TypeInterface $type,
        LeaveInterface $leave,
        BalanceInterface $leave_bal,
        EventInterface $event
    )
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->setting = $setting;
        $this->type = $type;
        $this->leave = $leave;
        $this->leave_bal = $leave_bal;
        $this->event = $event;

    }

    /** 
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index(UsersDataTable $dataTable)
    // {
    //     return $dataTable->render('test');
    // }
    public function index()
    {
        $TotalEmp = $this->user->where(['whereIn' => ['type' => [3,2] ] ]);
        $NewEmp = $this->user->where(['where' => ['DOJ' => Carbon::today()->format('Y-m-d')]]);
        $HBD = $this->user->getEmpBirthday();
        $JoinDate = $this->user->getEmpJoin();
        $counts = ['TotalEmp' => count($TotalEmp),'NewEmp' => count($NewEmp),'HBD' => count($HBD),'JoinDate'=> count($JoinDate)];
        return view('admin.dashboard',compact('counts'));
    }
    public function edit($id)
    {
        // $data = User::find($id);
        $data = $this->user->find($id);//repo
        return view('data.userEdit',compact('data'));
    }
     public function update(Request $request)
     {
        // dd($request->id);
        // $record = User::find($request->id);
        $record = $this->user->find($request->id);//repository

        $record->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);
        return redirect()->route('admin.home')->with('success',__('success.Record_updated'));
     }
    public function delete($id)
    {
        // $data = User::find($id);
        $data = $this->user->delete($id);//repository concept
        return redirect()->route('admin.home');

    }
    
    public function getLeaveDataById($id)
    {
        $Leave = $this->leave->find($id);
        // dd($Leave->Emp_id,'id',$id);
        $Emp = $this->user->find($Leave->Emp_id);
        // dd($Emp);
        return view('layouts.Leave.LeaveModel',compact('Leave','Emp'));
    }
    public function EditLeaveAction($id)
    {
        // code to edit taken not taken is here
        $record = $this->leave->find($id);//data of current leave
        $mode = 0;
        // calculations for Swipe Final status starts here
        if($record->final_status == 0)// not taken 
        {
            // code to swipe status 0 to 1 means not taken to taken
            //case 1= most probably this case is  Rejected Leave and taken
            
                $this->Calculate($id,"plus"); // working DO NOT CHANGE THIS STATUS
                // here Leave balance is calculated for this
                $mode = 0;// not approve and taken
                $message = "success.Taken_success";
               
        }
        else if($record->final_status == 1)// taken
        {
            // code to swipe status 1 to 0 means taken to not taken
            // case 2 =most probably this case is Approved leave and not taken
            $record->update([
                'final_status' => 0,// updated to not taken
            ]);
            $this->Calculate($id,"minus");// working DO NOT CHANGE THIS STATUS
            // here Leave balance is calculated for this
                $mode = 1;//  approve and not taken
                $message = "success.NotTaken_success";
        }
        // calculations for Swipe Final status ends here
                $Leave = $this->leave->find($id);// Leave data of id 
                // dd($Leave);
                $From = Auth::user()->email;// current login user
                $To = $this->user->find($Leave->Emp_id);// employee email
                // dd($To);
                $details = [
                    'title' => 'HRMS-Leave Apllication Final Status Updated',
                    'From' => $From,
                    'data' => $Leave,  
                    'mode' => $mode,      
                ];
                
                // dd($To,$From);

                \Mail::to($To->email)->send(new \App\Mail\EditLeaveMail($details));


            return redirect()->back()->with('success',__($message));

    }
    public function SaveNotify()
    {
        $myNotifications = $this->leave->where(['where' => ['Notify_status' => "0",'Emp_id' => Auth::user()->id]]);//notifications for login users 
         foreach ($myNotifications as $value) {
            $data = $this->leave->find($value->id);
            // dd($data);
            if($data->Notify_status == 0 && $data->status != 0)//"Pending"
            {
                $data->update([
                'Notify_status' => 1
                ]);
            }
         }
    }
    public function ApplyLeave($mode,Request $request)
    {
        //$mode == 0 means Applying Leave
        //$mode == 1 Means  Adding Leave Dynemically
        
         if( empty($request->Leave_Type) ) 
         {
            return redirect()->route('CallApplyLeave')->with('warning',__('success.Leave_Type'));

         }  
         elseif($mode == 1)
         {
            if(empty($request->Employee))
            {
                return redirect()->route('CallApplyLeave')->with('warning',__('success.Select2'));
            }
            else if(empty($request->remark))
            {
                return redirect()->route('CallApplyLeave')->with('warning',__('success.remark'));
            }
         }

         $remark = $request->remark;
        if($mode == 1)
        {
            $final = null;//null
            $applied_date = null;//null 
            $status = null;// null means not applied
            $Emp_id = $request->Employee;
            $E =$this->user->find($request->Employee);
            $Emp_type = $E->type;// type of slected employee
        }
        else if($mode == 0)
        {
            $final =0;// not taken
            $applied_date = Carbon::today()->toDateString();// today
            $status = 0;//pending 
            $Emp_id = Auth::user()->id;//login user
            $Emp_type = Auth::user()->type;// current user
        }

        if($request->Leave_Type == 1)// fullday 
        {
            $test=$request->validate([
            'Reason' => 'required|max:255',
            'Leave_Type' => 'required',
            'date_full' => 'required|after_or_equal:today',
            ]);
            if(!$test)
            {
                return redirect('CallApplyLeave')
                        ->withErrors($validator)
                        ->withInput();
            }

            $Leave = ([
                'Leave_Type' => $request->Leave_Type,
                'start_date' => $request->date_full,
                'end_date' => $request->date_full,
                'Total_Leaves' => 1,
                'applied_date' => $applied_date,
                'Reason' => $request->Reason,               
                'Emp_id' => $Emp_id,
                'Emp_type' => $Emp_type,
                'status' => $status,//null or "Pending"
                'Notify_status' => 0, 
                'final_status' => $final,              
            ]);
        }
        elseif($request->Leave_Type == 2 || $request->Leave_Type == 3) // 2= first-half and 3= second-half
        {
            $test=$request->validate([
            'Reason' => 'required|max:255',
            'Leave_Type' => 'required',
            'date_half' => 'required|after_or_equal:today',
            ]);
            if(!$test)
            {
                return redirect('CallApplyLeave')
                        ->withErrors($validator)
                        ->withInput();
            }

            $Leave = ([
                'Leave_Type' => $request->Leave_Type,
                'start_date' => $request->date_half,
                'end_date' => $request->date_half,
                'Total_Leaves' => 0.5,
                'applied_date' => $applied_date,
                'Reason' => $request->Reason,
                'Emp_id' => $Emp_id,
                'Emp_type' => $Emp_type,
                'status' => $status,//null or "Pending"
                'Notify_status' => 0,   
                'final_status' => $final,            
            ]);
        }
        elseif($request->Leave_Type == 4)//manual Days
        {
            $test=$request->validate([
            'Reason' => 'required|max:255',
            'Leave_Type' => 'required',
            'Total_Leaves' => 'required|numeric|max:5',
            'Manual_dates' => 'required|array|min:'.$request->Total_Leaves,
            ]);
            if(!$test)
            {
                return redirect('CallApplyLeave')
                        ->withErrors($validator)
                        ->withInput();
            }
           // to che if duplicate values for manual dates
            // dd( count($request->Manual_dates), count(array_unique($request->Manual_dates)));
            if( count($request->Manual_dates) != count(array_unique($request->Manual_dates)) )
                    return redirect()->back()->with('warning',__('success.Same_Dates'));
           
            $Leave = ([
                'Leave_Type' => $request->Leave_Type,
                'Manual_dates' => implode(',', $request['Manual_dates']),
                'Total_Leaves' => $request->Total_Leaves,
                'applied_date' => $applied_date,
                'Reason' => $request->Reason,
                'Emp_id' => $Emp_id,
                'Emp_type' => $Emp_type,
                'status' => $status,//null or "Pending"
                'Notify_status' => 0,  
                'final_status' => $final,           
            ]);
        }
        elseif($request->Leave_Type == 5)//5=="Continuos Days"
        {
            
            $test=$request->validate([
            'Reason' => 'required|max:255',
            'Leave_Type' => 'required',
            'start' => 'required|date|after_or_equal:today',
            'end_date' => 'required|date|after:start',
            ]);
            if(!$test)
            {
                return redirect('CallApplyLeave')
                        ->withErrors($validator)
                        ->withInput();
            }
            $to = Carbon::parse($request->start);
            $from = Carbon::parse($request->end_date);
            $Total_Leaves = $to->diffInDays($from);
            // if( $Total_Leaves >10 ) 
            // {
            //     return redirect()->route('CallApplyLeave')->with('warning',__('success.Cotinue_leave'));
            // }  
            // dd($to,$from);
            $Leave = ([
                'Leave_Type' => $request->Leave_Type,
                'start_date' => $request->start,
                'end_date' => $request->end_date,
                'Total_Leaves' => $Total_Leaves,
                'applied_date' => $applied_date,
                'Reason' => $request->Reason,
                'Emp_id' => $Emp_id,
                'Emp_type' => $Emp_type,
                'status' => $status,//null or "Pending"
                'Notify_status' => 0,
                'final_status' => $final,               
            ]);
        }
        $new = $this->leave->create($Leave);
        //Leave is Created here for mode ==1 and 0 both
        //for mode ==1 
        if($mode == 1)
        {
            $action = $this->leave->find($new->id);

            $this->Calculate($new->id,$status);// DO NOT PLACE THIS AFTER ACTION->UPDATE TO AVAOID WRONG ANSWERS

            $action->update([
                    'action_date' => Carbon::today()->toDateString(),
                    'status' => null,
                    'remark' => $remark,
                    'final_status' => 1,//taken
                ]);

        }

        // end of for mode ==1

        if($mode == 0)//apply leave
        {

            // dd($Leave);
            $From = $request->From;
            $To = $request->To;
            // dd($To);
            $details = [
                'title' => 'HRMS-Leave Apllication',
                'From' => $From,
                'Leave' => $Leave,  
                'mode' =>0,      
            ];
            
            // dd($To,$From);

            \Mail::to($To)->send(new \App\Mail\ApplyLeave($details));
            return redirect()->route('CallMyLeave')->with('success',__('success.Leave_success'));
        }
        else if($mode == 1)//Dynemically add Leave
        {
            $From = Auth::user()->email;//loged in user
            $To = $this->user->find($request->Employee);// find email for selected employee
            // dd($To);
            $details = [
                'title' => 'HRMS-Leave Apllication',
                'From' => $From,
                'Leave' => $Leave,
                'mode' =>1,        
            ];
            
            // dd($To,$From);

            \Mail::to($To->email)->send(new \App\Mail\ApplyLeave($details));
            return redirect()->route('CallMyLeave')->with('success',__('success.AddLeave_success'));
        }
        

    }

    public function EventDataTable(Request $request)
    {
        if ($request->ajax()) 
        {
            $EventData = $this->event->get();// all event List
            return Datatables::of($EventData)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    if(Auth::user()->Role == 2)
                    {
                        $actionBtn = "<a href='".route('HR.CallEditEvent',['id' => $row->id])."'><i class='fas fa-edit pr-1' data-toggle='tooltip' title='Edit Event'></i></a><a href='".route('HR.DeleteEvent',['id' => $row->id])."' data-toggle='tooltip' title='Delete Event'><i class='fas fa-trash'></i></a>";
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditEmp',['id' => $row->id])."'><i class='fas fa-eye'></i></a>";
                    }
                })
                ->addColumn('Edate',function ($row){
                    if($row->date != null)
                    {
                         $Date = date_format( date_create($row->date),'d-m-Y');
                         if($Date == Carbon::today()->format('d-m-Y'))
                         {
                            return $Date."<sup><span class='badge badge-warning text-sm '>Today</span></sup>";
                         }
                         else
                         {
                             return $Date;
                         }
                    }
                    else
                    {
                        return $row->date;
                    }
                })
                ->rawColumns(['action','Edate'])
                ->make(true); 
        }
    }
    public function getExitUsers(Request $request)
    {
        if ($request->ajax()) {
            $Userdata = $this->user->where(['where' => ['Status' => 2],"notwhere" => [ "relieving_date" ,'!=', null]]);// 2 means exit employee
            return Datatables::of($Userdata)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    if(Auth::user()->Role == 2)
                    {
                        $actionBtn = "<a href='".route('HR.CallEditEmp',['id' => $row->id])."'><i class='fas fa-edit pr-1' data-toggle='tooltip' title='Edit Employee'></i></a><a href='".route('HR.RemoveEmp',['id' => $row->id])."' data-toggle='tooltip' title='Delete Employee'><i class='fas fa-trash'></i></a><a href='".route('HR.CallExitEmpForm',['id' => $row->id])."' data-toggle='tooltip' title='Exit Employee'><i class='fas fa-sign-out-alt pl-1'></i></a>";
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditEmp',['id' => $row->id])."'><i class='fas fa-eye'></i></a>";
                    }
                })
                ->addColumn('Rdate',function ($row){
                    if($row->relieving_date != null)
                    {
                         $Relieving = date_format( date_create($row->relieving_date),'d-m-Y');
                         return $Relieving;
                    }
                    else
                    {
                        return $row->relieving_date;
                    }
                })
                ->addColumn('Gender', function($row){
                    if($row->Gender == 1)
                    {
                        return "Male";
                    }
                    else if($row->Gender == 2)
                    {
                        return "Female";
                    }
                })
                ->rawColumns(['action','Rdate','Gender'])
                ->make(true); 
        }
    }
    public function getDates($mode,Request $request)
    {
        if ($request->ajax()) {
            if($mode == 1)// Employee List
            {
                $Userdata = $this->user->getEmpBirthday();
            }
            else if($mode == 2)//New Employee List
            {
                $Userdata = $this->user->getEmpJoin();
            }
            return Datatables::of($Userdata)
                ->addIndexColumn()
                ->addColumn('Date', function($row) use ($mode){
                    if($mode == 1)//Birthday
                    {
                        if($row->DOB != null)
                        {
                         $DOB = date_format( date_create($row->DOB),'d-m-Y');
                         return $DOB;

                        }
                        else
                        {
                            return $row->DOB;
                        }
                    }
                    else if($mode == 2)// Joining Anniversary
                    {
                        if($row->DOJ != null)
                        {
                         $DOJ = date_format( date_create($row->DOJ),'d-m-Y');
                         return $DOJ;

                        }
                        else
                        {
                            return $row->DOJ;
                        }
                    }
                })
                ->rawColumns(['Date'])
                ->make(true); 
            }
    }
    public function getUsers($mode,Request $request)
    {
        // dd('hello',$mode);
        if ($request->ajax()) {
            if($mode == 1)// Employee List
            {
                $Userdata = $this->user->get();
            }
            else if($mode == 2)//New Employee List
            {
                $Userdata = $this->user->where(['where' => ['DOJ' => Carbon::today()->format('Y-m-d')]]);
            }
            else if($mode == 3)//HRList
            {
                $Userdata = $this->user->where(['where' => ['Type' => 2]]);
            }
            return Datatables::of($Userdata)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    if(Auth::user()->Role == 2)
                    {
                        $actionBtn = "<a href='".route('HR.CallEditEmp',['id' => $row->id])."'><i class='fas fa-edit pr-1' data-toggle='tooltip' title='Edit Employee'></i></a><a href='".route('HR.RemoveEmp',['id' => $row->id])."' data-toggle='tooltip' title='Delete Employee'><i class='fas fa-trash'></i></a><a href='".route('HR.CallExitEmpForm',['id' => $row->id])."' data-toggle='tooltip' title='Exit Employee'><i class='fas fa-sign-out-alt pl-1'></i></a>";
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditEmp',['id' => $row->id])."'><i class='fas fa-eye'></i></a>";
                        
                    }
                })
                ->addColumn('Status', function($row){
                    if($row->Status == 1)
                    {
                        return "<i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Active Employee'></i>";
                    }
                    else
                    {
                        return "<i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Exit Employee'></i> ";
                    }
                })
                ->addColumn('Gender', function($row){
                    if($row->Gender == 1)
                    {
                        return "Male";
                    }
                    else if($row->Gender == 2)
                    {
                        return "Female";
                    }
                })
                ->rawColumns(['action','Status','Gender'])
                ->make(true); 
        }
    }
    public function getMyLeaveData(Request $request)
    {
        $leave = $this->leave;
        if ($request->ajax()) {
            $MyLeave = $this->leave->where(['where'=>['Emp_id'=> Auth::user()->id]]);
            return Datatables::of($MyLeave)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    if($row->Notify_status == 0 && $row->status != 0)//"Pending"
                    {
                        $actionBtn = "<span class='right badge badge-warning text-dark mr-2'>New</span>";
                    }
                    else
                    {
                        $actionBtn = "";//empty
                    }
                        if($row->status == 1)//Approved
                        {
                            $actionBtn .= "<i class='fas fa-check-circle  text-success' data-toggle='tooltip' style='cursor: pointer;' title='Approved Leave'></i>";
                        }
                        else if($row->status == 2)//Not Approved
                        {
                            $actionBtn .= "<i class='fas fa-times-circle  text-danger' data-toggle='tooltip' style='cursor: pointer;' title='Rejected Leave'></i>";
                        }
                        else if($row->status == 0)//"Pending"
                        {
                            $actionBtn .= "<i class='fas fa-exclamation-circle  text-primary' data-toggle='tooltip'  style='cursor: pointer;'title='Pending Leave'></i>";
                        }

                        return $actionBtn;
                })
                ->addColumn('Leave_Type', function($row){

                     if($row->Leave_Type == 1)
                     {
                        return "FullDay";
                     }
                     else if($row->Leave_Type == 2)
                     {
                        return "First-Half Day";
                     }
                     else if($row->Leave_Type == 3)
                     {
                        return "Second-Half Day";
                     }
                     else if($row->Leave_Type == 4)
                     {
                        return "Manual Dates";
                     }
                     else if($row->Leave_Type == 5)
                     {
                        return "Continuos Days";
                     }
                     //to get name of Leave type  applied for Leave

                })
                ->addColumn('status', function($row){  
                        //option is not approve and rejected list then
                       if($row->final_status == 1)//taken
                        {
                            $StatusBtn = "<h1><i class='fas fa-envelope-square p-4 text-success' data-toggle='tooltip' style='cursor: pointer;' title='Taken'></i></h1>";
                            
                        }
                        else if($row->final_status == 0)// Not taken
                        {
                            $StatusBtn = "<h1><i class='fas fa-envelope-square p-4 text-danger' data-toggle='tooltip' style='cursor: pointer;' title='Not Taken'></i></h1>";
                            
                        }
                        return $StatusBtn;
                })
                ->addColumn('Sdate',function ($row){
                    if($row->start_date != null)
                    {
                     $start_date = date_format( date_create($row->start_date),'d-m-Y');
                     return $start_date;

                    }
                    else
                    {
                        return $row->start_date;
                    }
                })
                ->addColumn('Edate',function ($row){
                    if($row->end_date != null)
                    {
                     $end_date = date_format( date_create($row->end_date),'d-m-Y');
                     return $end_date;

                    }
                    else
                    {
                        return $row->end_date;
                    }
                })
                ->addColumn('Apply_date',function ($row){
                    if($row->applied_date != null)
                    {
                     $applied_date = date_format( date_create($row->applied_date),'d-m-Y');
                     return $applied_date;

                    }
                    else
                    {
                        return $row->applied_date;
                    }
                })
                ->addColumn('Adate',function ($row){
                    if($row->action_date != null)
                    {
                     $action_date = date_format( date_create($row->action_date),'d-m-Y');
                     return $action_date;

                    }
                    else
                    {
                        return $row->action_date;
                    }
                })
                ->rawColumns(['action','Leave_Type','status','Sdate','Edate','Apply_date','Adate'])
                ->make(true); 

        }
    }
    public function getLeaveData($option,Request $request)
    {
        $user = $this->user;
        $leave =$this->leave;
        if ($request->ajax()) {
            if(Auth::user()->Role == 2)//when login User is HR
            {           
                if($option == 0)
                {
                    //status = "Pending"
                    $LeaveData = $this->leave->where(['where'=>['status'=> 0, 'Emp_type' => 3]]);
                }
                else if($option == 1)
                {
                    $LeaveData = $this->leave->get();

                }
                else if($option == 2)
                {
                    $LeaveData = $this->leave->where(['where'=>['status'=> 1,'Emp_type' => 3]]);
                }
                else if($option == 3)
                {
                    $LeaveData = $this->leave->where(['where'=>['status'=> 2,'Emp_type' => 3]]);
                }
            }
            else
            {
                // This else Part is for the Admin to approve both HR and Employee Leaves
                if($option == 0)
                {
                    //status = "Pending"
                    $LeaveData = $this->leave->where(['where'=>['status'=> 0, 'Emp_type' => 2]]);
                }
                else if($option == 1)
                {
                    $LeaveData = $this->leave->get();

                }
                else if($option == 2)
                {
                    $LeaveData = $this->leave->where(['where'=>['status'=> 1,'Emp_type' => 2]]);
                }
                else if($option == 3)
                {
                    $LeaveData = $this->leave->where(['where'=>['status'=> 2,'Emp_type' => 2]]);
                }
            }
            return Datatables::of($LeaveData)
                ->addIndexColumn()
                ->addColumn('action', function($row) use ($option){  
                    if($option == 0)//requested
                    {
                        $actionBtn = "<a class='btn btn-primary' href='".route('getLeaveDataById',['id' => $row->id])."'>Take Action</a>";
                    }
                    else
                    {
                        if($row->status == 1)//Approved
                        {
                            $actionBtn = "<i class='fas fa-check-circle p-4 text-success' data-toggle='tooltip' style='cursor: pointer;' title='Approved Leave'></i>";
                        }
                        else if($row->status == 2)// Not Approved
                        {
                            $actionBtn = "<i class='fas fa-times-circle p-4 text-danger' data-toggle='tooltip' style='cursor: pointer;' title='Rejected Leave'></i>";
                        }
                        else if($row->status == 0)//status = "Pending"
                        {
                            $actionBtn = "<i class='fas fa-exclamation-circle p-4 text-primary' data-toggle='tooltip'  style='cursor: pointer;'title='Pending Leave'></i>";
                        }
                    }
                        return $actionBtn;
                })
                ->addColumn('Emp_name', function($row) use ($user){

                     $Emp = $user->find($row->Emp_id);
                     return $Emp->name;//to get name of employee applied for Leave

                 })
                ->addColumn('Leave_Type', function($row){

                     if($row->Leave_Type == 1)
                     {
                        return "FullDay";
                     }
                     else if($row->Leave_Type == 2)
                     {
                        return "First-Half Day";
                     }
                     else if($row->Leave_Type == 3)
                     {
                        return "Second-Half Day";
                     }
                     else if($row->Leave_Type == 4)
                     {
                        return "Manual Dates";
                     }
                     else if($row->Leave_Type == 5)
                     {
                        return "Continuos Days";
                     }
                     //to get name of Leave type  applied for Leave

                })
                ->addColumn('status', function($row) use ($option){  
                    if($option != 2 && $option != 3)
                    {
                        //option is not approve and rejected list then
                       if($row->final_status == 1)//taken
                        {
                            $StatusBtn = "<h1><i class='fas fa-envelope-square text-success' data-toggle='tooltip' style='cursor: pointer;' title='Taken'></i></h1>";
                            
                        }
                        else if($row->final_status == 0)// Not taken
                        {
                            $StatusBtn = "<h1><i class='fas fa-envelope-square  text-danger' data-toggle='tooltip' style='cursor: pointer;' title='Not Taken'></i></h1>";
                            
                        }
                    }
                    else
                    {
                        if($row->final_status == 1)//taken
                        {
                            $StatusBtn = "<h1><i class='fas fa-envelope-square text-success' data-toggle='tooltip' style='cursor: pointer;' title='Taken'></h1></i><a class='btn btn-primary' href='".route('EditLeaveAction',['id' => $row->id])."'>Swipe</a>";
                            
                        }
                        else if($row->final_status == 0)// not taken
                        {
                            $StatusBtn = "<h1><i class='fas fa-envelope-square text-danger' data-toggle='tooltip' style='cursor: pointer;' title='Not Taken'></i></h1><a class='btn btn-primary' href='".route('EditLeaveAction',['id' => $row->id])."'>Swipe</a>";
                        }
                    }
                        return $StatusBtn;
                })
                ->addColumn('Sdate',function ($row){
                    if($row->start_date != null)
                    {
                     $start_date = date_format( date_create($row->start_date),'d-m-Y');
                     return $start_date;

                    }
                    else
                    {
                        return $row->start_date;
                    }
                })
                ->addColumn('Edate',function ($row){
                    if($row->end_date != null)
                    {
                     $end_date = date_format( date_create($row->end_date),'d-m-Y');
                     return $end_date;

                    }
                    else
                    {
                        return $row->end_date;
                    }
                })
                ->addColumn('Apply_date',function ($row){
                    if($row->applied_date != null)
                    {
                     $applied_date = date_format( date_create($row->applied_date),'d-m-Y');
                     return $applied_date;

                    }
                    else
                    {
                        return $row->applied_date;
                    }
                })
                ->rawColumns(['action', 'Emp_name','Leave_Type','status','Sdate','Edate','Apply_date'])
                ->make(true); 
              
        }
    }
    public function LeaveAction($id,Request $request)
    {

        $test=$request->validate([
            'remark' => 'required|max:255',
            'status' => 'required',
            ]);
            if(!$test)
            {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }
        $remark = $request->remark;// storing remark
        $status = $request->status;//storing status
        if($status == 1)
        {
            $final_status = 1;//taken
        }
        else if($status == 2)
        {
            $final_status = 0;//not taken 
        }
        $action = $this->leave->find($id);

        $this->Calculate($id,$status);// DO NOT PLACE THIS AFTER ACTION->UPDATE TO AVAOID WRONG ANSWERS

        $action->update([
                'action_date' => Carbon::today()->toDateString(),
                'status' => $status,
                'remark' => $remark,
                'final_status' => $final_status,
            ]);

        
        //process to send mail start here

        $to = $this->user->find($action->Emp_id);
        $from = Auth::user()->email;//current user email
        $details = [
            'title' => 'HRMS-Regarding Your Leave Application and Status',
            'From' => $from,//HR id
            'data' => $action,
        ];
        // dd($to->email);
        \Mail::to($to->email)->send(new \App\Mail\ActionLeave($details));

        if($status == 1)
        {
            return redirect()->route('HR.LeaveRequest',['option' => 0])->with('success',__('success.Leave_Approved'));
        }
        else if($status == 2)
        {
            return redirect()->route('HR.LeaveRequest',['option' => 0])->with('success',__('success.Leave_Not_Approved'));
        }
        //process to send mail ends here

    }
    
    public function Calculate($id,$status)
    {
        $action = $this->leave->find($id);
        // To confirm whether the Leave_status should paid or un-paid starts here
        $dates = new DateTime();
        $months = array();//current request months
        //1= fullday 2== first-half 3= seconf-half
        if($action->Leave_Type == 1 || $action->Leave_Type == 2 || $action->Leave_Type == 3)
        {
            // dd($action->start_date);   
            $dates = date_format( date_create($action->start_date),'F Y');
            array_push($months,$dates);
            // dd($months);//month
        }
        elseif ($action->Leave_Type == 4)// manual days
        {
            $datearr = explode(',',$action->Manual_dates);
            foreach ($datearr as $key => $value) {
                 $dates = date_format( date_create($value),'F Y');
                    array_push($months,$dates);
                // if($key == 2)dd($value,date_format( date_create($value),'m Y'),$months);
            }

        }
        elseif ($action->Leave_Type == 5) // continue days
        {
            $period = CarbonPeriod::create(date_create($action->start_date), date_create($action->end_date));
               foreach ($period as $date) {
                             array_push($months,$date->format('F Y'));
                        }
                        // dd($months);
        }

        // from here the $months variable containing the months of current leaves 
                        
        // dd($months);
        $prev_month =  array(); // to count prev leaves
        $Prev_Leave = $this->leave->where(['where' => ['Emp_id' => $action->Emp_id,'final_status' => 1]]);//$Prev_Leave is for prev approve leave data //Taken
          $total_Leave = 0; // counted total leaves
          // dd($Prev_Leave);
          foreach ($Prev_Leave as $key => $leave) {
                //1= fullday 2== first-half 3= seconf-half
                if($leave->Leave_Type == 1 || $action->Leave_Type == 2 || $action->Leave_Type == 3)
                {
                    $dt = date_format( date_create($leave->start_date),'F Y');
                    array_push($prev_month,$dt);
                    $total_Leave += $leave->Total_Leaves;// to count previous leaves
                }
                else if ($leave->Leave_Type == 4)// Manual days
                {
                    $datearr = explode(',',$leave->Manual_dates);
                    foreach ($datearr as $value) {
                         $dt = date_format( date_create($value),'F Y');
                            array_push($prev_month,$dt);
                   
                    }
                     $total_Leave += $leave->Total_Leaves;// to count previous leaves

                }
                else if ($leave->Leave_Type == 5)// continue days
                {
                        $period = CarbonPeriod::create(date_create($leave->start_date), date_create($leave->end_date));
                       foreach ($period as $dt) {
                                     array_push($prev_month,$dt->format('F Y'));
                                }
                    $total_Leave += $leave->Total_Leaves;// to count previous leaves
                }

          }
        // dd($prev_month,$total_Leave);
        $Leave_status ="";
        $paid=array();
        $unpaid=array();
        // to check if there is any paid leave for the same month
          foreach ($months as $key => $value) {
                // dd($value, $prev_month,in_array($value, $prev_month));

                if(in_array($value, $prev_month))// if current leave month is in prev month list
                {
                    // dd($value, $prev_month);
                    //push in unpaid leave
                    array_push($unpaid,$value);//noted unpaid
                    // dd('unpaid',$unpaid);
                }
                else
                {
                    if($action->Leave_Type == 4 || $action->Leave_Type == 5)// manual and continue
                    {
                        //if not taken leave for same months and type manual and continous
                        if(in_array($value, $paid)) //and is in array of paid month
                        {
                            array_push($unpaid,$value);//noted unpaid
                            // dd($unpaid);
                        }
                        else
                        {
                            array_push($paid,$value);// noted paid
                        }
                            
                    }
                    else
                    {
                        //if not taken leave for same months
                        array_push($paid,$value);//noted unpaid
                    }
                    
                }

          }
        // dd('unpaid',$unpaid,'paid',$paid,$prev_month,$Prev_Leave);// to check how many paid and unpaid
          //manual and continue
          if( ($action->Leave_Type == 4 || $action->Leave_Type == 5) && sizeof($unpaid) > 0)
          {
            $Leave_status = 0;//unpaid
          }
          else
          {
            if(sizeof($unpaid) > 0)
                $Leave_status = 0;//unpaid
            else
                $Leave_status = 1;//paid
          }
          // dd($Leave_status);
          // paid unpaid Is Clear here

           // storing the data and status related to current leave application

            $action->update([
                'Leave_status' => $Leave_status,
            ]);

            //Leave table updated here

            if($status == null || $status == 1 || $status == "plus" || $status == "minus")// if dynemic leave or approved leave or not approved and taken
            {

                // process to affect the Leave balance table 
                $store_months = array();
                foreach ($months as $key => $value) {
                        $mon = explode(" ", $value);
                        array_push($store_months,$mon[0]);//noted unpaid
                }
                $store_months = array_count_values($store_months);//stores the value and it's iteration
                $year = date_format(Carbon::today(),'Y');// getting current year
                // dd($store_months,$months);
                $Employee = $this->leave_bal->where(['where' => ['Emp_id' => $action->Emp_id,'curr_year' => $year]]);
                // dd($store_months,$Employee[0]);
                // dd(array_key_exists('March',$store_months));
                foreach ($store_months as $key => $value) {
                    if(array_key_exists($key, $store_months))
                    {
                        // dd($Employee,sizeof($Employee));
                        // to update no of leaves on table monthly
                            if($action->Leave_Type == 2 || $action->Leave_Type == 3)
                            {
                                if($status == "minus")
                                {
                                    // if not taken then need to decrease and increase accordingly
                                   $store_months[$key] = $Employee[0]->$key - $value + 0.5;
                                }
                                else
                                {
                                    $store_months[$key] = $Employee[0]->$key + $value - 0.5;
                                }
                                // for half days need to decrease 0.5 of leave
                            }
                            else
                            {
                                if($status == "minus")
                                {
                                    // if not taken then need to decrease and increase accordingly
                                   $store_months[$key] = $Employee[0]->$key - $value;
                                }
                                else
                                {
                                    $store_months[$key] = $Employee[0]->$key + $value;
                                }
                                
                            }

                        
                        // $Employee[0]->update([
                        // $Employee[0]->$key => $Employee[0]->$key + $value,
                        // ]);
                    }
                }
                // dd($store_months,$months);
                $Employee[0]->update($store_months);

                // dd($this->leave_bal->where(['where' => ['Emp_id' => $action->Emp_id]]));
                // dd($this->leave_bal->where(['where' => ['Emp_id' => $action->Emp_id]]));
                // to store data in Leave balance table
                if($action->Leave_Type == 2 || $action->Leave_Type == 3)
                {
                    //need to count 0.5 for half days 
                    if(sizeof($paid) > 0)
                    {
                        if($status == "minus")
                        {
                            // if not taken then need to decrease and increase accordingly
                            $Employee[0]->update([
                            'total' => $Employee[0]->total - 0.5,
                            'paid_total' => $Employee[0]->paid_total - 0.5,
                            'current_balance' => $Employee[0]->current_balance + 0.5,
                            ]);
                        }
                        else
                        {
                            $Employee[0]->update([
                            'total' => $Employee[0]->total + 0.5,
                            'paid_total' => $Employee[0]->paid_total + 0.5,
                            'current_balance' => $Employee[0]->current_balance - 0.5,
                            ]);
                        }
                        
                    }
                    elseif(sizeof($unpaid) > 0)
                    {
                        // dd('unpaid');
                        if($status == "minus")
                        {
                            // if not taken then need to decrease and increase accordingly
                            $Employee[0]->update([
                            'total' => $Employee[0]->total - 0.5,
                            'unpaid_total' => $Employee[0]->unpaid_total - 0.5,
                            ]);
                        }
                        else
                        {
                            $Employee[0]->update([
                            'total' => $Employee[0]->total + 0.5,
                            'unpaid_total' => $Employee[0]->unpaid_total + 0.5,
                            ]);
                        }
                    }
                        
                }
                else
                {
                    if($status == "minus")
                        {
                            // if not taken then need to decrease and increase accordingly
                            // dd(sizeof($paid),$paid,sizeof($unpaid),$unpaid);
                            $Employee[0]->update([
                                'total' => $Employee[0]->total - sizeof($paid) - sizeof($unpaid),
                                'paid_total' => $Employee[0]->paid_total - sizeof($paid),
                                'unpaid_total' => $Employee[0]->unpaid_total - sizeof($unpaid),
                                'current_balance' => $Employee[0]->current_balance + sizeof($paid),
                            ]);
                        }
                        else
                        {
                            $Employee[0]->update([
                                'total' => $Employee[0]->total + sizeof($paid) + sizeof($unpaid),
                                'paid_total' => $Employee[0]->paid_total + sizeof($paid),
                                'unpaid_total' => $Employee[0]->unpaid_total + sizeof($unpaid),
                                'current_balance' => $Employee[0]->current_balance - sizeof($paid),
                            ]);
                        }
                }
                // dd($Leave_status,$paid,$unpaid);
                // for status == "plus" Means if swiping status for case not-approved and taken
                if($status=="plus")
                {
                    $action->update([
                        'final_status' => 1,// taken noted
                    ]);

                }
                else if($status == "minus")
                {
                    // "minus" means swiping status for case approved and not taken
                    $action->update([
                        'final_status' => 0,//not taken noted
                    ]);
                }
                // Leave balance table updated here
            }
    }

     public function profile()
     {
        $profile = $this->user->find(Auth::user()->id);
        return view('data.profile',compact('profile'));

     }
     public function change()
     {
        return view('data.change');
     }
    public function changePassword(Request $request)
    {
        $validatedData = $request->validate([
            'old_password' => 'required',
            'password'     => 'required|min:8',
            'confirm' => 'required|same:password',
                ]);

        if((\Hash::check($request->old_password, Auth::user()->password)) == false) 
        {
            return back()->with('warning',__('success.Wrong_old_password'));
        }
        if(!$validatedData)
            return redirect('admin.change')
                    ->withErrors($validator)
                    ->withInput();
        
        $record = $this->user->find(Auth::user()->id);//repository

        $record->update([
            'password' => Hash::make($request->password),
        ]);
        return redirect()->route('admin.dashboard');

    }
    public function saveSetting(Request $request)
    {

        foreach ($request->key as $name => $value) {
            $this->setting->create([
                    'key' => $name,
                    'value' => $value,
                    ]);
        }
    
            return back()->with('success',__('success.setting_saved'));
        

    }

}
