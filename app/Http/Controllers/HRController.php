<?php

namespace App\Http\Controllers;
use App\Repositories\User\UserInterface;
use App\Repositories\Setting\SettingInterface;
use App\Repositories\Type\TypeInterface;
use App\Repositories\Role\RoleInterface;
use App\Repositories\Admin\AdminInterface;
use App\Repositories\Leave\LeaveInterface;
use App\Repositories\Balance\BalanceInterface;
use App\Repositories\Event\EventInterface;
use App\Repositories\HolidayList\HolidayListInterface;
use Illuminate\Http\Request;
use Excel;
use DataTables; //for Yajra
use Illuminate\Support\Facades\Auth;
use App\Exports\UsersExport;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr; // for array helpers
use Carbon\Carbon; // for date and time 


class HRController extends Controller
{
    private $user;

    private $setting;

    private $type;

    public $new;

    private $leave;

    private $leave_bal;

    private $event;

    private $holiday;

	public function __construct(
        UserInterface $user,
        SettingInterface $setting,
        TypeInterface $type,
        LeaveInterface $leave,
        BalanceInterface $leave_bal,
        EventInterface $event,
        HolidayListInterface $holiday
    )
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->setting = $setting;
        $this->type = $type;
        $this->leave = $leave;
        $this->leave_bal = $leave_bal;
        $this->event = $event;
        $this->holiday = $holiday;
    }
    public function CallEditEmp($id)
    {
        // dd($id);
        $data = $this->user->find($id);//repo
        return view('HR.EditEmp.EditEmp',compact('data'));
    }
    public function CallExitEmployee($id)
    {
        $data = $this->user->find($id);//repo
        return view('HR.EditEmp.ExitEmpForm',compact('data'));
    }
    public function CallHolidayEdit($id)
    {
        $data = $this->holiday->find($id);
        $mode = 1;// edit mode
        return view('HR.HolidayForm',compact('data','mode'));

    }
    public function HolidayDataTable(Request $request)
    {
        if ($request->ajax()) 
        {
            $EventData = $this->holiday->get();// all event List
            return Datatables::of($EventData)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    if(Auth::user()->Role == 2)
                    {
                        $actionBtn = "<a href='".route('HR.CallHolidayEdit',['id' => $row->id])."'><i class='fas fa-edit pr-1' data-toggle='tooltip' title='Edit Event'></i></a><a href='".route('HR.DeleteHoliday',['id' => $row->id])."' data-toggle='tooltip' title='Delete Event'><i class='fas fa-trash'></i></a>";
                        return $actionBtn;
                    }
                    else
                    {
                        return "<a href='".route('HR.CallEditEmp',['id' => $row->id])."'><i class='fas fa-eye'></i></a>";
                    }
                })
                ->addColumn('Fdate',function ($row){
                    if($row->date != null)
                    {
                         $Date = date_format( date_create($row->date),'d-m-Y');
                         if($Date == Carbon::today()->format('d-m-Y'))
                         {
                            return $Date."<sup><span class='badge badge-warning text-sm '>Today</span></sup>";
                         }
                         else
                         {
                             return $Date;
                         }
                    }
                    else
                    {
                        return $row->date;
                    }
                })
                ->rawColumns(['action','Fdate'])
                ->make(true); 
        }
    }
    public function SaveEmpStatus($id,Request $request)
    {
        $test=$request->validate([
            'relieving_date' => 'required',
            'status' => 'required',            
        ]);
        if(!$test)
        {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }
            $Employee = $this->user->find($id);
            $Employee->update([
                'Status' => $request->status,
                'relieving_date' => $request->relieving_date,
            ]);
        return redirect()->route('HR.EmpList',['mode' => 1])->with('success',__('success.Employee_status'));
    }
    public function EditLeaveData($code)
    {
        // $List = $this->user->get();// all user data 
        $List = $this->user->where([ 'where' => ['type'=> 3] ]);
        $mode =1;// add Leave mode
        if($code == 1)
            return view('Emp.ApplyLeave',compact('List','mode'));
        if($code == 2)
            return view('layouts.Leave.IncrementLeaveBal',compact('List'));
        if($code == 3)
        {
            return view('layouts.Leave.LastLeaveBal',compact('List'));
        }
    }
    public function getLastBalanceById(Request $request)
    {
        $year = date_format(Carbon::today(),'Y');// getting current year
        $data = $this->leave_bal->where(['where' => ['Emp_id' => $request->id ,'curr_year' => $year]]);// employee current data 
        // dd($data[0]->last_year_balance,$year,$id);
        return json_encode($data[0]->last_year_balance);
    }
    public function EditLeaveBal(Request $request)
    {
         $test=$request->validate([
            'Employee' => 'required',
            'Number' => 'required|numeric|max:5',
            'Increment_Decrement' => 'required',
            
        ]);
        if(!$test)
        {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $EmployeeBal = $this->leave_bal->find($request->Employee);
                // dd('bay',$this->leave_bal->find($request->Employee));

        if($request->Increment_Decrement == "Increment")
        {
            $EmployeeBal[0]->update([
            'current_balance' => $EmployeeBal[0]->current_balance + $request->Number,
            ]);
        }
        else if($request->Increment_Decrement == "Decrement")
        {
            $EmployeeBal[0]->update([
            'current_balance' => $EmployeeBal[0]->current_balance - $request->Number,
            ]);
        }
        
        return redirect()->back()->with('success',__('success.Increment_success'));

    }
    public function EditLastBal(Request $request)
    {
        $test=$request->validate([
            'Employee' => 'required',
            'Number' => 'required|numeric',            
        ]);
        if(!$test)
        {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $year = date_format(Carbon::today(),'Y');// current year
        $Last_bal = $this->leave_bal->where(['where' => ['Emp_id' => $request->Employee,'curr_year' => $year]]);

        if($request->Number > 12 )
        {
            // last year balance must be less than 12 
            // dd($request->Number);
            return redirect()->back()->with('warning',__('success.EditLast_Number'));
        }
        else
        {
            $Last_bal[0]->update([
                'last_year_balance' => $request->Number,
            ]);
            return redirect()->back()->with('success',__('success.EditLast_Success'));
        }
    }
    public function SaveHoliday($mode,Request $request)
    {
        // code to create events
        $test=$request->validate([
            'Festival' => 'required|max:255',
            'date' => 'required',
        ]);
        if(!$test)
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $day = date_create($request->date)->format('l');
        $holidayData = ([
            'festival' => $request->Festival,
            'date' => $request->date,
            'day' => $day,
        ]);
        // dd($event);
        if($mode == 0) //insert
        {
            $this->holiday->create($holidayData);
            return redirect()->route('HolidayList')->with('success',__('success.Holiday_Added'));
        }
        else if($mode == 1) //update
        {
            $new = $this->holiday->find($request->id);
            $new->update($holidayData);
            return redirect()->route('HolidayList')->with('success',__('success.Holiday_Updated'));
        }

    }
    public function SaveEvents($mode,Request $request)
    {
        // code to create events
        $test=$request->validate([
            'EventTitle' => 'required|max:255',
            'EventDate' => 'nullable',
            'startTime' => 'nullable',
            'endTime' => 'nullable',
            'Description' => 'nullable|max:1000',

        ]);
        if(!$test)
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $event = ([
            'title' => $request->EventTitle,
            'date' => $request->EventDate,
            'start_time' => $request->startTime,
            'end_time' => $request->endTime,
            'description' => $request->Description,
        ]);
        // dd($event);
        if($mode == 0) //insert
        {
            $this->event->create($event);
            return redirect()->route('HR.EventsList')->with('success',__('success.Event_Added'));
        }
        else if($mode == 1) //update
        {
            $new = $this->event->find($request->id);
            $new->update($event);
            return redirect()->route('HR.EventsList')->with('success',__('success.Event_Edited'));
        }

    }
    public function CallEditEvent($id)
    {
        //code to call addevent view in edit mode
            $mode = 1; // update
            $event = $this->event->find($id);
            return view('data.AddEvents',compact('mode','event'));
    }

    public function deleteEvent($id)
    {
        // $data = User::find($id);
        $data = $this->event->delete($id);
        return redirect()->route('HR.EventsList')->with('success',__('success.Event_Deleted'));

    }
    public function DeleteHoliday($id)
    {
        $data = $this->holiday->delete($id);
        return redirect()->route('HolidayList')->with('success',__('success.Holiday_Deleted'));
    }
    public function AddEmpPrimary(Request $request)
    {
        // dd($this->id);
        $test=$request->validate([
            'name' => 'required|max:255',
            'Contact' => 'required|numeric|digits:10',
            'email' => 'required|email|unique:users',
            'Designation' => 'required',
            'Salary' => 'required|numeric',
            'DOJ' => 'required|before_or_equal:today',
            'Email_password' => 'required|max:255',
        ]);
        if(!$test)
        {
            return redirect('index')
                    ->withErrors($validator)
                    ->withInput();
        }
        $Employee = ([
            'name' => $request->name,
            'Contact' => $request->Contact,
            'email' => $request->email,
            'Designation' => $request->Designation,
            'Salary' => $request->Salary,
            'DOJ' => $request->DOJ,
            'Email_password' => $request->Email_password,
            'password' => Hash::make($request->Email_password),
            'type' => 3,
            'Role' => 3,
            'Status' =>1,
        ]);
        //creating user
        $user_created = $this->user->create($Employee);

        // Employee is created here
        // Leave Balance entry starts here
        $this->leave_bal->create([
            'Emp_id' => $user_created->id,
            'curr_year' => date_format(Carbon::today(),'Y'),
            'last_year_balance' => 0,// 0 for new employee can add later on 
        ]);
        

        //Leave Balance entry ends here
        // $emp = $this->user->where([ 'where' => ['email'=>$request->email] ]);
        // $this->new = $emp[0]->id;
        // dd($this->new);

        $details = [
        'title' => 'HRMS-Login with your Credentials',
        'body' => 'Please login with following Credentials',
        'password' => $request->Email_password,
        'email' => $request->email,
        ];
   
        \Mail::to($request->email)->send(new \App\Mail\SendMail($details));

        return redirect()->route('HR.CallEditEmp',['id' => $user_created->id])->with('success',__('success.Record_Inserted'));

    }


    public function EditEmployee($id,Request $request)
    {
        // dd($request);
        if($request->form_name == 'Primary')
        {
            $test=$request->validate([
            'name' => 'required|max:255',
            'Contact' => 'required|numeric|digits:10',
            'Designation' => 'required',
            'Salary' => 'required|numeric',
            'DOJ' => 'required|before_or_equal:today',
            'Email_password' => 'required|max:255',
            ]);
            if(!$test)
            {
                return redirect('index')
                        ->withErrors($validator)
                        ->withInput();
            }

            $Employee = ([
                'name' => $request->name,
                'Contact' => $request->Contact,
                'Designation' => $request->Designation,
                'Salary' => $request->Salary,
                'DOJ' => $request->DOJ,
                'Email_password' => $request->Email_password,
            ]);
             // dd($this->new);
            $Primary = $this->user->find($id);
            $Primary->update($Employee);
        }

        // for personal
        if($request->form_name == 'Personal')
        {
            $test=$request->validate([
            'Personal_Email' => 'nullable|email',
            'Another_cont' => 'nullable|numeric|digits:10',
            'Address' => 'nullable|max:255',
            'Education' => 'nullable|max:255',
            'Course' => 'nullable|max:255',
            'Gender' => 'nullable',
            'DOB' => 'nullable|before:today',
            ]);
            if(!$test)
            {
                return redirect('index')
                        ->withErrors($validator)
                        ->withInput();
            }

            $Employee = ([
                'Personal_Email' => $request->Personal_Email,
                'Another_cont' => $request->Another_cont,
                'Address' => $request->Address,
                'Education' => $request->Education,
                'Course' => $request->Course,
                'Gender' => $request->Gender,
                'DOB' => $request->DOB,
                
            ]);
            // dd($this->new);
            $Personal = $this->user->find($id);
            $Personal->update($Employee);
        }
        // for social
        if($request->form_name == 'Social')
        {
            $test=$request->validate([
            'SkypeID' => 'nullable|max:255',
            'Skype_password' => 'nullable',
            'TeamloggerID' => 'nullable|max:255',
            'Team_password' => 'nullable',
            
            ]);
            if(!$test)
            {
                return redirect('index')
                        ->withErrors($validator)
                        ->withInput();
            }

            $Employee = ([
                'SkypeID' => $request->SkypeID,
                'Skype_password' => $request->Skype_password,
                'TeamloggerID' => $request->TeamloggerID,
                'Team_password' => $request->Team_password,
                ]);
            $Social = $this->user->find($id);
            $Social->update($Employee);
        }
        // for bank
        if($request->form_name == 'Bank')
        {
            $test=$request->validate([
            'Bank_name' => 'nullable|max:255',
            'Acc_holder_name' => 'nullable|max:255',
            'Acc_no' => 'nullable|numeric',
            'IFSC' => 'nullable',
            ]);
            if(!$test)
            {
                return redirect('index')
                        ->withErrors($validator)
                        ->withInput();
            }

            $Employee = ([
                'Bank_name' => $request->Bank_name,
                'Acc_holder_name' => $request->Acc_holder_name,
                'Acc_no' => $request->Acc_no,
                'IFSC' => $request->IFSC,
            ]);
            $Bank = $this->user->find($id);
            $Bank->update($Employee);
        }
        // for salary
        if($request->form_name == 'Salary')
        {
            $test=$request->validate([
            'Experience' => 'nullable|max:255',
            'Inc_Time_Period' => 'nullable',
            'Last_Inc_Date' => 'nullable',
            'Last_Inc_Amount' => 'nullable',
            ]);
            if(!$test)
            {
                return redirect('index')
                        ->withErrors($validator)
                        ->withInput();
            }

            $Employee = ([
                'Experience' => $request->Experience,
                'Inc_Time_Period' => $request->Inc_Time_Period,
                'Last_Inc_Date' => $request->Last_Inc_Date,
                'Last_Inc_Amount' => $request->Last_Inc_Amount,
            ]);
            $Salary = $this->user->find($id);
            $Salary->update($Employee);
        }

        //for Formal
        if($request->form_name == 'Formal')
        {
            $test=$request->validate([
            'Doc_code' => 'nullable|max:255',
            'Doc_status' => 'nullable',
            'NDA_status' => 'nullable',
            ]);
            if(!$test)
            {
                return redirect('index')
                        ->withErrors($validator)
                        ->withInput();
            }

            $Employee = ([
                'Doc_code' => $request->Doc_code,
                'Doc_status' => $request->Doc_status,
                'NDA_status' => $request->NDA_status,
            ]);
            $Formal = $this->user->find($id);
            $Formal->update($Employee);
        }
        // redirection link with success message
        return redirect()->route('HR.CallEditEmp',['id' => $id])->with('success',__('success.Record_updated'));

    }

    public function RemoveEmployee($id)
    {
        $delete = $this->user->find($id);
        $delete->delete();
        return redirect()->route('EmpList')->with('fail',__('success.Record_Deleted'));

    }
    public function Export($format)
    {
        $heading = array("id", "Name", "Email Id", "Designation", "Gender", "Type", "Role", "Date Of Joining", "Date of Birth", "Contact No", "Alternate No", "Email Password", "Skype Id", "Skype Password","TeamLogger ID", "TeamLogger Password", "Education", "Course", "Address", "Personal Email id", "Current Salary", "Experience","Increment Time period","Last increment date","Last increment Amount", "Bank name", "Bank a/c holder name", "'Bank A/c no", "IFSC Code", "NDA Status", "Document Status", "Document Code", "Status","Relieving Date" );
        $data = $this->user->select(['select' => ['id','name','email','Designation','Gender','type','role','DOJ','DOB','Contact', 'Another_cont', 'Email_password', 'SkypeID' ,'Skype_password', 'TeamloggerID','Team_password' ,'Education','Course','Address','Personal_Email','Salary','Experience','Inc_Time_Period','Last_Inc_Date','Last_Inc_Amount','Bank_name','Acc_holder_name','Acc_no','IFSC','NDA_status','Doc_status','Doc_code','Status','relieving_date']]);
        if($format == 'CSV')
        {
            return Excel::download(new UsersExport($data,$heading), 'EmployeeData.csv');
        }
        else if($format == 'Excel')
        {
            return Excel::download(new UsersExport($data,$heading), 'EmployeeData.xlsx');
        }
    }

   
    public function ImportHolidayList(Request $request)
    {
        $format =array(
            'festival' => 'Festivals',
            'date' => 'Date',
            'day' => 'Day',
        );
        // dd($format);
        if ($request->input('submit') != null ){

              $file = $request->file('file');

              // File Details 
              $filename = $file->getClientOriginalName();
              $extension = $file->getClientOriginalExtension();
              $tempPath = $file->getRealPath();
              $fileSize = $file->getSize();
              $mimeType = $file->getMimeType();

              // Valid File Extensions
              $valid_extension = array("csv");

              // 2MB in Bytes
              $maxFileSize = 2097152; 

              // Check file extension
            if(in_array(strtolower($extension),$valid_extension)){

                // Check file size
                if($fileSize <= $maxFileSize){

                      // File upload location
                      $location = 'uploads';

                      // Upload file
                      $file->move($location,$filename);
                      // Import CSV to Database
                      $filepath = public_path($location."/".$filename);
                        // dd($filepath);
                      // Reading file
                      $file = fopen($filepath,"r");

                      $importData_arr = array();
                      $insertData = array();
                      $fields = array();    
           
                      $i = 0;
                      $fieldNum = 0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) 
                    {
                         $num = count($filedata);

                         // Skip first row (Remove below comment if you want to skip the first row)
                        if($i == 0)
                        {
                            $fieldNum = $num;
                            for ($c=0; $c < $num; $c++) 
                            {
                                $fields[$i][] = $filedata [$c];
                                // if any field is not matching in csv file
                                if(! (in_array($filedata [$c], $format)) )
                                {
                                    return redirect()->route('HR.CallImportHolidays')->with('warning',__('success.Invalid_Format'));
                                }
                                
                            }
                            // dd(sizeof($format));
                            foreach ($format as $fieldName)
                            {
                                // if any field is missing in csv file
                                if(! (in_array($fieldName, $fields[0])) )
                                {
                                    // dd($fieldName,$fields[0]);
                                    return redirect()->route('HR.CallImportHolidays')->with('warning',__('success.Missing_Field'));                              
                                }
                            }
                            $i++;
                            continue;
                        
                        }
                        else
                        {
                            // dd($num);
                            //if all feilds are not having same number of data or missing some coma then
                            if($fieldNum != $num)
                            {
                                return redirect()->route('HR.CallImportHolidays')->with('warning',__('success.Missing_Field'));
                            }
                            else
                            {
                                for ($c=0; $c < $num; $c++) {
                                    $importData_arr[$i][ $fields[0][$c] ] = $filedata [$c];
                                    // if($c==24)dd($i,$importData_arr[$i],$filedata [$c],$fields[0]);
                                }
                                // dd($importData_arr);

                            }
                        }

                         $i++;
                    }
                      fclose($file);
                     // Insert to MySQL database
                     // dd($fields,$importData_arr);
                      $i=0;
                    foreach($importData_arr as $importData)
                    {
                        // $temp =explode(',', $importData);
                        // dd($importData);
                        foreach ($format as $key => $value) 
                        {
                            if(in_array($value, $fields[0]))
                            {
                                if( empty($importData[$value]) )
                                {
                                    // dd($key,$importData[$value]);
                                    $importData[$value] = null;
                                }
                                if($key == 'date')
                                {
                                    $date = str_replace('/', '-', $importData[$value]);
                                    $dateVal = date_format( date_create($date),'Y-m-d');
                                    $insertData= Arr::add($insertData, $key , $dateVal);
                                }
                                else
                                {
                                    $insertData= Arr::add($insertData, $key , $importData[$value]);
                                }                               
                            }
                            else
                            {
                               $insertData= Arr::add($insertData, $key , null);
                            }

                        }

                        $i++;
                        $emp = $this->holiday->create($insertData);
                        // Employee is created here
                        $insertData = null;                      

                    }
                    // dd($insertData);

                       // dd($insertData);
                        return redirect()->route('HR.CallImportHolidays')->with('success',__('success.Import_Sucess'));              
                }
                else{
                    return redirect()->route('HR.CallImportHolidays')->with('warning',__('success.FILE_Large'));

                }

            }
                else{
                    return redirect()->route('HR.CallImportHolidays')->with('warning',__('success.Invalid_FILE'));

                }

        }
    }

    public function ImportEmployee(Request $request)
    {
        $format =array(
            'name' => 'Name',
            'Designation' => 'Designation',
            'DOJ' => 'Date Of Joining',
            'DOB' => 'Date of Birth',
            'Contact' => 'Contact No',
            'Another_cont' => 'Alternate No',
            'email' => 'Email Id',
            'Email_password' => 'Password',
            'SkypeID' => 'Skype Id',
            'Skype_password' => 'Skype Password',
            'TeamloggerID' => 'TeamLogger ID',
            'Team_password' => 'TeamLogger Password',
            'Education' => 'Education',
            'Course' => 'Course',
            'Experience' => 'Experience',
            'Address' => 'Address',
            'Personal_Email' => 'Personal Email id',
            'Salary' => 'Current Salary',
            'Inc_Time_Period' => 'Increment Time period',
            'Last_Inc_Date' => 'Last increment date',
            'Last_Inc_Amount' => 'Last increment Amount',
            'Bank_name' => 'Bank name',
            'Acc_holder_name' => 'Bank a/c holder name',
            'Acc_no' => 'Bank A/c no',
            'IFSC' => 'IFSC Code',
            'Gender' => 'Gender',
            'NDA_status' => 'NDA Status',
            'Doc_status' => 'Document Status',
            'Doc_code' => 'Document Code',
            'relieving_date' => 'Relieving Date',
        );
        // dd($format);
        if ($request->input('submit') != null ){

              $file = $request->file('file');

              // File Details 
              $filename = $file->getClientOriginalName();
              $extension = $file->getClientOriginalExtension();
              $tempPath = $file->getRealPath();
              $fileSize = $file->getSize();
              $mimeType = $file->getMimeType();

              // Valid File Extensions
              $valid_extension = array("csv");

              // 2MB in Bytes
              $maxFileSize = 2097152; 

              // Check file extension
            if(in_array(strtolower($extension),$valid_extension)){

                // Check file size
                if($fileSize <= $maxFileSize){

                      // File upload location
                      $location = 'uploads';

                      // Upload file
                      $file->move($location,$filename);
                      // Import CSV to Database
                      $filepath = public_path($location."/".$filename);
                        // dd($filepath);
                      // Reading file
                      $file = fopen($filepath,"r");

                      $importData_arr = array();
                      $insertData = array();
                      $fields = array();    
           
                      $i = 0;
                      $fieldNum = 0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) 
                    {
                         $num = count($filedata);

                         // Skip first row (Remove below comment if you want to skip the first row)
                        if($i == 0)
                        {
                            $fieldNum = $num;
                            for ($c=0; $c < $num; $c++) 
                            {
                                $fields[$i][] = $filedata [$c];
                                // if any field is not matching in csv file
                                if(! (in_array($filedata [$c], $format)) )
                                {
                                    return redirect()->route('HR.CallImportEmp')->with('warning',__('success.Invalid_Format'));
                                }
                                
                            }
                            // dd(sizeof($format));
                            foreach ($format as $fieldName)
                            {
                                // if any field is missing in csv file
                                if(! (in_array($fieldName, $fields[0])) )
                                {
                                    // dd($fieldName,$fields[0]);
                                    return redirect()->route('HR.CallImportEmp')->with('warning',__('success.Missing_Field'));                              
                                }
                            }
                            $i++;
                            continue;
                        
                        }
                        else
                        {
                            // dd($num);
                            //if all feilds are not having same number of data or missing some coma then
                            if($fieldNum != $num)
                            {
                                return redirect()->route('HR.CallImportEmp')->with('warning',__('success.Missing_Field'));
                            }
                            else
                            {
                                for ($c=0; $c < $num; $c++) {
                                    $importData_arr[$i][ $fields[0][$c] ] = $filedata [$c];
                                    // if($c==24)dd($i,$importData_arr[$i],$filedata [$c],$fields[0]);
                                }
                                // dd($importData_arr);

                            }
                        }

                         $i++;
                    }
                      fclose($file);
                     // Insert to MySQL database
                     // dd($fields,$importData_arr);
                      $i=0;
                    foreach($importData_arr as $importData)
                    {
                        // $temp =explode(',', $importData);
                        // dd($importData);
                        foreach ($format as $key => $value) 
                        {
                            if(in_array($value, $fields[0]))
                            {
                                if( empty($importData[$value]) )
                                {
                                    // dd($key,$importData[$value]);
                                    $importData[$value] = null;
                                }
                                if( ($key == 'DOJ' || $key == 'DOB' || $key == 'Last_Inc_Date' || $key == 'relieving_date') && (!empty($importData[$value])) )
                                {
                                    $date = str_replace('/', '-', $importData[$value]);
                                    $dateVal = date_format( date_create($date),'Y-m-d');
                                    $insertData= Arr::add($insertData, $key , $dateVal);
                                }
                                else
                                {
                                    $insertData= Arr::add($insertData, $key , $importData[$value]);
                                }

                            }
                            else
                            {
                               $insertData= Arr::add($insertData, $key , null);
                            }

                        }
                            $insertData = Arr::add($insertData, 'type' , 3);//type employee
                            $insertData = Arr::add($insertData, 'Role' , 3);//role employee
                            $insertData = Arr::add($insertData, 'Status' , 1);// employee is active
                            // $insertData = Arr::add($insertData, 'password', Hash::make(str_random(8)));//generating random password

                        $i++;
                        $emp = $this->user->create($insertData);
                        // Employee is created here
                        //Leave Balance Entry starts here
                        $this->leave_bal->create([
                            'Emp_id' => $emp->id,
                            'curr_year' => date_format(Carbon::today(),'Y'),
                            'last_year_balance' => 0,// 0 for new employee can add later on 
                        ]);
                        // Leave Balance entry ends here
                        $insertData = null;                      

                    }
                    // dd($insertData);

                       // dd($insertData);
                        return redirect()->route('HR.CallImportEmp')->with('success',__('success.Import_Sucess'));              
                }
                else{
                    return redirect()->route('HR.CallImportEmp')->with('warning',__('success.FILE_Large'));

                }

            }
                else{
                    return redirect()->route('HR.CallImportEmp')->with('warning',__('success.Invalid_FILE'));

                }

        }
    }
}
