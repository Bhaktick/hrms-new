<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use App\Repositories\Leave\LeaveRepository;
use App\Repositories\Event\EventRepository;
use Illuminate\Support\ServiceProvider;
use App\Http\View\Composers\NotifyComposers;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot(LeaveRepository $leave,EventRepository $event)
    {
        View::composer(['layouts.navbar'], NotifyComposers::class);
        // View::composer('*', NotifyComposers::class);

        // View::composer(['dashboard', 'profile'], function($view) {
        //     $view->with('key', 'value');
        // });
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }
}