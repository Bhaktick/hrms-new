<?php
use App\Repositories\Type\TypeInterface;

if(!function_exists('getUserType'))
{
	function getUserType(TypeInterface $type)
	{
		$usertype = $type->find(Auth::user()->type);//finding the name of type user
		
		return $usertype->name;
	}
}


?>