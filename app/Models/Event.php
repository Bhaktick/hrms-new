<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
   	protected $table = 'events';

    protected $fillable = [
        'title', 'date', 'start_time', 'end_time', 'description'
    ];
}
