<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
	protected $table = 'types';

    protected $fillable = [
        'name',
    ];
    public function user()
    {
        return $this->belongsTo('User');
    }
}
