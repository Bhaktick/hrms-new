<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $table = 'candidates';
    protected $fillable = [
        'can_name', 'can_email', 'can_designantion', 'can_contact','can_experience', 'interviewer','practical_interviewer','interview_date','interview_time','interview_type','practical_date','practical_time','practical_type','technical_status','practical_status','hr_status','joining_date','notice_period','remark',
    ];
}
