<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
	protected $table = 'leaves';
    protected $fillable = [
        'Leave_Type', 'start_date', 'end_date', 'Manual_dates', 'Total_Leaves','applied_date','Reason','Emp_id','Emp_type','action_date','status','Notify_status','Leave_status','remark','final_status'
    ];
}
