<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HolidayList extends Model
{
    protected $table = 'holiday_lists';

    protected $fillable = [
        'festival', 'date', 'day'
    ];
}
