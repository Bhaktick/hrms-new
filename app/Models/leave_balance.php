<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class leave_balance extends Model
{
    protected $table = 'leave_balances';
    protected $fillable = [
        'Emp_id', 'curr_year', 'last_year_balance', 'January', 'February','March','April','May','June','July','August','September','October','November','December','total','paid_total','unpaid_total','current_balance',
    ];
}
