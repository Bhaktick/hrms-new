<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();// generate required routes for authentifications

	Route::get('/', function () {
	    return view('welcome');
	});
Route::group(['middleware' => 'auth'], function () {
	// for no access page 403 error
	Route::get('/NoAccess', function() {
		     return view('layouts.role.403_Page');
		})->name('NoAccess');
	
	Route::get('users', 'UserController@index');

	Route::any('/dashboard','HomeController@index')->name('admin.dashboard');// for dashboard page

	Route::get('getLeaveDataById/{id}','HomeController@getLeaveDataById')->name('getLeaveDataById');//to get Leave data by id for model 

	Route::any('EmpdataTable/{mode}','HomeController@getUsers')->name('EmpdataTable');

	Route::any('HolidayDataTable','HRController@HolidayDataTable')->name('HolidayDataTable');
	
	Route::any('getDates/{mode}','HomeController@getDates')->name('getDates');

	Route::any('EventDataTable','HomeController@EventDataTable')->name('EventDataTable');

	Route::any('/EmpList', function() {
			     return view('HR.EmpList');
			})->name('HR.EmpList');

	Route::any('/HRList', function() {
			     return view('HR.HRList');
			})->name('HRList');

	Route::any('/NewEmpList', function() {
			     return view('HR.NewEmpList');
			})->name('HR.NewEmpList');

	Route::any('/TodayHBD', function() {
			     return view('HR.TodayHBD');
			})->name('TodayHBD');

	Route::any('/JoinAnniversary', function() {
			     return view('HR.JoiningAnniversary');
			})->name('JoinAnniversary');

	Route::get('/HolidayList', function() {
		     return view('data.HolidayList');
		})->name('HolidayList'); // to call add employee view

	Route::get('/profile', 'HomeController@profile')->name('admin.profile');
	Route::any('/change', 'HomeController@change')->name('admin.change');
	Route::any('/changePassword', 'HomeController@changePassword')->name('admin.changePassword');

	Route::any('/CallApplyLeave/{mode}', function($mode) {
				$mode = $mode;
				$List = array();
			    return view('Emp.ApplyLeave',compact('List','mode'));// to call leave application form
			})->name('CallApplyLeave');

	Route::any('/ApplyLeave/{mode}','HomeController@ApplyLeave')->name('ApplyLeave');// to apply Leave form and add Leave  dynemically



	Route::get('/getMyLeaveData', 'HomeController@getMyLeaveData')->name('getMyLeaveData');//to get myleave data

	Route::any('CallMyLeave', function() {
			     return view('layouts.Leave.MyLeave');// to call leave myLeave data
			})->name('CallMyLeave');

	Route::any('SaveNotify','HomeController@SaveNotify')->name('SaveNotify');// to save notification status


	Route::any('/CallEditEmp/{id}','HRController@CallEditEmp')->name('HR.CallEditEmp');//to create employee
	
	Route::get('EditLeaveAction/{id}','HomeController@EditLeaveAction')->name('EditLeaveAction');// to save changes from getLeaveDataById 
		
	Route::any('getCandidateList','RecruitController@getCandidateList')->name('getCandidateList');// to get candidate data

		Route::any('getTScheduleList','RecruitController@getTScheduleList')->name('getTScheduleList');// to get Technical Schedule data

		Route::any('getPScheduleList','RecruitController@getPScheduleList')->name('getPScheduleList');// to get Practical Schedule data

		Route::any('getCandidateStatus','RecruitController@getCandidateStatus')->name('getCandidateStatus');// to get candidate Status data

		Route::any('getSelectedList','RecruitController@getSelectedList')->name('getSelectedList');// to get Selected candidate  data

		Route::get('/CallExitEmployee', function() {
			     return view('HR.ExitEmp');
			})->name('CallExitEmployee'); // to call exit employee view

		Route::any('ExitEmpData','HomeController@getExitUsers')->name('ExitEmpData');//to get exit data

		Route::any('EventsList',function(){
				return view('data.EventList');
			})->name('HR.EventsList');// to List Events

		Route::get('candidateList',function(){
				return view('HR.Recruit.candidateList');
			})->name('HR.candidateList');// to call candidate list

		Route::get('TechnicalSchedule',function(){
			return view('HR.Recruit.TechnicalSchedule');
		})->name('HR.TechnicalSchedule');// to call candidate Techincal Schedule List

		Route::get('PracticalSchedule',function(){
			return view('HR.Recruit.PracticalSchedule');
		})->name('HR.PracticalSchedule');// to call candidate Practical Schedule List

		Route::get('CandidateStatus',function(){
			return view('HR.Recruit.CandidateStatus');
		})->name('HR.CandidateStatus');// to call  CandidateStatus

		Route::get('Selected',function(){
			return view('HR.Recruit.Selected');
		})->name('HR.Selected');// to call Selected candidate list

		Route::any('/LeaveData/{option}','HomeController@getLeaveData')->name('HR.LeaveData');// to get Leave data option wise

		Route::any('/LeaveRequest/{option}',function($option) {
				return view('layouts.Leave.LeaveData',compact('option'));
			})->name('HR.LeaveRequest'); //to call view for display leave data
	
});
Route::group(['middleware' => ['auth','is_admin'],'prefix' => 'admin'], function () {
		
		Route::get('/home', 'HomeController@index')->name('admin.home');
		Route::get('/goEdit/{id}', 'HomeController@edit')->name('admin.goEdit');
		Route::any('/updateUser', 'HomeController@update')->name('admin.updateUser');
		Route::get('/goDelete/{id}', 'HomeController@delete')->name('admin.goDelete');
		
		Route::any('/Settings', function() {
		     return view('admin.setting');
		})->name('admin.Settings');


		Route::any('/set', 'HomeController@saveSetting')->name('admin.set');
});
Route::group(['middleware' => ['auth','is_HR'],'prefix' => 'HR'], function () {

		Route::get('/CallAddEmp', function() {
		     return view('HR.AddEmp.AddEmp',);
		})->name('HR.CallAddEmp'); // to call add employee view
		
		Route::get('/CallImportEmp', function() {
		     return view('HR.ImportEmp');
		})->name('HR.CallImportEmp'); // to call import and store employee view

		Route::get('/CallImportHolidays', function() {
		     return view('HR.ImportHolidays');
		})->name('HR.CallImportHolidays'); // to call import and store Holiday List

		Route::get('/CallAddBankDetail', function() {
		     return view('HR.AddBankDetail');
		})->name('HR.CallAddBankDetail'); // to call add employee view

		Route::any('/ImportEmp','HRController@ImportEmployee')->name('HR.ImportEmp');//to import and store employee
		
		Route::any('/ImportHolidayList','HRController@ImportHolidayList')->name('HR.ImportHolidayList');//to import and store HolidayList

		Route::any('DeleteHoliday/{id}','HRController@DeleteHoliday')->name('HR.DeleteHoliday');// delete holidays by id

		Route::any('/RemoveEmp/{id}','HRController@RemoveEmployee')->name('HR.RemoveEmp');// to delete employee

		Route::any('/ExitEmp/{id}','HRController@ExitEmployee')->name('HR.ExitEmp');// to exit employee

		Route::any('/CallExitEmpForm/{id}','HRController@CallExitEmployee')->name('HR.CallExitEmpForm');// to exit employee

		Route::get('/AddBankDetail','HRController@AddBankDetail')->name('HR.AddBankDetail');// to employee bank details
		Route::any('/ExportFile/{format}', 'HRController@Export')->name('HR.ExportFile');// to export Employee file

		Route::any('/ExportCandidate/{format}', 'RecruitController@ExportCandidate')->name('HR.ExportCandidate');// to export Candidate file

		Route::any('/EditEmp/{id}','HRController@EditEmployee')->name('HR.EditEmp');// to edit employee

		// for submitting new employee data
		Route::any('/AddEmpPrimary/','HRController@AddEmpPrimary')->name('HR.AddEmpPrimary');// to add primary data
		Route::any('/AddEmpPersonal/','HRController@AddEmpPersonal')->name('HR.AddEmpPersonal');// to add personal data
		Route::any('/AddEmpSocial/','HRController@AddEmpSocial')->name('HR.AddEmpSocial');// to add social data
		Route::any('/AddEmpBank/','HRController@AddEmpBank')->name('HR.AddEmpBank');// to Bank data
		Route::any('/AddEmpSalary/','HRController@AddEmpSalary')->name('HR.AddEmpSalary');// to Bank data

		
		Route::any('/HR.LeaveAction/{id}','HomeController@LeaveAction')->name('HR.LeaveAction');// to storing action for Leave Request

		Route::any('/HR.CallHolidayEdit/{id}','HRController@CallHolidayEdit')->name('HR.CallHolidayEdit');// to call holiday Edit form


		Route::any('EditLeaveData/{code}','HRController@EditLeaveData')->name('HR.EditLeaveData');// for adding dynamically leaves code 1 for add Leave and 2 for Increment pending Leave

		Route::any('EditLeaveBal','HRController@EditLeaveBal')->name('HR.EditLeaveBal');// to submit increment data for pending leaves

		Route::any('HR.getLastBalanceById','HRController@getLastBalanceById')->name('HR.getLastBalanceById');// to get data of last year balance
		Route::any('HR.EditLastBal','HRController@EditLastBal')->name('HR.EditLastBal');// to edit last year leave balance

		Route::any('HR.SaveEvents/{mode}','HRController@SaveEvents')->name('HR.SaveEvents');// to add or edit Events

		Route::any('HR.SaveHoliday/{mode}','HRController@SaveHoliday')->name('HR.SaveHoliday');// to add or edit Events
		
		Route::any('HR.CallEditEvent/{id}','HRController@CallEditEvent')->name('HR.CallEditEvent');// to add or edit Events

		

		Route::any('HR.CallHolidayAdd',function(){
			$mode = 0;// insert
			return view('HR.HolidayForm',compact('mode'));
		})->name('HR.CallHolidayAdd');// to call holiday form in insert mode

		Route::get('/CallAddEvents',function(){
			$mode = 0;// Add insert mode
			return view('data.AddEvents',compact('mode'));
		})->name('HR.CallAddEvents');// to call addEvents module

		

		Route::get('CallAddCandidate',function(){
			return view('HR.Recruit.AddCandidate');
		})->name('HR.CallAddCandidate');// to call form to add Candidate

		Route::any('AddCandidate/{code}','RecruitController@AddCandidate')->name('HR.AddCandidate');// to submit candidate data

		Route::any('RemoveCandidate/{id}','RecruitController@RemoveCandidate')->name('HR.RemoveCandidate');

		Route::any('CallEditCandidate/{id}','RecruitController@CallEditCandidate')->name('HR.CallEditCandidate');// to call edit form for candidate

		Route::any('scheduleForm/{id}/{Itype}','RecruitController@CallScheduleForm')->name('HR.scheduleForm');// to call schedule form 

		Route::any('SaveSchedule/{id}/{Itype}','RecruitController@SaveSchedule')->name('HR.SaveSchedule');// to save schedule form 

		Route::any('CallEditSchedule/{id}/{Itype}','RecruitController@CallEditSchedule')->name('HR.CallEditSchedule');// to call Edit schedule form

		Route::any('EditSchedule/{id}/{Itype}','RecruitController@EditSchedule')->name('HR.EditSchedule');// to Edit schedule form 

		Route::any('setResult/{id}/{Itype}/{result}','RecruitController@setResult')->name('HR.setResult');// to Edit schedule form 
		
		Route::any('swipeResult/{id}/{Itype}/{result}','RecruitController@swipeResult')->name('HR.swipeResult');// to Edit schedule form

		Route::any('ResultModel/{id}/{mode}','RecruitController@ResultModel')->name('HR.ResultModel');// to Edit schedule form

		Route::any('SaveFinal/{id}','RecruitController@SaveFinal')->name('HR.SaveFinal');// to Edit schedule form

		Route::any('CallEditSelected/{id}','RecruitController@CallEditSelected')->name('HR.CallEditSelected');// to call Edit selected candidate form

		Route::any('EditSelected/{id}','RecruitController@EditSelected')->name('HR.EditSelected');// to call Edit selected candidate form

		Route::any('SaveEmpStatus/{id}','HRController@SaveEmpStatus')->name('HR.SaveEmpStatus');// to Submit Edit Employee Status form
		

		Route::get('/DeleteEvent/{id}', 'HRController@deleteEvent')->name('HR.DeleteEvent');
				
});


