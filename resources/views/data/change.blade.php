@extends('layouts.admin.app')
@section('content')

<div class="col-md-12 "> 
<form method="post" action="{{route('admin.changePassword')}}">
		@csrf
	<div class="container-fluid py-3 col-md-10">
		<!-- /.row -->
		<div class="row py-3">
		    <div class="col-md-12">
		        <div class="card card-default">
		          	<div class="card-header">
		            	<h3 class="card-title text-xl">Reset Password</h3>
		          	</div>
			        <div class="card-body py3">
			        	@if ($errors->any())
							    <div class="alert alert-danger">
							      <strong>Whoops!</strong> There were some problems with your input.<br><br>
							        <ul>
								     @foreach ($errors->all() as $error)
								        <li>{{ $error }}</li>
								      @endforeach
							        </ul>
							    </div>
						@endif
			        	@include('layouts.message')
			            <div class="bs-stepper">
			              	<div class="bs-stepper-content">
			                <!-- your steps content here -->
				                <div id="logins-part" class="content" role="tabpanel" aria-labelledby="logins-part-trigger">
					                <div class="form-group">
					                    <label for="exampleInputEmail1">Old Password</label>
					                    <input type="password" name="old_password" class="form-control">
					                </div>
					                <div class="form-group">
					                    <label for="exampleInputPassword1">Password</label>
					                     <input type="password" name="password" class="form-control">
					                </div>
					                <div class="form-group">
					                    <label for="exampleInputPassword1"> Confirm Password</label>
					                     <input type="password" name="confirm" class="form-control">
					                </div>
					                <button type="submit" class="btn-lg btn-primary">change password</button>
				                </div>
				              
			              	</div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>	
	</div>	

</form>
</div>
@endsection


