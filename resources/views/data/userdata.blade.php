@extends('layouts.admin.app')
@section('content')
<div class="container-fluid p-5">
	<div class="card rounded">
		<div class="card-header">
		<h1 class="card-title text-xl text-dark">WELCOME {{$usertype}} USER TABLE DATA</h1>
		</div>
		<div class="card-body">
			<table class="table table-hover">
				<thead>
					<th>ID</th>
					<th>Name</th>
					<th>EMAIL</th>
					<th>CREATED DATE</th>
					<th>Action</th>
				</thead>
				<tbody>
					@foreach($userdata as $data)
						<tr>
							<td>{{$data->id}}</td>
							<td>{{$data->name}}</td>
							<td>{{$data->email}}</td>
							<td>{{ $data->created_at->format('d-m-Y') }}</td>
							<td>
								<a href="{{route('admin.goEdit',['id' => $data->id])}}"><i class="fas fa-edit"></i></a>
								<a href="{{route('admin.goDelete',['id' => $data->id])}}"><i class="fas fa-trash"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
