@extends('layouts.admin.app')
@section('content')
<div class="container">
		<h1 class="card-title text-xl text-dark">WELCOME {{$usertype}} USER TABLE DATA</h1><br/>
		<form method="Post" action="{{route('admin.updateUser')}}">
<table class="table table-dark table-hover">
	@csrf
	<thead>
		<th>ID</th>
		<th>Name</th>
		<th>EMAIL</th>
	</thead>
	<tbody>
<tr>
	<td><input type="text" name="id" readonly="true" value="{{$data->id}}" ></td>
	<td><input type="text" name="name" value="{{$data->name}}"></td>
	<td><input type="text" name="email" value="{{$data->email}}"></td>
</tr>
<tr class="text-center">
	<td colspan='3'><input type="submit" name="submit" class="btn-lg btn-primary" value="Update"></td>
</tr>

</tbody>
</table>
</form>
</div>
@endsection
