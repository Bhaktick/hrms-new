@extends('layouts.admin.app')
@section('content')
<div class="container-fluid p-5">

   <div class="card card-info ">
        <div class="card-header ">
        	<h1 class="card-title text-xl"><i class="fa fa-calendar-plus nav-icon"></i> Add Event</h1>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-6 p-4">
            	   @if ($errors->any())
    				    <div class="alert alert-danger">
    				      <strong>Whoops!</strong> There were some problems with your input.<br><br>
    				        <ul>
    					     @foreach ($errors->all() as $error)
    					        <li>{{ $error }}</li>
    					      @endforeach
    				        </ul>
    				    </div>
    			     @endif
                    @include('layouts.message')
                    @if($mode == 0)
                        <form method="Post" class="formEvent" action="{{route('HR.SaveEvents',['mode' => 0])}}"> <!-- Insert -->
                    @elseif($mode == 1)
                        <form method="Post" class="formEvent" action="{{route('HR.SaveEvents',['mode' => 1])}}"> <!-- update -->
                        <input type="hidden" name="id" id='id' value="{{$event->id}}">    
                    @endif  
                    @csrf
                    <div class="form-group">
        	                <label for="EventTitle">Event Title</label>
        	                <input type="text" class="form-control " Name="EventTitle" placeholder="Enter Event Title Here..." value="{{($mode == 1)? $event->title : old('EventTitle')}}" />
        	        </div>
        	        <div class="form-group ">
        	            <label for="EventDate">Event Date</label>
        	            <input type="text" name="EventDate" class="datepicker form-control input-text full-width" placeholder="Departure Date" value="{{($mode == 1)? $event->date : old('EventDate')}}"/>
                  	</div>
                  	<!-- how to take input and manage it is remaining for timing 
                  	'time_start' => 'date_format:H:i',
                	'time_end' => 'date_format:H:i|after:time_start', -->
                  	<div class="form-group ">
        	            <label for="startTime">Start Time</label>
              			<input class="timepicker form-control" type="text" name="startTime" placeholder="Departure Start Time" value="{{($mode == 1)? $event->start_time : old('startTime')}}" />
                  	</div>
                  	<div class="form-group ">
        	            <label for="endTime">End Time</label>
              			<input class="timepicker form-control bg-white" type="text" name="endTime" id="end" placeholder="Departure End Time" value="{{($mode == 1)? $event->end_time : old('endTime')}}" />
                  	</div>
                    <div class="form-group">
                            <label for="Description">Event Details</label>
                                @if($mode==1)
                                   <textarea class="form-control" Name="Description">{{$event->description}}</textarea>
                                @else
                                    <textarea class="form-control" Name="Description">{{old('Description')}}</textarea>
                                @endif
                    </div>
                </div>
                    <div class="col-md-6 text-center  ">
                        <h1 class="p-5 m-5 h-80  shadow-lg text-center rounded float-right m-3 bg-secondery">
                            <a href="{{route('HR.EventsList')}}">
                                <i class="fa fa-calendar-plus text-dark fa-5x nav-icon p-5 m-4 "></i>
                            </a>
                        </h1>
                     </div>
            </div>

        </div>
        <div class="card-footer">
        	<button type="submit" class="btn btn-info">Submit</button>
        </div>
    </div>
</div>
			


@endsection
@section('page_script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


  <!-- timepicker links here  -->

  <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.min.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.js"></script>


<script type="text/javascript">
$(document).ready(function () {

	var today = new Date();
    if( $("#id").value != undefined)
    {
        alert(("#id").value)
        $("#end").attr('disabled',true);
    }
	$('.datepicker').datepicker({
    	// to format and manage datepicker controller
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose:true,
        startDate: "today",
        daysOfWeekDisabled: '0',
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
		 $('.timepicker').timepicker({
		 		'disableTextInput' : false,
		 		'timeFormat' : 'g:i A',
		 		'step': 15,
		 		'minTime' : '10:00 AM',
		 		'maxTime' : '8:00 PM',
                'disableTimeRanges': [
					['1:00 PM', '2:00 PM'],
				]
            }).on('selectTime', function (ev) {
	            $(this).timepicker('hide');
	            $("#end").attr('disabled',false);
                end =this.value;
                if( $(this).attr('name') != "endTime")
                {
                    $("#end").val('');
	            }
                // alert(end);
	           	$("#end").timepicker({
	           		'disableTextInput' : false,
			 		'timeFormat' : 'g:i A',
			 		'step': 15,
			 		'minTime' : end,
			 		'maxTime' : '8:00pm',
	                'disableTimeRanges': [
						['1:00 PM', '2:00 PM'],
					],
	        	});
	        }); 



    });





    

</script>
@endsection
