@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
<div class="container-fluid p-5">
    <div class="card card-warning">
        <div class="card-header">
                <h1 class="card-title text-xl text-dark" id="title"><i class="fa fa-id-card nav-icon"></i> EMPLOYEE DATA</h1>
        </div>
        <div class="card-body " style="overflow-x: auto;">
           @if(Auth::user()->Role == 2)
                <a href="{{route('HR.CallAddEmp')}}" class="btn btn-primary my-2"><i class="fa fa-plus nav-icon"></i> ADD</a>
                <a href="{{route('HR.CallImportEmp')}}" class="btn btn-primary my-2"><i class="fa fa-plus nav-icon"></i> Import</a>
            
                <a href="{{route('HR.ExportFile',['format' => 'CSV'])}}" class="btn btn-primary my-2"> CSV</a>
                <a href="{{route('HR.ExportFile',['format' => 'Excel'])}}" class="btn btn-primary my-2">Excel</a>
            @endif

            @include('layouts.message')
            <table class="table table-hover " id="user">
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>DOB</th>
                    <th>EMAIL</th>
                    <th>Designation</th>
                    <th>Gender</th>
                    <th>Education</th>
                    <th>Course</th>
                    <th>Experience</th>
                    <th>Status</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- Datatables JS CDN -->
@endsection
@section('page_script')
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
    <!-- Public/vendor/...  -->
    <script src="https://datatables.net/extensions/buttons/examples/html5/simple.html"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
     <!-- for buttons --> 

     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">


<script type="text/javascript">
  $(function () {
    var table = $('#user').removeAttr('width').DataTable({
        serverside :true,
        processing: true,
        scrollX: true,
        aaSorting: [],// to stop initial sorting 
        ajax: "{{ route('EmpdataTable',['mode' => 1]) }}",
        columnDefs: [
            { width: 100, targets: 2 }
        ],
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'DOB', name: 'DOB'},            
            {data: 'email', name: 'email'},
            {data: 'Designation', name: 'Designation'},
            {data: 'Gender', name: 'Gender'},
            {data: 'Education', name: 'Education'},
            {data: 'Course', name: 'Course'},
            {data: 'Experience', name: 'Experience'},
            {
                data: 'Status', 
                name: 'Status', 
                orderable: true, 
                searchable: true
            },
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
        ],
        
    });    
  });


          
</script>
@endsection
