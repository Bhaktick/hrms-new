@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
<div class="container-fluid p-5">
    <div class="card card-warning">
        <div class="card-header">
                <h1 class="card-title text-xl text-dark" id="title"><i class="fa fa-calendar-alt nav-icon"></i> Practical Schedule List</h1>
        </div>
        <div class="card-body " style="overflow-x: auto;">
            @include('layouts.message')
            <table class="table table-hover " id="user">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Designation</th>
                    <th>Experience</th>
                    <th>Type</th>
                    <th>Interviewer</th>
                    <th>Interview Date</th>
                    <th>Interview Time</th>
                    <th>Result</th>
                    <th>Edit</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- Datatables JS CDN -->
@endsection
@section('page_script')
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
    <!-- Public/vendor/...  -->
    <script src="https://datatables.net/extensions/buttons/examples/html5/simple.html"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
     <!-- for buttons --> 

     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">

<script type="text/javascript">
  $(function () {
    var table = $('#user').DataTable({
        serverside :true,
        processing: true,
        aaSorting: [],// to stop initial sorting 
        ajax: "{{ route('getPScheduleList') }}",
        columns: [
            {data: 'can_name', name: 'can_name'},
            {data: 'can_email', name: 'can_email'},
            {data: 'can_contact', name: 'can_contact'},
            {data: 'can_designantion', name: 'can_designantion'},
            {data: 'can_experience', name: 'can_experience'},
            {data: 'Type', name: 'Type'},
            {data: 'Interviewer', name: 'Interviewer'},
            {data: 'Pdate', name: 'Pdate'},
            {data: 'practical_time', name: 'practical_time'},
            {
                data: 'result', 
                name: 'result', 
                orderable: true, 
                searchable: true
            },
            {
                data: 'Edit', 
                name: 'Edit', 
                orderable: true, 
                searchable: true
            },
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
        ],
        
    });    
  });


          
</script>
@endsection
