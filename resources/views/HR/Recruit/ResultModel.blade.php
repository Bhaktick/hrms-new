@extends('layouts.admin.app')
@section('content')
@include('layouts.RadioCSS')
<div class="container-fluid p-5">
        <div class="card card-warning">
            <div class="card-header">
                <h1 class="card-title text-xl text-dark">Form to Relieving Candidate</h1>
            </div>
            <div class="card-body " style="overflow-x: auto;">
                 
                <div class="row p-3">
                    <div class=" card col-md-4 ">   
                        <form method="Post" action="{{route('HR.SaveFinal',['id' => $candidate->id])}}">
                            @csrf
                            @if ($errors->any())
                            <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                 @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                  @endforeach
                                </ul>
                            </div>
                            @endif
                            @include('layouts.message')
                            @if($candidate->hr_status == null || $mode == 1)
                                <div class="form-group">
                                    <label for="remark">Remark</label>
                                    @if($mode == 1)
                                    <textarea class="form-control" Name="remark">{{$candidate->remark}}</textarea>
                                    @else
                                    <textarea class="form-control" Name="remark"></textarea>
                                    @endif
                                    
                                </div>
                                <div class="form-check">
                                    <div class="form-group">
                                    <label>Please select Candidate is Selected or Not</label>
                                    </div>
                                    
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=1 {{ ($candidate->hr_status == 1 && $mode == 1) ? 'checked' : '' }}> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Approved Leave' >Selected</i> 
                                    </label>
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=2 {{ ($candidate->hr_status == 2 && $mode == 1) ? 'checked' : '' }}> <i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Rejected Leave'  >Rejected</i> 
                                    </label>
                                </div>
                                <div class="form-group text-center p-2">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Save Action">
                                </div>
                            @elseif($mode != 1)
                            <!-- not edit mode then -->
                            <h2 class="bg-info m-3 p-2 rounded">Action Already Taken !!!</h2>
                            @endif
                        </form>
                    </div>
                    <div class="card col-md-8">
                        <table class="table table-white rounded m-2">
                            <tr>
                                <th>Candidate Name</th>
                                <td>{{$candidate->can_name}}</td>
                            </tr>
                            <tr>
                                <th>Candidate Email</th>
                                <td>{{$candidate->can_email}}</td>
                            </tr>
                            <tr>
                                <th>Candidate Contact</th>
                                <td>{{$candidate->can_contact}}</td>
                            </tr>
                            <tr>
                                <th>Candidate Designation</th>
                                <td>{{$candidate->can_designantion}}</td>
                            </tr>
                            <tr>
                                <th>Candidate Experience</th>
                                <td>{{$candidate->can_experience}}</td>
                            </tr>
                            <tr>
                                <th>Candidate Technical Status</th>
                                <td>
                                    @if($candidate->technical_status == 1)
                                    <i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Selected'>
                                    @elseif($candidate->technical_status == 2)
                                    <i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Candidate Parctical Status</th>
                                <td>@if($candidate->practical_status == 1)
                                    <i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Selected'>
                                    @elseif($candidate->practical_status == 2)
                                    <i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'>
                                    @endif</td>
                            </tr>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>


@endsection