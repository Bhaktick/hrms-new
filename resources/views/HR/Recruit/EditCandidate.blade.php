@extends('layouts.admin.app')
@section('content')

<div class="container-fluid p-5">
<!-- /.row -->
	<form method="Post" class="formMode" action="{{route('HR.AddCandidate',['code' => 1])}}">

<!-- do not change name of form connected with Jquery -->
	<div class=" card card-primary">
		<div class="card-header"> 
				<h1 class="card-title text-xl"><i class="fa fa-edit nav-icon"></i> Edit Candidate</h1>
		</div>
		<div class="card-body ">
			<div class="row">
				<div class="col-md-6 p-4">
		        	@if ($errors->any())
		    	    <div class="alert alert-danger">
		    	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
		    	        <ul>
		    		     @foreach ($errors->all() as $error)
		    		        <li>{{ $error }}</li>
		    		      @endforeach
		    	        </ul>
		    	    </div>
		    		@endif

		    		@include('layouts.message')

						@csrf		      	
						    <input type="hidden" name="can_id" value="{{$candidate->id}}">

				      	<div class="form-group">
				            <label for="Name">Candidate Name</label>
				            <input type="text" class="form-control " Name="Name" placeholder="Enter Candidate Name here..." value="{{$candidate->can_name}}">
				      	</div>
				      	<div class="form-group">
				            <label for="Email">Candidate Email</label>
				            <input type="text" class="form-control " Name="Email" placeholder="Enter Candidate Email here..." value="{{$candidate->can_email}}">
				      	</div>
				      	<div class="form-group">
				            <label for="Contact">Candidate Contact</label>
				            <input type="text" class="form-control " Name="Contact" placeholder="Enter Candidate Contact here..." value="{{$candidate->can_contact}}">
				      	</div>
				      	<div class="form-group">
				            <label for="Designantion">Candidate For Designantion</label>
				            <input type="text" class="form-control " Name="Designantion" placeholder="Enter Candidate Designantion here..." value="{{$candidate->can_designantion}}">
				      	</div>
				      	<div class="form-group" >
				            <label for="Type">Candidate Type</label>
				            <select class="form-control " id="type" name="Type">
				            	<option value=null  disabled="true" selected="true">-- Select Candidate Type --</option>
				            	<option value="Fresher" {{ $candidate->can_experience == 0 ? 'selected' : '' }}>Fresher</option>
				            	<option value="Experienced" {{ $candidate->can_experience > 0 ? 'selected' : '' }}>Experienced</option>
				            </select>
				      	</div>
				      	<div class="form-group" id="experience">
				            <label for="Experience">Candidate Experience (Years)</label>
				            <input type="text" class="form-control " Name="Experience" placeholder="Enter Candidate Experience here..." value="{{$candidate->can_experience}}">
				      	</div>
				    </div>

				     <div class="col-md-6 text-center  ">
				      	<h1 class="p-3 m-5 h-80  shadow-lg text-center rounded float-right m-3 bg-secondery"><i class="fa fa-user-plus text-dark fa-5x nav-icon p-5 m-4 "></i>
				      	</h1>
				     </div>
			  </div>
		      	
		</div>
		<div class="card-footer">
				<button type="submit" class="btn btn-primary">Edit Candidate</button>
	    </div>
	</div>
</form>
</div>


@endsection
@section('page_script')

<script type="text/javascript">
$(document).ready(function () {
	var exp = "{{$candidate->can_experience}}";
	if(exp == 0)
	{
		$("#experience").hide();
	}


	$("#type").change(function () {
		var type = $(this).val();

		if(type == "Experienced")
		{
			$("#experience").show();
		}
		else if(type == "Fresher")
		{
			$("#experience").hide();
		}
	});

});
</script>

@endsection