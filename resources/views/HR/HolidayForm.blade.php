@extends('layouts.admin.app')
@section('content')
<div class="container-fluid p-5">

   <div class="card card-info ">
        <div class="card-header ">
        	<h1 class="card-title text-xl"><i class="fa fa-taxi nav-icon"></i> @if($mode == 0) Add Holiday @elseif($mode == 1) Edit Holiday @endif</h1>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-6 p-4">
            	   @if ($errors->any())
    				    <div class="alert alert-danger">
    				      <strong>Whoops!</strong> There were some problems with your input.<br><br>
    				        <ul>
    					     @foreach ($errors->all() as $error)
    					        <li>{{ $error }}</li>
    					      @endforeach
    				        </ul>
    				    </div>
    			     @endif
                    @include('layouts.message')
                    @if($mode == 0)
                        <form method="Post" class="formHoliday" action="{{route('HR.SaveHoliday',['mode' => 0])}}"> <!-- Insert -->
                    @elseif($mode == 1)
                        <form method="Post" class="formHoliday" action="{{route('HR.SaveHoliday',['mode' => 1])}}"> <!-- update -->
                        <input type="hidden" name="id" id='id' value="{{$data->id}}">    
                    @endif  
                    @csrf
                    <div class="form-group">
        	                <label for="Festival">Festival Name</label>
        	                <input type="text" class="form-control " Name="Festival" placeholder="Enter Event Title Here..." value="{{($mode == 1)? $data->festival : old('Festival')}}" />
        	        </div>
        	        <div class="form-group ">
        	            <label for="date">Festival Date</label>
        	            <input type="text" name="date" class="datepicker form-control input-text full-width" placeholder="Departure Date" value="{{($mode == 1)? $data->date : old('date')}}"/>
                  	</div>
                </div>
                    <div class="col-md-6 text-center  ">
                        <h1 class="p-5 m-5 h-80 shadow-lg text-center rounded float-right m-3 bg-secondery">
                            <a href="{{route('HR.EventsList')}}">
                                <i class="fa fa-taxi text-dark fa-5x nav-icon p-5 m-4 "></i>
                            </a>
                        </h1>
                     </div>
            </div>

        </div>
        <div class="card-footer">
        	<button type="submit" class="btn btn-info">Submit</button>
        </div>
    </div>
</div>
			


@endsection
@section('page_script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


  <!-- timepicker links here  -->

  <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.min.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.js"></script>


<script type="text/javascript">
$(document).ready(function () {

	var today = new Date();
	$('.datepicker').datepicker({
    	// to format and manage datepicker controller
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose:true,
        startDate: "today",
        daysOfWeekDisabled: '0',
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });





    

</script>
@endsection
