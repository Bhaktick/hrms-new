@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
<div class="container-fluid p-5">    
  <div class="card">
        <div class="card-header">
            <h1 class="card-title text-xl text-dark"> Import Festival Holiday List</h1>
        </div>
        <div class="card-body">
          <!-- Form -->
            @include('layouts.message')

             <form method='post' action="{{ route('HR.ImportHolidayList') }}" enctype='multipart/form-data' >
               {{ csrf_field() }}
               <input type='file' name='file' required="true">
               <input type='submit' name='submit' value='Import'>
             </form>
        </div>
   </div>
 </div>
@endsection