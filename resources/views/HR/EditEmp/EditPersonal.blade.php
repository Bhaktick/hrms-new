<form method="POST" action="{{route('HR.EditEmp',['id' => $data->id])}}">

  	@csrf
    @if(Auth::user()->Role != 2)
    <fieldset disabled="disabled">
    @endif
    <div class="card-body">
      <h1>Personal Details</h1>

    	@if ($errors->any())
	    <div class="alert alert-danger">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
		     @foreach ($errors->all() as $error)
		        <li>{{ $error }}</li>
		      @endforeach
	        </ul>
	    </div>
		@endif

		@include('layouts.message')
    <input type="hidden" name="form_name" value="Personal">
    <input type="hidden" name="form_id" value="{{$data->id}}">
      <div class="form-group">
        <label for="Personal_Email">Employee Personal Email</label>
        <input type="email" class="form-control" Name="Personal_Email" placeholder="Enter Employee Personal Email" value="{{$data->Personal_Email}}">
      </div>
      <div class="form-group">
        <label for="Another_cont">Altenative Contact</label>
        <input type="text" class="form-control" Name="Another_cont" placeholder="Enter Altenative Contact" value="{{$data->Another_cont}}">
      </div>
    <div class="form-check">
        <div class="form-group">
        <label for="Gender">Employee Gender</label>
        </div>
        <label>
            <input type="radio" class="form-check-input" Name="Gender" value=1 {{ ($data->Gender == 1 ) ? 'checked' : '' }}> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Done' > Male</i> 
        </label>
        <label>
            <input type="radio" class="form-check-input" Name="Gender" value=2 {{ ($data->Gender == 2) ? 'checked' : '' }}> <i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Not Done'  > Female</i> 
        </label>
    </div>
    <div class="form-group">
        <label for="DOB">Date Of Birth</label>
        <input type="text" name="DOB" class="datepicker form-control input-text full-width" placeholder="Departure Date" value="{{$data->DOB}}" />    
    </div>
      <div class="form-group">
        <label for="Address">Address</label>
        <textarea class="form-control" Name="Address">{{$data->Address}} </textarea>
      </div>
      <div class="form-group">
        <label for="Education">Education</label>
        <input type="text" class="form-control" Name="Education" placeholder="Enter Employee Education Email" value="{{$data->Education}}">
      </div>
      <div class="form-group">
        <label for="Course">Course</label>
        <input type="text" class="form-control" Name="Course" placeholder="Enter Employee Course" value="{{$data->Course}}">
      </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </fieldset>
</form>
@section('page_script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">
$(document).ready(function () {

        var today = new Date();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
</script>
@endsection