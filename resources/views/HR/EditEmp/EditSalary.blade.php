<form method="POST" action="{{route('HR.EditEmp',['id' => $data->id])}}">

  	@csrf
    @if(Auth::user()->Role != 2)
    <fieldset disabled="disabled">
    @endif
    <div class="card-body">
       <h1>Salary Details</h1>

    	@if ($errors->any())
	    <div class="alert alert-danger">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
		     @foreach ($errors->all() as $error)
		        <li>{{ $error }}</li>
		      @endforeach
	        </ul>
	    </div>
		@endif

		@include('layouts.message')
      <input type="hidden" name="form_name" value="Salary">
    <input type="hidden" name="form_id" value="{{$data->id}}">
      <div class="form-group">
        <label for="Experience"> Experience</label>
        <input type="text" class="form-control" Name="Experience" placeholder="Enter Experience" value="{{$data->Experience}}">
      </div>
      <div class="form-group">
        <label for="Inc_Time_Period"> Increment Time Period</label>
        <input type="Inc_Time_Period" class="form-control" Name="Inc_Time_Period" placeholder="Enter Increment Time Period" value="{{$data->Inc_Time_Period}}">
      </div>
      <div class="form-group">
        <label for="Last_Inc_Date"> Last Increment Date</label>
        <input type="text" name="Last_Inc_Date" class="datepicker form-control input-text full-width" placeholder="Enter Departure Date" value="{{$data->Last_Inc_Date}}" />
      </div>
      <div class="form-group">
        <label for="Last_Inc_Amount">Last Increment Amount </label>
        <input type="text" class="form-control" Name="Last_Inc_Amount" placeholder="Enter Last Increment Amount" value="{{$data->Last_Inc_Amount}}">
      </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </fieldset>
</form>
@section('page_script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">
$(document).ready(function () {

        var today = new Date();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
</script>
@endsection