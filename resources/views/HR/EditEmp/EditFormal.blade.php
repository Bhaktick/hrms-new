<form method="POST" action="{{route('HR.EditEmp',['id' => $data->id])}}">

  	@csrf
    @if(Auth::user()->Role != 2)
    <fieldset disabled="disabled">
    @endif
    <div class="card-body">
       <h1>Formal Details</h1>

    	@if ($errors->any())
	    <div class="alert alert-danger">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
		     @foreach ($errors->all() as $error)
		        <li>{{ $error }}</li>
		      @endforeach
	        </ul>
	    </div>
		@endif
		@include('layouts.message')
        <input type="hidden" name="form_name" value="Formal">
        <input type="hidden" name="form_id" value="{{$data->id}}">
            <div class="form-group">
                <label for="Doc_code">Document Code</label>
                <input type="Doc_code" class="form-control" Name="Doc_code" placeholder="Enter Increment Time Period" value="{{$data->Doc_code}}">
            </div>
            <div class="form-check">
                <div class="form-group">
                <label>Document Status</label>
                </div>
                <label>
                    <input type="radio" class="form-check-input" Name="Doc_status" value=1 {{ ($data->Doc_status == 1 ) ? 'checked' : '' }}> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Done' > Done</i> 
                </label>
                <label>
                    <input type="radio" class="form-check-input" Name="Doc_status" value=2 {{ ($data->Doc_status == 2) ? 'checked' : '' }}> <i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Not Done'  > Not Done</i> 
                </label>
            </div>
            <div class="form-check">
                <div class="form-group">
                <label>NDA Status</label>
                </div>
                <label>
                    <input type="radio" class="form-check-input" Name="NDA_status" value=1 {{ ($data->NDA_status == 1 ) ? 'checked' : '' }}> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Done' > Done</i> 
                </label>
                <label>
                    <input type="radio" class="form-check-input" Name="NDA_status" value=2 {{ ($data->NDA_status == 2) ? 'checked' : '' }}> <i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Not Done'  > Not Done</i> 
                </label>
            </div>
        </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </fieldset>
</form>
