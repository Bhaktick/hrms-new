<form method="POST" action="{{route('HR.EditEmp',['id' => $data->id])}}">

  	@csrf
    @if(Auth::user()->Role != 2)
    <fieldset disabled="disabled">
    @endif
    <div class="card-body">
           <h1>Social Details</h1>

    	@if ($errors->any())
	    <div class="alert alert-danger">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
		     @foreach ($errors->all() as $error)
		        <li>{{ $error }}</li>
		      @endforeach
	        </ul>
	    </div>
		@endif

		@include('layouts.message')
    <input type="hidden" name="form_name" value="Social">
    <input type="hidden" name="form_id" value="{{$data->id}}">
      <div class="form-group">
        <label for="SkypeID">Skype ID </label>
        <input type="text" class="form-control" Name="SkypeID" placeholder="Enter Skype ID" value="{{$data->SkypeID}}">
      </div>
       <div class="form-group">
        <label for="Skype_password">Skype Password</label>
        <input type="text" class="form-control" Name="Skype_password" placeholder="Enter Skype Password" value="{{$data->Skype_password}}">
      </div>
      <div class="form-group">
        <label for="TeamloggerID">Teamlogger ID </label>
        <input type="text" class="form-control" Name="TeamloggerID" placeholder="Enter Teamlogger ID" value="{{$data->TeamloggerID}}">
      </div>
       <div class="form-group">
        <label for="Team_password">Teamlogger Password</label>
        <input type="text" class="form-control" Name="Team_password" placeholder="Enter Teamlogger Password" value="{{$data->Team_password}}">
      </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </fieldset>
</form>