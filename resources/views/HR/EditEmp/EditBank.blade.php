<form method="POST" action="{{route('HR.EditEmp',['id' => $data->id])}}">
  	@csrf
    @if(Auth::user()->Role != 2)
    <fieldset disabled="disabled">
    @endif
    <div class="card-body">
        <h1>Bank Details</h1>

    	@if ($errors->any())
	    <div class="alert alert-danger">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
		     @foreach ($errors->all() as $error)
		        <li>{{ $error }}</li>
		      @endforeach
	        </ul>
	    </div>
		@endif

		@include('layouts.message')
    <input type="hidden" name="form_name" value="Bank">
    <input type="hidden" name="form_id" value="{{$data->id}}">
      <div class="form-group">
        <label for="Bank_name">Bank Name</label>
        <input type="text" class="form-control" Name="Bank_name" placeholder="Enter Bank Name" value="{{$data->Bank_name}}">
      </div>
      <div class="form-group">
        <label for="Acc_holder_name">Bank Account Holder Name</label>
        <input type="text" class="form-control" Name="Acc_holder_name" placeholder="Enter Bank Account Holder Name" value="{{$data->Acc_holder_name}}">
      </div>
       <div class="form-group">
        <label for="Acc_no">Bank Account Number</label>
        <input type="text" class="form-control" Name="Acc_no" placeholder="Enter Account Number" value="{{$data->Acc_no}}">
      </div>
      <div class="form-group">
        <label for="IFSC">Bank IFSC </label>
        <input type="text" class="form-control" Name="IFSC" placeholder="Enter Bank IFSC" value="{{$data->IFSC}}">
      </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </fieldset>
</form>
@section('page_script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">
$(document).ready(function () {

        var today = new Date();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
</script>
@endsection