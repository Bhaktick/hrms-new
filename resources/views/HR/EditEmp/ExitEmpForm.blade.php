@extends('layouts.admin.app')
@section('content')
@include('layouts.RadioCSS')
<div class="container-fluid p-5">
        <div class="card card-warning">
            <div class="card-header">
                <h1 class="card-title text-xl text-dark">Form to Exit Employees</h1>
            </div>
            <div class="card-body " style="overflow-x: auto;">
                 
                <div class="row p-3">
                    <div class=" card col-md-4 ">   
                        <form method="Post" action="{{route('HR.SaveEmpStatus',['id' => $data->id])}}">
                            @csrf
                            @if ($errors->any())
                            <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                 @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                  @endforeach
                                </ul>
                            </div>
                            @endif
                            @include('layouts.message')
                            @if($data->Status != null)
                                
                                <div class="form-check">
                                    <div class="form-group">
                                        <label for="relieving_date">Relieving  Date</label>
                                        <input type="text" name="relieving_date" class="datepicker form-control input-text full-width" placeholder="Departure Date" value="{{$data->relieving_date}}" />
                                    </div>
                                    <div class="form-group">
                                    <label>Please Select Employee is Exited or Not</label>
                                    </div>
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=1 {{ ($data->Status == 1) ? 'checked' : '' }}> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Approved Leave' > Active</i> 
                                    </label>
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=2 {{ ($data->Status == 2) ? 'checked' : '' }}><i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Rejected Leave'  > Exit</i> 
                                    </label>
                                </div>
                                <div class="form-group text-center p-2">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Save Action">
                                </div>
                                @endif
                        </form>
                    </div>
                    <div class="card col-md-8">
                        <table class="table table-white rounded m-2">
                            <tr>
                                <th>Employee Name</th>
                                <td>{{$data->name}}</td>
                            </tr>
                            <tr>
                                <th>Employee Email</th>
                                <td>{{$data->email}}</td>
                            </tr>
                            <tr>
                                <th>Employee Contact</th>
                                <td>{{$data->Contact }}</td>
                            </tr>
                            <tr>
                                <th>Employee Designation</th>
                                <td>{{$data->Designation}}</td>
                            </tr>
                            <tr>
                                <th>Employee Experience</th>
                                <td>{{$data->Experience}}</td>
                            </tr>
                            <tr>
                                <th>Employee Joining Date</th>
                                <td>{{$data->DOJ}}</td>
                            </tr>
                            <tr>
                                <th>Employee Status</th>
                                <td>
                                    @if($data->Status == 1)
                                    <i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Active'>
                                    @elseif($data->Status == 2)
                                    <i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Exit'>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>


@endsection
@section('page_script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


  <!-- timepicker links here  -->

  <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.min.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.js"></script>


<script type="text/javascript">
$(document).ready(function () {
    var today = new Date();
    $('.datepicker').datepicker({
        // to format and manage datepicker controller
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose:true,
        daysOfWeekDisabled: '0',
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });





    

</script>
@endsection
