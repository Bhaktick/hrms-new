<form method="POST" action="{{route('HR.AddEmpPrimary')}}" id="Primary">

  	@csrf
    <div class="card-body ">
        <h1>Primary Details</h1>
        	@if ($errors->any())
    	    <div class="alert alert-danger">
    	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
    	        <ul>
    		     @foreach ($errors->all() as $error)
    		        <li>{{ $error }}</li>
    		      @endforeach
    	        </ul>
    	    </div>
    		@endif

    		@include('layouts.message')

          <div class="form-group">
            <label for="name">Employee Name</label>
            <input type="text" class="form-control" Name="name" placeholder="Enter name" value="{{old('name')}}">
          </div>
          <div class="form-group">
            <label for="email">Employee Company Email</label>
            <input type="email" class="form-control" Name="email" placeholder="Enter Email" value="{{old('email')}}">
          </div>
          <div class="form-group">
            <label for="Email_password">Company Mail Password</label>
            <input type="text" class="form-control" Name="Email_password" placeholder="Enter Email password" value="{{old('Email_password')}}">
          </div>
          <div class="form-group">
            <label for="Designation">Employee Designation</label>
            <input type="text" class="form-control" Name="Designation" placeholder="Enter name" value="{{old('Designation')}}">
          </div>
          <div class="form-group">
            <label for="Contact">Employee Contact</label>
            <input type="text" class="form-control" Name="Contact" placeholder="Enter Conatct Details" value="{{old('Contact')}}">
          </div>
          <div class="form-group">
            <label for="Salary">Employee Salary</label>
            <input type="text" class="form-control" Name="Salary" placeholder="Enter Conatct Details" value="{{old('Salary')}}">
          </div>
          <div class="form-group">
            <label for="DOJ">Employee Date of Joining</label>
            <input type="text" name="DOJ" class="datepicker form-control input-text full-width" placeholder="Departure Date" value="{{old('DOJ')}}" />
          </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
</form>
@section('page_script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">
$(document).ready(function () {

        var today = new Date();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
</script>
@endsection
