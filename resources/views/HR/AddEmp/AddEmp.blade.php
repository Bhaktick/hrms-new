@extends('layouts.admin.app')
@section('content')
<div class="container-fluid p-5">

   <div class="card card-info ">
          <div class="card-header ">
            <h1 class="card-title text-xl">
              <i class="fas fa-plus"></i>
                New Employee </h1>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-right-tabContent">
                  <div class="tab-pane fade show active" id="vert-tabs-right-Primary" role="tabpanel" aria-labelledby="vert-tabs-right-Primary-tab">
                   @include('HR.AddEmp.Primary')
                  </div>

                  <div class="tab-pane fade" id="vert-tabs-right-Personal2" role="tabpanel" aria-labelledby="vert-tabs-right-Personal2-tab">
<!--                    ('HR.EditEmp.EditPersonal') -->                  
                  </div>

                  <div class="tab-pane fade" id="vert-tabs-right-Social" role="tabpanel" aria-labelledby="vert-tabs-right-Social-tab">
                   <!-- ('HR.EditEmp.EditSocial') -->
                  </div>

                  <div class="tab-pane fade" id="vert-tabs-right-Formal" role="tabpanel" aria-labelledby="vert-tabs-right-Formal-tab">
                   <!-- ('HR.EditEmp.EditFormal') -->
                  </div>

                  <div class="tab-pane fade" id="vert-tabs-right-Bank" role="tabpanel" aria-labelledby="vert-tabs-right-Bank-tab">
                  <!-- ('HR.EditEmp.EditBank') -->
                  </div>

                  <div class="tab-pane fade" id="vert-tabs-right-Salary" role="tabpanel" aria-labelledby="vert-tabs-right-Salary-tab">
                   <!-- ('HR.EditEmp.EditSalary') -->
                  </div>

                </div>
              </div>
              <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs nav-tabs-right h-100" id="vert-tabs-right-tab" role="tablist" aria-orientation="vertical">
                  <a class="nav-link active" id="vert-tabs-right-Primary-tab" data-toggle="pill" href="#vert-tabs-right-Primary" role="tab" aria-controls="vert-tabs-right-Primary" aria-selected="true">Primary Details</a>
                  <a class="nav-link disabled" id="vert-tabs-right-Personal2-tab" data-toggle="pill" href="#vert-tabs-right-Personal2" role="tab" aria-controls="vert-tabs-right-Personal2" aria-selected="false" >Personal Details</a>
                  <a class="nav-link disabled" id="vert-tabs-right-Social-tab" data-toggle="pill" href="#vert-tabs-right-Social" role="tab" aria-controls="vert-tabs-right-Social" aria-selected="false" >Social Details</a>
                  <a class="nav-link disabled" id="vert-tabs-right-Formal-tab" data-toggle="pill" href="#vert-tabs-right-Formal" role="tab" aria-controls="vert-tabs-right-Formal" aria-selected="false" >Formal Details</a>
                  <a class="nav-link disabled" id="vert-tabs-right-Bank-tab" data-toggle="pill" href="#vert-tabs-right-Bank" role="tab" aria-controls="vert-tabs-right-Bank" aria-selected="false" >Bank Details</a>
                  <a class="nav-link disabled" id="vert-tabs-right-Salary-tab" data-toggle="pill" href="#vert-tabs-right-Salary" role="tab" aria-controls="vert-tabs-right-Salary" aria-selected="false" >Salary Details</a>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card -->
        </div>
</div>
@endsection