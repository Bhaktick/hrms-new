<!DOCTYPE html>
<html lang="en">
<head>
@include('layouts.includes.head')
@yield('css')

</head>
<body class="sidebar-mini">
<div>
	@if(Auth::user()->Role == 1)
		@include('layouts.role.AdminSlidebar')
	@elseif(Auth::user()->Role == 2)
		@include('layouts.role.HRSlidebar')
	@elseif(Auth::user()->Role == 3)
		@include('layouts.role.EmpSlidebar')
	@endif
	 <div id="flash_message_div"></div>
	 @include('layouts.navbar')


	<div class="content-wrapper">
		<div class="text-lg">
		@yield('content')

		</div>
	</div>

	  @include('layouts.includes.footer')
	  
	  @yield('page_script')

	  <script type="text/javascript">
	  	// for Message Blade
			    setTimeout(function() {
		    $('.alert').fadeOut("slow");
		     }, 8000);
	  </script>
</div>

</body>
</html>