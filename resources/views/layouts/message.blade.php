@if ($message = Session::get('success'))
        <div class="alert alert-success text-light ">
            <h3>{{ $message }}</h3>
        </div>

@endif
@if ($message = Session::get('fail'))
        <div class="alert alert-danger text-dark">
            <h3>{{ $message }}</h3>
        </div>

@endif
@if ($message = Session::get('warning'))
        <div class="alert alert-warning text-dark">
            <h3>{{ $message }}</h3>
        </div>

@endif
