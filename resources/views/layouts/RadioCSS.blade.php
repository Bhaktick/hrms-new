@section('css')
    <style type="text/css">
    [type=radio] { 
      position: absolute;
      opacity: 0;
      width: 0;
      height: 0;
    }

    /* IMAGE STYLES */
    [type=radio] + i {
      cursor: pointer;
    }

    /* CHECKED STYLES */
    [type=radio]:checked + i {
      margin: 3px;
      border: 4px solid;
      border-radius: 15px;
      animation: blink 1s;
      animation-iteration-count: 3;
    }
    @keyframes blink { 50% { border-color:#333 ; }  }


</style>
@endsection