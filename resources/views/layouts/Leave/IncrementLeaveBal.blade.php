@extends('layouts.admin.app')
@section('content')
<form method="Post" action="{{route('HR.EditLeaveBal')}}">
    @csrf
    <!-- Main content -->
    <div class="container-fluid p-5">
            <!-- SELECT2 EXAMPLE -->
        <div class="card card-warning">
            <div class="card-header">
              <h1 class="card-title text-xl text-dark"><i class="fa fa-edit nav-icon"></i> Increment Leave Balance </h1> 
            </div>
            <div class="card-body " style="overflow-x: auto;">
                <div class="row">
                    <div class="col-md-6 p-6">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                             @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                              @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="form-group ">
                            <label for="Employee">Employee Name </label>
                            <select id="getEmployee"  class="form-control " style="min-height: 300px display: inline !important;" name="Employee">
                                <option value="null" selected="true" disabled>--- Select Employee --- </option>
                                @foreach ($List as $ListItem)
                                  <option value="{{$ListItem->id}}">{{$ListItem->name}} - #{{$ListItem->id}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Employee">Employee Name </label>
                            <select id="getEmployee"  class="form-control " name="Increment_Decrement">
                                <option value="null" selected="true" disabled>--- Select Type --- </option>
                                <option value="Increment">Increment</option>
                                <option value="Decrement">Decrement</option>
                               
                            </select>
                        </div>     
                        <div class="form-group">
                                <label for="Number">Number of Leaves</label>
                                <input type="number" class="form-control  " Name="Number">
                        </div>            
                    </div>
                     <div class="col-md-6 text-center ">
                            <h1 class="p-3 m-5 h-80  shadow-lg text-center rounded float-right m-3 bg-secondery"><i class="fa fa-edit text-dark fa-5x nav-icon p-5 m-4 "></i>
                            </h1>
                     </div>        
                </div>           
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-warning">Save Pending Leave</button>
            </div>
        </div>
    </div>
</form>
@endsection
@section('page_script')
<!-- JS library for searching of employee Css is also there-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>  
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" type="text/css"> 
<script type="text/javascript">

      $(document).ready(function() {   
        $("#getEmployee").select2();  

        // $('#getEmployee').on('change', function() {
        //   var data = $("#getEmployee option:selected").val();
        //   alert(data);

        // });
      });  
    
</script> 

@endsection

