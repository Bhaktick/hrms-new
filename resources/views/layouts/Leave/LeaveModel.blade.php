@extends('layouts.admin.app')
@section('content')
@include('layouts.RadioCSS')
<div class="container-fluid p-5">
        <div class="card card-warning">
            <div class="card-header">
                <h1 class="card-title text-xl text-dark">Leave Application of {{$Emp->name}} </h1>
            </div>
            <div class="card-body " style="overflow-x: auto;">
                 
                <div class="row p-3">
                    <div class=" card col-md-4 ">   
                        <form method="Post" action="{{route('HR.LeaveAction',['id' => $Leave->id])}}">
                            @csrf
                            <h1>Form to Take Action</h1>
                            @if ($errors->any())
                            <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                 @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                  @endforeach
                                </ul>
                            </div>
                            @endif
                            @include('layouts.message')
                            @if($Leave->status == 0 && $Leave->final_status != 1)
                                <div class="form-group">
                                    <label for="remark">Remark</label>
                                    <textarea class="form-control" Name="remark"></textarea>
                                </div>
                                <div class="form-check">
                                    <div class="form-group">
                                    <label>Please select Leave is Approved or Not</label>
                                    </div>
                                    
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=1> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Approved Leave'> Approve</i> 
                                    </label>
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=2> <i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Rejected Leave'> Reject</i> 
                                    </label>
                                </div>
                                <div class="form-group text-center p-2">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Save Action">
                                </div>
                            @else
                            <h2 class="bg-info m-3 p-2 rounded">Action Already Taken !!!</h2>
                            @endif
                        </form>
                    </div>
                    <div class="card col-md-8">
                        <table class="table table-white rounded m-2">
                            <tr>
                                <th>Employee Name</th>
                                <td>{{$Emp->name}}</td>
                            </tr>
                            <tr>
                                <th>Leave_Type</th>
                                <td>{{$Leave->Leave_Type}}</td>
                            </tr>
                            <tr>
                                <th>Reason</th>
                                <td>{{$Leave->Reason}}</td>
                            </tr>
                            @if($Leave->Leave_Type != 4 && $Leave->Leave_Type != 5)
                            <tr>
                                <th>Date</th>
                                <td>{{$Leave->start_date}}</td>
                            </tr>
                            @endif
                            @if($Leave->Leave_Type == 5)
                            <tr>
                                <th>Start Date</th>
                                <td>{{$Leave->start_date}}</td>
                            </tr>
                            <tr>
                                 <th>End Date</th>
                                 <td>{{$Leave->end_date}}</td>
                            </tr>
                            @endif
                            @if($Leave->Leave_Type == 4)
                            <tr>
                                <th>Manual Dates</th>
                                <td>{{$Leave->Manual_dates}}</td>
                            </tr>
                            @endif
                            <tr>
                                <th>Total Leaves</th>
                                <td>{{$Leave->Total_Leaves}}</td>
                            </tr>
                            <tr>
                                <th>Applied Date</th>
                                <td>{{$Leave->applied_date}}</td>
                            </tr>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>


@endsection