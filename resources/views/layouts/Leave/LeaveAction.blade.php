<!DOCTYPE html>
<html>
<head>
    <title>Leave Apllication</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <h1>This Email is From  :: {{ $details['From'] }}</h1>
    <h1>{{ $details['title'] }}</h1>
    <!-- Approved -->
    @if($details['data']['status'] == 1)
        <p>
            This mail is to inform you about you Leave Application that id Applied on date {{$details['data']['applied_date']}} is Approved .

            The details About Your Application is as Follows:

        </p> 
    @else
        <p>
            This mail is to inform you about you Leave Application that id Applied on date {{$details['data']['applied_date']}} is Not Approved.

            The details About Your Application is as Follows:


        </p>
    @endif   
    <table>
        <thead>
            <th>Leave_Type</th>
            <th>Applied Date</th>
            <th>Reason</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Manual Dates</th>
            <th>Total Leaves</th>
            <th>Status</th>
        </thead>
        <tbody>
            <tr>
                <td>{{$details['data']['Leave_Type']}}</td>
                <td>{{$details['data']['applied_date']}}</td>
                <td>{{$details['data']['Reason']}}</td>
                <td>{{$details['data']['start_date']}}</td>
                <td>{{$details['data']['end_date']}}</td>
                <td>{{$details['data']['Manual_dates']}}</td>
                <td>{{$details['data']['Total_Leaves']}}</td>
                <td>{{$details['data']['status']}}</td>
            </tr>
        </tbody>
    </table>         

    


   
    <p>Thank you</p>
    <p>HRMS</p>
</body>
</html>