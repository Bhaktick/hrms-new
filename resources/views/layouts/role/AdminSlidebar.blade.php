<div class="wrapper">
  
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('admin.dashboard')}}" class="brand-link">
      <img src="{{ asset('adminlte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">HRMS - Admin</span>
    </a>
 

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            <li class="nav-item">
                <a href="{{ route('admin.dashboard')}}" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>Dashboard</p>
                </a>
            </li>       
            <li class="nav-item">
                <a href="{{route('HRList')}}" class="nav-link">
                  <i class="nav-icon fas fa-th-list"></i>
                  <p>HR List</p>
                </a>
            </li>   
<!--             <li class="nav-item">
                <a href="{{ route('CallMyLeave')}}" class="nav-link">
                  <i class="nav-icon fas fa-money-check"></i>
                  <p>My Leaves</p>
                </a>
            </li>  -->  
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item text-center text-light">
                    <H5>MANAGE</H5>
            </li>       
          <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Employees
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('HR.EmpList')}}" class="nav-link">
                  <i class="far fa-id-card nav-icon"></i>
                  <p>Employee List</p>
                </a>
              </li>        
              <li class="nav-item">
                <a href="{{ route('CallExitEmployee')}}" class="nav-link">
                  <i class="far fa-id-card nav-icon"></i>
                  <p>Ex Employee List</p>
                </a>
              </li>
            </ul>
          </li> 

          <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-plus"></i>
              <p>
                Recruit
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('HR.candidateList')}}" class="nav-link">
                  <i class="far fa-id-card nav-icon"></i>
                  <p>Candidate List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('HR.TechnicalSchedule')}}" class="nav-link">
                  <i class="far fa-calendar-alt nav-icon"></i>
                  <p>Technical Schedules</p>
                </a>
              </li> 
              <li class="nav-item">
                <a href="{{route('HR.PracticalSchedule')}}" class="nav-link">
                  <i class="far fa-calendar-alt nav-icon"></i>
                  <p>Practical Schedules</p>
                </a>
              </li>
             <li class="nav-item">
                <a href="{{ route('HR.CandidateStatus')}}" class="nav-link">
                  <i class="fa fa-tasks nav-icon"></i>
                  <p>Candidate Status</p>
                </a>
              </li>  
              <li class="nav-item">
                <a href="{{ route('HR.Selected')}}" class="nav-link">
                  <i class="fa fa-user-check nav-icon"></i>
                  <p>Selected Candidates</p>
                </a>
              </li>         
            </ul>
          </li>   

          <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Leave Management
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('HR.LeaveRequest',['option' => 0])}}" class="nav-link">
                <i class="fa fa-question-circle nav-icon"></i>
                
                  <p>Requested Leaves</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('HR.LeaveRequest',['option' => 1])}}" class="nav-link">
                  <i class="fa fa-list-alt nav-icon"></i>
                  <p>All Leaves</p>
                </a>
              </li>   
              <li class="nav-item">
                <a href="{{ route('HR.LeaveRequest',['option' => 2])}}" class="nav-link">
                  <i class="fa fa-check-circle nav-icon"></i>
                  <p> Approved Leaves</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('HR.LeaveRequest',['option' => 3])}}" class="nav-link">
                  <i class="fa fa-times-circle nav-icon"></i>
                  <p>Not Appproved Leaves</p>
                </a>
              </li>                    
            </ul>
          </li>   



           <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-taxi"></i>
              <p>
                Holiday List
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="{{ route('HolidayList')}}" class="nav-link">
                  <i class="fa fa-stream nav-icon"></i>
                  <p> Holiday  List</p>
                </a>
              </li>                      
            </ul>
          </li>  
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

<!-- ./wrapper -->
</html>
