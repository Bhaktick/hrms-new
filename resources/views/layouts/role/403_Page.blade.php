@extends('layouts.admin.app')
@section('content')
<div class="container-fluid p-5">
   <div class="card card-warning m-5">
	   	<div class="card-header text-xl"><h1><i class="fas fa-lock text-dark"></i> ERROR 403</h1></div>
	   	<div class="card-body text-center">
	   		<h2><i class="fas fa-exclamation-triangle text-warning"></i>  Access Denied </h2>
	   		<h1 class="p-5"> You Are Not Allowed Here ...</h1>
	   	</div>
   </div>
</div>
@endsection