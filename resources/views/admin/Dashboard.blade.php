@extends('layouts.admin.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
    <div class="col-sm-6 py-3">
        <h1 class="m-0">Dashboard</h1>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$counts['TotalEmp']}}</h3>
                            <h4>Total Employees</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-stalker"></i>
                        </div>
                        <a href="{{route('HR.EmpList')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
              <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{$counts['NewEmp']}}</h3>

                            <h4>New Joining</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="{{route('HR.NewEmpList')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                      <!-- ./col -->
                <div class="col-lg-3 col-6">
                        <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>{{$counts['HBD']}}</h3>
                            <h4>Today's Birthday</h4>
                        </div>
                        <div class="icon">
                            <i class="fa fa-birthday-cake"></i>
                        </div>
                        <a href="{{route('TodayHBD')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{$counts['JoinDate']}}</h3>
                             <h4>Joining Anniversary</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-clock"></i>
                        </div>
                        <a href="{{route('JoinAnniversary')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
 

        </div>
    </section>
</div>

@endsection