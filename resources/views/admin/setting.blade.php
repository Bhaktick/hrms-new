@extends('layouts.admin.app')
@section('content')

<div class="col-md-12 "> 
<form method="post" action="{{route('admin.set')}}">
		@csrf
	<div class="container-fluid py-3 col-md-10">
		<!-- /.row -->
		<div class="row py-3">
		    <div class="col-md-12">
		        <div class="card card-default ">
		          	<div class="card-header">
		            	<h3 class="card-title text-xl">Settings of site</h3>
		          	</div>
			        <div class="card-body py3">
			            <div class="bs-stepper">
			              	<div class="bs-stepper-content">
								            @include('layouts.message')
			              		
			                <!-- your steps content here -->
				                <div id="logins-part" class="content" role="tabpanel" aria-labelledby="logins-part-trigger">
					                <div class="form-group">
					                    <label for="exampleInputEmail1">Title</label>
					                    <input type="text"  class="form-control" name="key[title]">
					                </div>
					                <div class="form-group">
					                    <label for="exampleInputPassword1"> Pagination</label>
					                     <input type="number" class="form-control" name="key[pagination]" max=25>
					                </div>
					                <button type="submit" class="btn-lg btn-primary">Save Settings</button>
				                </div>
				              
			              	</div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>	
	</div>	

</form>
</div>
@endsection


