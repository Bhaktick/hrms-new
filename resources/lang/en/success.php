<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Record_updated' => 'Record has been updated successfully',
    'Record_Inserted' => 'Record has been Inserted successfully',
    'Record_Deleted' => 'Record has been Deleted successfully',
    'Invalid_FILE' => 'Invalid File Extension',
    'FILE_Large' => 'File too large. File must be less than 2MB',
    'Import_Sucess' => 'File Imported Sucessfully',
    'Invalid_Format' => 'Uploaded File Format Does Not Match to Our Format',
    'Missing_Field' => 'Some Fields are missing in Uploaded File (Hint :: Check Your number of Headers and Separator of value)',
    'Leave_success' => 'Leave Applied Success Fully',
    'Leave_Type' => 'Please Select Leave type',
    'Same_Dates' => "Manual Dates Can't Be common",
    'Leave_Approved' => 'Leave Approved Sucessfully',
    'Leave_Not_Approved' => 'Leave Rejected Successfully',
    'Cotinue_leave' => 'Continuos Leaves Should be maximum of 10 Days',
    'Increment_success' => "Pending Leave Incremented Successfully",
    'Select2' => "Please Select Employee ",
    'AddLeave_success' => "Leave Added Successfully",
    'remark' => "Remark is Required",
    'Taken_success' => "The Leave Status is Updated as Taken Leave Successfully ",
    'NotTaken_success' => "The Leave Status is Updated as Not Taken Leave Successfully",
    'EditLast_Success' => "Last Year Leave Balance Updated Sucessfully",
    'EditLast_Number' => "Last Year Leave Balance Must be Less than or Equal to 12",
    'Experience_required' => "Candidate Experience is Missing ",
    'Candidate_saved' => "Candidate Added successfully",
    'Experience_numeric' => "Candidate Experience should be numeric only ",
    'Candidate_edited' => "Candidate Edited  Successfully ",
    'Candidate_Removed' => "Candidate Deleted Successfully",
    'Technical_success' => " Technical Interview is Scheduled Successfully ",
    'Practical_success' => " Practical Interview is Scheduled Successfully ",
    'EditSchedule_success' => "Interview Schedule is Edited Successfully",
    'Technical_Result' => "Technical Interview Result Set Successfully",
    'Practical_Result' => "Practical Interview Result Set Successfully",
    'HR_Result' => "HR Interview Result Set Successfully",
    'Technical_swipe' => "The Technical Status is Swiped Successfully",
    'Practical_swipe' => "The Practical Status is Swiped Successfully",
    'HR_swipe' => "The HR Status is Swiped Successfully",
    'Selected_edited' => "Selected Candidate Edited Successfully", 
    'Employee_status' => "Employee Status is Updated Successfully",
    'Event_Added' => "Event Added Successfully",
    'Event_Edited' => "Event Edited Successfully",
    'Event_Deleted' => "Event Deleted Successfully",
    'Holiday_Added' => "Holiday Added Successfully",
    'Holiday_Updated' => "Holiday Edited Successfully",
    'Holiday_Deleted' => "Holiday Deleted Successfully",
    'Wrong_old_password' => "You have entered wrong Old Password",
    'setting_saved' => "Setting saved Successfully",

];
