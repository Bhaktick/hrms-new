<?php

use Illuminate\Database\Seeder;
use App\Repositories\Role\RoleInterface;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $roles;
    protected $data;

    public function __construct(RoleInterface $roles)
    {
        $this->roles = $roles;
        $this->data=array([
            "id"=>"1",
            "name"=>"Admin",
            "created_at"=>date('Y-m-d H:i:s')
            
        ],
        [
            "id"=>"2",
            "name"=>"HR",
            "created_at"=>date('Y-m-d H:i:s')
        ],
        [
            "id"=>"3",
            "name"=>"Employee",
            "created_at"=>date('Y-m-d H:i:s')
        ],
        );

        
    }
    public function run()
    {
        foreach($this->data as $data)
        {
            if(! ( $this->roles->find($data['id'])) )
            {
                $this->roles->create($data);
            }
        }
    }
}
