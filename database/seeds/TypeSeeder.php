<?php

use Illuminate\Database\Seeder;
use App\Repositories\Type\TypeInterface;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $type;
    protected $data;

    public function __construct(TypeInterface $type)
    {
        $this->type = $type;
        $this->data=array([
            "id"=>"1",
            "name"=>"Admin",
            "created_at"=>date('Y-m-d H:i:s')
            
        ],
        [
            "id"=>"2",
            "name"=>"HR",
            "created_at"=>date('Y-m-d H:i:s')
        ],
        [
            "id"=>"3",
            "name"=>"Employee",
            "created_at"=>date('Y-m-d H:i:s')
        ],
        );

        
    }

    public function run()
    {
        foreach($this->data as $data)
        {
            if(! ( $this->type->find($data['id'])) )
            {
                $this->type->create($data);
            }
        }
    }
}
