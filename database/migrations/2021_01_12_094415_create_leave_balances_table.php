<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Emp_id')->unsigned();
            $table->foreign('Emp_id')->references('id')->on('users');
            $table->integer('curr_year');
            $table->double('last_year_balance',3,1);
            $table->double('January',3,1)->default(0);
            $table->double('February',3,1)->default(0);
            $table->double('March',3,1)->default(0);
            $table->double('April',3,1)->default(0);
            $table->double('May',3,1)->default(0);
            $table->double('June',3,1)->default(0);
            $table->double('July',3,1)->default(0);
            $table->double('August',3,1)->default(0);
            $table->double('September',3,1)->default(0);
            $table->double('October',3,1)->default(0);
            $table->double('November',3,1)->default(0);
            $table->double('December',3,1)->default(0);
            $table->double('total',3,1)->default(0);
            $table->double('paid_total',3,1)->default(0);
            $table->double('unpaid_total',3,1)->default(0);
            $table->double('current_balance',3,1)->default(12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_balances');
    }
}
