<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Leave_Type')->nullable();//types =>1/2/3/4/5
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->text('Manual_dates')->nullable();
            $table->text('Total_Leaves')->nullable();
            $table->date('applied_date')->nullable();//can be null for dynemic Leaves
            $table->text('Reason')->nullable();
            $table->integer('Emp_id')->unsigned()->nullable();
            $table->foreign('Emp_id')->references('id')->on('users');
            $table->integer('Emp_type')->unsigned()->nullable();
            $table->foreign('Emp_type')->references('id')->on('types');
            $table->date('action_date')->nullable();
            $table->integer('Notify_status')->nullable();// 0 / 1
            $table->integer('Leave_status')->nullable();//paid/unpaid
            $table->integer('status')->nullable();//pending/approve / not approve
            $table->text('remark')->nullable();
            $table->integer('final_status')->nullable();//taken / not taken
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
