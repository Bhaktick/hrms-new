<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->text('Designation')->nullable();
            $table->text('Gender')->nullable();
            $table->integer('type')->unsigned()->nullable();
            $table->foreign('type')->references('id')->on('types');
            $table->integer('Role')->unsigned()->nullable();
            $table->foreign('Role')->references('id')->on('roles');
            $table->date('DOJ')->nullable();
            $table->date('DOB')->nullable();
            $table->text('Contact')->nullable();
            $table->text('Another_cont')->nullable();
            $table->text('Email_password')->nullable();
            $table->text('SkypeID')->nullable();
            $table->text('Skype_password')->nullable();
            $table->text('TeamloggerID')->nullable();
            $table->text('Team_password')->nullable();
            $table->text('Education')->nullable();
            $table->text('Course')->nullable();
            $table->text('Address')->nullable();
            $table->text('Personal_Email')->nullable();
            $table->text('Salary')->nullable();
            $table->text('Experience')->nullable();
            $table->text('Inc_Time_Period')->nullable();
            $table->text('Last_Inc_Date')->nullable();
            $table->text('Last_Inc_Amount')->nullable();
            $table->text('Bank_name')->nullable();
            $table->text('Acc_holder_name')->nullable();
            $table->text('Acc_no')->nullable();
            $table->text('IFSC')->nullable();
            $table->text('NDA_status')->nullable();
            $table->text('Doc_status')->nullable();
            $table->text('Doc_code')->nullable();
            $table->text('Status')->nullable();
            $table->date('relieving_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
