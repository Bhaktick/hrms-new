<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->text('can_name');
            $table->text('can_email')->nullable();
            $table->text('can_designantion');
            $table->text('can_contact');
            $table->text('can_experience')->nullable();
            $table->integer('interviewer')->unsigned()->nullable();// who is going to take interview
            $table->foreign('interviewer')->references('id')->on('users');
            $table->integer('practical_interviewer')->unsigned()->nullable();// who is going to take practical interview
            $table->foreign('practical_interviewer')->references('id')->on('users');
            $table->date('interview_date')->nullable();// theory interview date
            $table->text('interview_time')->nullable();// interview timing
            $table->integer('interview_type')->nullable();//Telephonic or Face to Face
            $table->date('practical_date')->nullable();// practical interview date
            $table->text('practical_time')->nullable();//practical interview timing
            $table->integer('practical_type')->nullable();// online or offline
            $table->integer('technical_status')->nullable();// interview pass or fail
            $table->integer('practical_status')->nullable();// interview pass or fail
            $table->integer('hr_status')->nullable();// candidate accept offer or not
            $table->text('notice_period')->nullable();//after 3 month joining like that
            $table->date('joining_date')->nullable();//date of joining after offer accept
            $table->text('remark')->nullable();//remark after final status 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
