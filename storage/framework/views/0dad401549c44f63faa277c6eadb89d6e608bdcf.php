<form method="POST" action="<?php echo e(route('HR.EditEmp',['id' => $data->id])); ?>">

  	<?php echo csrf_field(); ?>
    <?php if(Auth::user()->Role != 2): ?>
    <fieldset disabled="disabled">
    <?php endif; ?>
    <div class="card-body">
      <h1>Personal Details</h1>

    	<?php if($errors->any()): ?>
	    <div class="alert alert-danger">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
		     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		        <li><?php echo e($error); ?></li>
		      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	        </ul>
	    </div>
		<?php endif; ?>

		<?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <input type="hidden" name="form_name" value="Personal">
    <input type="hidden" name="form_id" value="<?php echo e($data->id); ?>">
      <div class="form-group">
        <label for="Personal_Email">Employee Personal Email</label>
        <input type="email" class="form-control" Name="Personal_Email" placeholder="Enter Employee Personal Email" value="<?php echo e($data->Personal_Email); ?>">
      </div>
      <div class="form-group">
        <label for="Another_cont">Altenative Contact</label>
        <input type="text" class="form-control" Name="Another_cont" placeholder="Enter Altenative Contact" value="<?php echo e($data->Another_cont); ?>">
      </div>
    <div class="form-check">
        <div class="form-group">
        <label for="Gender">Employee Gender</label>
        </div>
        <label>
            <input type="radio" class="form-check-input" Name="Gender" value=1 <?php echo e(($data->Gender == 1 ) ? 'checked' : ''); ?>> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Done' > Male</i> 
        </label>
        <label>
            <input type="radio" class="form-check-input" Name="Gender" value=2 <?php echo e(($data->Gender == 2) ? 'checked' : ''); ?>> <i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Not Done'  > Female</i> 
        </label>
    </div>
    <div class="form-group">
        <label for="DOB">Date Of Birth</label>
        <input type="text" name="DOB" class="datepicker form-control input-text full-width" placeholder="Departure Date" value="<?php echo e($data->DOB); ?>" />    
    </div>
      <div class="form-group">
        <label for="Address">Address</label>
        <textarea class="form-control" Name="Address"><?php echo e($data->Address); ?> </textarea>
      </div>
      <div class="form-group">
        <label for="Education">Education</label>
        <input type="text" class="form-control" Name="Education" placeholder="Enter Employee Education Email" value="<?php echo e($data->Education); ?>">
      </div>
      <div class="form-group">
        <label for="Course">Course</label>
        <input type="text" class="form-control" Name="Course" placeholder="Enter Employee Course" value="<?php echo e($data->Course); ?>">
      </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </fieldset>
</form>
<?php $__env->startSection('page_script'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">
$(document).ready(function () {

        var today = new Date();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
</script>
<?php $__env->stopSection(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/EditEmp/EditPersonal.blade.php ENDPATH**/ ?>