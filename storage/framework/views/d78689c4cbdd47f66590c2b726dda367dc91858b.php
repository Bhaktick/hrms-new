<div class="hold-transition sidebar-mini layout-fixed ">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
          <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          </li>
          <?php if(Auth::user()->Role == 2): ?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo e(route('HR.CallAddEvents')); ?>"><i class="fas fa-calendar-plus text-lg"></i></a>
            </li>
          <?php endif; ?>
        </ul>


        <!-- Right navbar links -->
  
            <!-- User Option Dropdown Menu -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
              <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="far fa-bell"></i>
                  <?php if($notify['count'] !=0): ?>
                  <span class="badge badge-info text-sm navbar-badge"><?php echo e($notify['count']); ?></span>
                  <?php endif; ?>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                  <span class="dropdown-item dropdown-header"><?php echo e($notify['count']); ?> Notifications</span>
                    <?php if(Auth::user()->Role == 1 || Auth::user()->Role == 2): ?>
                    <div class="dropdown-divider"></div>
                        <a href="<?php echo e(route('HR.LeaveRequest',['option' => 0])); ?>" class="dropdown-item">
                            <i class="fas fa-user-tie mr-2"></i><?php echo e($notify['RequestCount']); ?> New Leave Application
                        </a>
                    <?php endif; ?>
                    <?php if(Auth::user()->Role == 3 || Auth::user()->Role == 2): ?>
                        <div class="dropdown-divider"></div>
                        <a href="<?php echo e(route('CallMyLeave')); ?>" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i><?php echo e($notify['LeaveCount']); ?> Update of Leave Application
                        </a>
                    <?php endif; ?>    
                        <div class="dropdown-divider"></div>
                        <a href="<?php echo e(route('HR.EventsList')); ?>" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i><?php echo e($notify['EventCount']); ?> Event Today
                        </a>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="fas fa-user-cog"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                  <span class="dropdown-item dropdown-header">USER OPTIONS</span>
                  <div class="dropdown-divider"></div>
                  <a href="<?php echo e(route('admin.profile')); ?>" class="dropdown-item">
                   <p>Profile</p>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="<?php echo e(route('admin.change')); ?>" class="dropdown-item">
                   <p>Reset Password</p>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="<?php echo e(route('admin.Settings')); ?>" class="dropdown-item">
                   <p>Settings</p>
                  </a>
                  <div class="dropdown-divider"></div>
                  <form method="post" action="<?php echo e(route('logout')); ?>">
                    <?php echo csrf_field(); ?>
                    <button class="dropdown-item" type="submit">
                       <p>Logout</p>
                    </button>
                  </form>
                </div>
            </li>
        </ul>
    </nav>
    <!-- /.Navbar -->
</div>
<?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/navbar.blade.php ENDPATH**/ ?>