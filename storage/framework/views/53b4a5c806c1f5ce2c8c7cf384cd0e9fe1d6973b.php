
<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
    <div class="col-sm-6 py-3">
        <h1 class="m-0">Dashboard</h1>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo e($counts['TotalEmp']); ?></h3>
                            <h4>Total Employees</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-stalker"></i>
                        </div>
                        <a href="<?php echo e(route('HR.EmpList')); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
              <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3><?php echo e($counts['NewEmp']); ?></h3>

                            <h4>New Joining</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo e(route('HR.NewEmpList')); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                      <!-- ./col -->
                <div class="col-lg-3 col-6">
                        <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?php echo e($counts['HBD']); ?></h3>
                            <h4>Today's Birthday</h4>
                        </div>
                        <div class="icon">
                            <i class="fa fa-birthday-cake"></i>
                        </div>
                        <a href="<?php echo e(route('TodayHBD')); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?php echo e($counts['JoinDate']); ?></h3>
                             <h4>Joining Anniversary</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-clock"></i>
                        </div>
                        <a href="<?php echo e(route('JoinAnniversary')); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
 

        </div>
    </section>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/admin/dashboard.blade.php ENDPATH**/ ?>