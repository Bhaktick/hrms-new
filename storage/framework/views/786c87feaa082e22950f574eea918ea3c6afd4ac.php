
<?php $__env->startSection('content'); ?>
<!-- Main content -->
<div class="container-fluid p-5">
        <!-- SELECT2 EXAMPLE -->
    <div class="card card-warning">
        <div class="card-header">
          	<?php if($option == 0): ?>
          		<h1 class="card-title text-xl text-dark" id="title"><i class="fa fa-question-circle nav-icon"></i> Requested Leaves</h1>
          	<?php elseif($option == 1): ?>
          		<h1 class="card-title text-xl text-dark" id="title"><i class="fa fa-list-alt nav-icon"></i> All Leaves</h1>
          	<?php elseif($option == 2): ?>
          		<h1 class="card-title text-xl text-dark" id="title"><i class="fa fa-check-circle nav-icon"></i> Approved Leaves</h1>
          	<?php elseif($option == 3): ?>
          		<h1 class="card-title text-xl text-dark" id="title"><i class="fa fa-times-circle nav-icon"></i> Not Approved Leaves</h1>
          	<?php endif; ?>
          	<input type="hidden" id="option" value="<?php echo e($option); ?>">
          </h1>
        </div>
        <div class="card-body " style="overflow-x: auto;">
            <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php if(Auth::user()->Role == 2): ?>
                <a href="<?php echo e(route('HR.EditLeaveData',['code'=> 1])); ?>" class="btn btn-primary my-2"><i class="fa fa-plus nav-icon"></i> Add Leave</a>
                <a href="<?php echo e(route('HR.EditLeaveData',['code' => 2])); ?>" class="btn btn-primary my-2"><i class="fa fa-plus nav-icon"></i> Increment Pending Leave</a>
                <a href="<?php echo e(route('HR.EditLeaveData',['code' => 3])); ?>" class="btn btn-primary my-2"><i class="fa fa-edit nav-icon"></i> Edit Last Year Leaves</a>
            <?php endif; ?>
            <form method="POST">
                <?php echo csrf_field(); ?>
                <table class="table table-hover yajra-datatable" id="Leave">
                    <thead>
                        <th>Employee Name</th>
                        <th>Leave Type</th>
                        <th>Reason</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Manual Dates</th>
                        <th>Total Leaves</th>
                        <th>Applied Date</th>
                        <th><?php if($option == 0): ?> Action <?php else: ?> Status <?php endif; ?></th>
                        <th>Final Status</th>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </form>
        </div>
    </div>

</div>

          <!-- /.card-header -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
    <!-- Public/vendor/...  -->
    <script src="https://datatables.net/extensions/buttons/examples/html5/simple.html"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
     <!-- for buttons --> 

     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">

       
<script type="text/javascript">
  $(function () {
  	var option = $("#option").val();
  	var url = '<?php echo e(route("HR.LeaveData", ":id")); ?>';
  	url = url.replace(':id',option);


  	// alert(option);
    var table = $('#Leave').removeAttr('width').DataTable({
        serverside :true,
        processing: true,
        scrollX: true,
        aaSorting: [],// to stop initial sorting 
        ajax: url,
        columnDefs: [
            { width: 100, targets: 3 },
            { width: 100, targets: 4 },
            { width: 100, targets: 5 },
            { width: 100, targets: 7 },
        ],
        columns: [
            {data: 'Emp_name', name: 'Emp_name'},
            {data: 'Leave_Type', name: 'Leave_Type'},
            {data: 'Reason', name: 'Reason'},            
            {data: 'Sdate', name: 'Sdate'},
            {data: 'Edate', name: 'Edate'},
            {data: 'Manual_dates', name: 'Manual_dates'},
            {data: 'Total_Leaves', name: 'Total_Leaves'},
            {data: 'Apply_date', name: 'applied_date'},
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
            {
                data: 'status', 
                name: 'status', 
                orderable: true, 
                searchable: true
            },

        ]
    });    
 
// $('#LeaveAction').click(function()
//     {
//         //action="route('HR.LeaveAction',['id' => $row->id,'status' => 1])"
//         var id = $(this).attr('data-id');
//         alert(id);

//     });

});         
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/Leave/LeaveData.blade.php ENDPATH**/ ?>