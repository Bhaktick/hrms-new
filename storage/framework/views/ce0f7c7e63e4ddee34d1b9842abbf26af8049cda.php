<form method="POST" action="<?php echo e(route('HR.EditEmp',['id' => $data->id])); ?>">
  	<?php echo csrf_field(); ?>
    <?php if(Auth::user()->Role != 2): ?>
    <fieldset disabled="disabled">
    <?php endif; ?>
    <div class="card-body">
        <h1>Bank Details</h1>

    	<?php if($errors->any()): ?>
	    <div class="alert alert-danger">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
		     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		        <li><?php echo e($error); ?></li>
		      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	        </ul>
	    </div>
		<?php endif; ?>

		<?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <input type="hidden" name="form_name" value="Bank">
    <input type="hidden" name="form_id" value="<?php echo e($data->id); ?>">
      <div class="form-group">
        <label for="Bank_name">Bank Name</label>
        <input type="text" class="form-control" Name="Bank_name" placeholder="Enter Bank Name" value="<?php echo e($data->Bank_name); ?>">
      </div>
      <div class="form-group">
        <label for="Acc_holder_name">Bank Account Holder Name</label>
        <input type="text" class="form-control" Name="Acc_holder_name" placeholder="Enter Bank Account Holder Name" value="<?php echo e($data->Acc_holder_name); ?>">
      </div>
       <div class="form-group">
        <label for="Acc_no">Bank Account Number</label>
        <input type="text" class="form-control" Name="Acc_no" placeholder="Enter Account Number" value="<?php echo e($data->Acc_no); ?>">
      </div>
      <div class="form-group">
        <label for="IFSC">Bank IFSC </label>
        <input type="text" class="form-control" Name="IFSC" placeholder="Enter Bank IFSC" value="<?php echo e($data->IFSC); ?>">
      </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </fieldset>
</form>
<?php $__env->startSection('page_script'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">
$(document).ready(function () {

        var today = new Date();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
</script>
<?php $__env->stopSection(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/EditEmp/EditBank.blade.php ENDPATH**/ ?>