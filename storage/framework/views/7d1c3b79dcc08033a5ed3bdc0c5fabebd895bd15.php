<?php if($message = Session::get('success')): ?>
        <div class="alert alert-success text-light ">
            <h3><?php echo e($message); ?></h3>
        </div>

<?php endif; ?>
<?php if($message = Session::get('fail')): ?>
        <div class="alert alert-danger text-dark">
            <h3><?php echo e($message); ?></h3>
        </div>

<?php endif; ?>
<?php if($message = Session::get('warning')): ?>
        <div class="alert alert-warning text-dark">
            <h3><?php echo e($message); ?></h3>
        </div>

<?php endif; ?>
<?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/message.blade.php ENDPATH**/ ?>