<div class="wrapper">
  
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo e(route('admin.dashboard')); ?>" class="brand-link">
      <img src="<?php echo e(asset('adminlte/dist/img/AdminLTELogo.png')); ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">HRMS - Employee</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="<?php echo e(route('admin.dashboard')); ?>" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>Dashboard</p>
                </a>
            </li>       
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Leave
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo e(route('CallApplyLeave',['mode' => 0])); ?>" class="nav-link">
                  <i class="fa fa-mail-bulk nav-icon"></i>
                  <p> Apply Leave</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo e(route('CallMyLeave')); ?>" class="nav-link">
                  <i class="nav-icon fas fa-money-check"></i>
                  <p>My Leaves</p>
                </a>
            </li>
              <li class="nav-item">
                <a href="<?php echo e(route('CallApplyLeave',['mode' => 0])); ?>" class="nav-link">
                  <i class="fa fa-balance-scale nav-icon"></i>
                  <p>Leave Balance</p>
                </a>
              </li>                       
            </ul>
          </li>   

           <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-taxi"></i>
              <p>
                Holiday List
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="<?php echo e(route('HolidayList')); ?>" class="nav-link">
                  <i class="fa fa-stream nav-icon"></i>
                  <p> Holiday  List</p>
                </a>
              </li>                      
            </ul>
          </li>  
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

<!-- ./wrapper -->
</html>
<?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/role/EmpSlidebar.blade.php ENDPATH**/ ?>