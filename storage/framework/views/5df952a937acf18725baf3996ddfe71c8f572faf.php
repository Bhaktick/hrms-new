
<?php $__env->startSection('content'); ?>

<div class="container-fluid p-5">
<!-- /.row -->
<?php if($mode == 0): ?>
	<form method="Post" class="formMode" action="<?php echo e(route('ApplyLeave',['mode' => 0])); ?>" name="mode0">
<?php elseif($mode == 1): ?>
	<form method="Post" class="formMode" action="<?php echo e(route('ApplyLeave',['mode' => 1])); ?>" name="mode1">
<?php endif; ?>
<!-- do not change name of form connected with Jquery -->
	<div class=" card card-info">
		<div class="card-header">
			<h1 class="card-title text-xl"> 
				<?php if($mode == 0): ?>
					<i class="fa fa-mail-bulk nav-icon"></i> Apply Leave
				<?php elseif($mode == 1): ?>
					<i class="fa fa-plus nav-icon"></i> Add Paid Leave
				<?php endif; ?>
			</h1>
		</div>
		<div class="card-body">
			<div class="row">
                <div class="col-md-6 p-5">
		        	<?php if($errors->any()): ?>
		    	    <div class="alert alert-danger">
		    	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
		    	        <ul>
		    		     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		    		        <li><?php echo e($error); ?></li>
		    		      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		    	        </ul>
		    	    </div>
		    		<?php endif; ?>

		    		<?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

						<?php echo csrf_field(); ?>

						<?php if($mode == 0): ?>
							<div class="form-group">
					            <label for="From">Your Email</label>
					            <input type="text" class="form-control" Name="From" value="<?php echo e(Auth::user()->email); ?>" readonly>
					      	</div>
						<?php elseif($mode == 1): ?>
							<div class="form-group">
					            <label for="From">Employee Name</label>
					            <select class="form-control" id="getEmployee" name="Employee">
		                    		<option value="null" selected="true" disabled>--- Select Employee --- </option>
								    <?php $__currentLoopData = $List; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ListItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		   				 		        <option value="<?php echo e($ListItem->id); ?>"><?php echo e($ListItem->name); ?> - #<?php echo e($ListItem->id); ?></option>
		    		      			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							    </select>
					      	</div>
					      	<div class="form-group">
		                        <label for="remark">Remark</label>
		                        <textarea class="form-control" Name="remark"></textarea>
		                    </div>
					      	<!-- THE From Email id for To and From will be from backend applyLeave mode ==1   -->
						<?php endif; ?>
						
				      	<?php if(auth::user()->Role == 2 && $mode != 1): ?>
				      		<div class="form-group">
				            	<label for="To">To(ADMIN)</label>
				            	<input type="text" class="form-control" Name="To" value="harsh@gmail.com" readonly>
				      		</div>
				      	<?php else: ?>
				      		<div class="form-group">
				            	<label for="To">To(HR)</label>
				            	<input type="text" class="form-control" Name="To" value="poorvicoderkube2020@gmail.com" readonly>
				      		</div>
				      	<?php endif; ?>
				      	
				      	<div class="form-group">
				            <label for="Reason">Reason</label>
				            <input type="text" class="form-control " Name="Reason" placeholder="Enter Reason For Leave">
				      	</div>
				      	<div class="form-group">
				            <label for="Leave_Type">Leave Type</label>
				            <select name="Leave_Type" id="Leave_Type" class="form-control">
				            	<option value="" disabled="true" selected="true">--- Select Leave Type ---</option>
				            	<option value="1">Full Day</option>
				            	<option value="2">First Half Day</option>
				            	<option value="3">Second Half Day</option>
				            	<option value="4">Manual Days</option>
				            	<option value="5">Continuos Days</option>
				            </select>
				      	</div>

				      	<!-- continue leaves -->
				      		<div id="continue">
						      	<div class="form-group">
						            <label for="start">Start Date</label>
						            <input type="text" name="start" id="start" class="datepicker bg-white form-control input-text full-width" placeholder="Departure Start Date" readonly/>
						      	</div>
						      	<div class="form-group">
						            <label for="end_date">End Date</label>
						            <input type="text" name="end_date" id="end" class="datepicker bg-white form-control input-text full-width" placeholder="Departure End Date" disabled="true" readonly/>
						      	</div>
						    </div>
		   		      	<!-- /continue leaves -->
				      	<!-- manual leaves -->
					    	<div id="manual">
					    		<div class="form-group">
						            <label for="name">Number of Days</label>
						            <div class="row p-2">
							            <input type="number" name="Total_Leaves" id="manualAdd" class="form-control " placeholder="Enter number of Days" />
							            <input type="button" id="add" value="Proceed" class="btn btn-info">
						            </div>
						      	</div>
						      	<div class="form-group">
						      		<label for="Manual_dates">Manual Dates</label>
							     	<div id="addHere">

							     	</div>
						      	</div>
						    </div>
						<!-- /manual leaves -->  
						<!-- halfday leaves -->  
		 		      		<div id="halfday">
						    	<div class="form-group">
						            <label for="date_half">Select Date</label>
						            <input type="text" name="date_half" class="datepicker bg-white form-control input-text full-width" placeholder="Departure Date" readonly />
						      	</div>
						    </div>
						<!-- /halfday leaves -->    
						<!-- fullday leaves -->
					    	<div id="fullday">
						    	<div class="form-group">
						            <label for="date_full">Select Date</label>
						            <input type="text" name="date_full" class="datepicker bg-white form-control input-text full-width" placeholder="Departure Date"  readonly/>
						      	</div>
						    </div>  	
				      	<!-- /fullday leaves -->
				      	
				      	<div class="form-group">
				            <input type="hidden" class="form-control" Name="Emp_id" value="<?php echo e(Auth::user()->Emp_id); ?>" >
				             <input type="hidden" class="form-control " Name="Emp_type" value="<?php echo e(Auth::user()->Emp_type); ?>" >
				      	</div>
				</div>  
				<div class="col-md-6 text-center  ">
                        <h1 class="p-3 m-5 h-80 shadow-lg text-center rounded float-right m-3 bg-secondery">
             				<?php if($mode == 0): ?>	
                                <i class="fa fa-mail-bulk text-dark fa-5x nav-icon p-5 m-4 "></i>
                         	<?php elseif($mode == 1): ?>
                         		<i class="fa fa-plus text-dark fa-5x nav-icon p-5 m-4 "></i>
                         	<?php endif; ?>
                        </h1>
                </div>		
		    </div>
		    <div>
		    	
		    </div>
		</div>
		<div class="card-footer">
			
			<?php if($mode == 0): ?>
	      	<button type="submit" class="btn btn-warning">Send Leave Application</button>
	      	<?php elseif($mode == 1): ?>
	      	<button type="submit" class="btn btn-warning">Save Leave </button>
	      	<?php endif; ?>
	      	
	    </div>
	</div>
</form>
</div>

<template id="mytemplate">
  <input type="text" name="Manual_dates[]" class="datepicker bg-white m-2 form-control input-text full-width" placeholder="Departure Date"  readonly/>
</template>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<!-- JS library for searching of employee Css is also there-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>  
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" type="text/css"> 



<script type="text/javascript">
$(document).ready(function () {


		// to hide this elements
		$("#continue").hide();
		$("#manual").hide();
		$("#halfday").hide();
		$("#fullday").hide();

		$("#getEmployee").select2();  // for employee List


	       var today = new Date();
	       var end = new Date();
		function callDatepicker()
		{
			var mode = $('.formMode').attr('name');
			if(mode == "mode1")
			{
			
				$('.datepicker').datepicker({
		        	// to format and manage datepicker controller
		            format: 'yyyy-mm-dd',
		            todayHighlight: true,
		            autoclose:true,
		            startDate: "-3m",
		            daysOfWeekDisabled: '0',
		        }).on('changeDate', function (ev) {
		                $(this).datepicker('hide');
		                $("#end").attr('disabled',false);
		                end =this.value;
		               	$("#end").datepicker('setStartDate',end); 
		            });
			}
			else
			{


				$('.datepicker').datepicker({
		        	// to format and manage datepicker controller
		            format: 'yyyy-mm-dd',
		            todayHighlight: true,
		            autoclose:true,
		            startDate: "today",
		            daysOfWeekDisabled: '0',
		        }).on('changeDate', function (ev) {
		                $(this).datepicker('hide');
		                $("#end").attr('disabled',false);
		                end =this.value;
		               	$("#end").datepicker('setStartDate',end); 
		            });
	    	}

	        $('.datepicker').keyup(function () {
	           if (this.value.match(/[^0-9]/g)) {
	                this.value = this.value.replace(/[^0-9^-]/g, '');
	            }
	            
	        });

		}

        $("#add").click(function() {
        	// to add manual date elements in form dynamically
	        	var i=0;
	        	var num = $("#manualAdd").val();
	        	if(num > 5)
	        	{
	        		alert("Manual Leaves Can't be More than 5");
	        		$('#addHere').html('');
	        	}
	        	else
	        	{
	        		for(i=0;i<num;i++)
		        	{
		        	// 	// alert(control);
		        	// $($('#mytemplate').html()).insertAfter( $('#addHere') );
		        	$('#addHere').append($('#mytemplate').html());
		        		callDatepicker();
		           	}
	        	}
	        	


			});
        $("#Leave_Type").change(function () {
        	// to activate proper part as per the leave type
	        	var check = $(this).val();
	        	// alert(check);
	        	if(check == 5)//continue day
	        	{
	        		$("#continue").show();
					$("#manual").hide();
					$("#halfday").hide();
					$("#fullday").hide();
	        	}
	        	else if(check == 4)// manual day
				{
					$("#continue").hide();
					$("#manual").show();
					$("#halfday").hide();
					$("#fullday").hide();
				}
				else if(check == 2 || check == 3)// first-half and second half
				{
					$("#continue").hide();
					$("#manual").hide();
					$("#halfday").show();
					$("#fullday").hide();
				}
				else if(check == 1)// fullday
				{
					$("#continue").hide();
					$("#manual").hide();
					$("#halfday").hide();
					$("#fullday").show();
				}
				callDatepicker();                
            });


        

    });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/Emp/ApplyLeave.blade.php ENDPATH**/ ?>