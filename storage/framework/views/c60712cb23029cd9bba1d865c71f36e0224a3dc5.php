<form method="POST" action="<?php echo e(route('HR.EditEmp',['id' => $data->id])); ?>" id="Primary">
  	<?php echo csrf_field(); ?>
    <?php if(Auth::user()->Role != 2): ?>
    <fieldset disabled="disabled">
    <?php endif; ?>
    <div class="card-body">
        <h1>Primary Details</h1>
        	<?php if($errors->any()): ?>
    	    <div class="alert alert-danger">
    	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
    	        <ul>
    		     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    		        <li><?php echo e($error); ?></li>
    		      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    	        </ul>
    	    </div>
    		<?php endif; ?>
    		<?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="form-group">
                <input type="hidden" name="form_name" value="Primary">
                <input type="hidden" name="form_id" value="<?php echo e($data->id); ?>">
        </div>
          <div class="form-group">
            <label for="name">Employee Name</label>
            <input type="text" class="form-control" Name="name" value="<?php echo e($data->name); ?>">
          </div>
          <div class="form-group">
            <label for="email">Employee Company Email</label>
            <input type="email" class="form-control" Name="email" value="<?php echo e($data->email); ?>" disabled="true">
          </div>
          <div class="form-group">
            <label for="Email_password">Company Mail Password</label>
            <input type="text" class="form-control" Name="Email_password" value="<?php echo e($data->Email_password); ?>">
          </div>
          <div class="form-group">
            <label for="Designation">Employee Designation</label>
            <input type="text" class="form-control" Name="Designation" value="<?php echo e($data->Designation); ?>">
          </div>
          <div class="form-group">
            <label for="Contact">Employee Contact</label>
            <input type="text" class="form-control" Name="Contact" value="<?php echo e($data->Contact); ?>">
          </div>
          <div class="form-group">
            <label for="Salary">Employee Salary</label>
            <input type="text" class="form-control" Name="Salary" value="<?php echo e($data->Salary); ?>">
          </div>
          <div class="form-group">
            <label for="DOJ">Employee Date of Joining</label>
            <input type="text" name="DOJ" class="datepicker form-control input-text full-width" placeholder="Departure Date" value="<?php echo e($data->DOJ); ?>" />
            </div>

          </div>

    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
    </fieldset>
</form>
<?php $__env->startSection('page_script'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">
$(document).ready(function () {


        // $("#Primary :input").prop("disabled", true); 

        var today = new Date();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
</script>
<?php $__env->stopSection(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/EditEmp/EditPrimary.blade.php ENDPATH**/ ?>