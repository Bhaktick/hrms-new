
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.RadioCSS', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="container-fluid p-5">
        <div class="card card-warning">
            <div class="card-header">
                <h1 class="card-title text-xl text-dark">Form to Relieving Candidate</h1>
            </div>
            <div class="card-body " style="overflow-x: auto;">
                 
                <div class="row p-3">
                    <div class=" card col-md-4 ">   
                        <form method="Post" action="<?php echo e(route('HR.SaveFinal',['id' => $candidate->id])); ?>">
                            <?php echo csrf_field(); ?>
                            <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                 <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            <?php endif; ?>
                            <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php if($candidate->hr_status == null || $mode == 1): ?>
                                <div class="form-group">
                                    <label for="remark">Remark</label>
                                    <?php if($mode == 1): ?>
                                    <textarea class="form-control" Name="remark"><?php echo e($candidate->remark); ?></textarea>
                                    <?php else: ?>
                                    <textarea class="form-control" Name="remark"></textarea>
                                    <?php endif; ?>
                                    
                                </div>
                                <div class="form-check">
                                    <div class="form-group">
                                    <label>Please select Candidate is Selected or Not</label>
                                    </div>
                                    
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=1 <?php echo e(($candidate->hr_status == 1 && $mode == 1) ? 'checked' : ''); ?>> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Approved Leave' >Selected</i> 
                                    </label>
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=2 <?php echo e(($candidate->hr_status == 2 && $mode == 1) ? 'checked' : ''); ?>> <i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Rejected Leave'  >Rejected</i> 
                                    </label>
                                </div>
                                <div class="form-group text-center p-2">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Save Action">
                                </div>
                            <?php elseif($mode != 1): ?>
                            <!-- not edit mode then -->
                            <h2 class="bg-info m-3 p-2 rounded">Action Already Taken !!!</h2>
                            <?php endif; ?>
                        </form>
                    </div>
                    <div class="card col-md-8">
                        <table class="table table-white rounded m-2">
                            <tr>
                                <th>Candidate Name</th>
                                <td><?php echo e($candidate->can_name); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Email</th>
                                <td><?php echo e($candidate->can_email); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Contact</th>
                                <td><?php echo e($candidate->can_contact); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Designation</th>
                                <td><?php echo e($candidate->can_designantion); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Experience</th>
                                <td><?php echo e($candidate->can_experience); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Technical Status</th>
                                <td>
                                    <?php if($candidate->technical_status == 1): ?>
                                    <i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Selected'>
                                    <?php elseif($candidate->technical_status == 2): ?>
                                    <i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Candidate Parctical Status</th>
                                <td><?php if($candidate->practical_status == 1): ?>
                                    <i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Selected'>
                                    <?php elseif($candidate->practical_status == 2): ?>
                                    <i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'>
                                    <?php endif; ?></td>
                            </tr>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/Recruit/ResultModel.blade.php ENDPATH**/ ?>