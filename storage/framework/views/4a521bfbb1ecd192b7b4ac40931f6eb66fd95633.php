
<?php $__env->startSection('content'); ?>
<div class="container-fluid p-5">

   <div class="card card-info ">
        <div class="card-header ">
            <h1 class="card-title text-xl"><i class="fa fa-edit nav-icon"></i> 
                <?php if($Itype == 0): ?>
                    Edit Technical Interview Schedule
                <?php elseif($Itype == 1): ?>
                    Edit Practical Interview Schedule
                <?php endif; ?>    
            </h1>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-6 p-4">
                    <form method="Post" action="<?php echo e(route('HR.EditSchedule',['id' => $candidate->id,'Itype' => $Itype])); ?>">
                         <?php echo csrf_field(); ?>
                        <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                  <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                        <?php endif; ?>
                        <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php if($Itype == 0): ?>
                            <div class="form-group">
                                <label for="TInterviewer">Technical Interviewer</label>                            
                                <select id="getInterviewer"  class="form-control " style="min-height: 300px display: inline !important;" name="TInterviewer">
                                    <option value="null"  disabled>--- Select Employee --- </option>
                                    <?php $__currentLoopData = $List; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ListItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($ListItem->id); ?>" <?php echo e($candidate->interviewer  == $ListItem->id ? 'selected' : ''); ?> ><?php echo e($ListItem->name); ?> - #<?php echo e($ListItem->id); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="TDate">Technical Interview Date</label>
                                <input type="text" name="TDate" class="datepicker form-control input-text full-width" placeholder="Departure Date"  value="<?php echo e($candidate->interview_date); ?>" />
                            </div>
                            <div class="form-group">
                                <label for="TTime">Technical Interview Time</label>
                                <input class="timepicker form-control" name="TTime" type="text" placeholder="Departure Start Time" value="<?php echo e($candidate->interview_time); ?>">
                            </div>
                            <div class="form-group">
                                <label for="type">Technical Interview Type</label>
                                <select class="form-control " name="type">
                                        <option selected> -- Select Interview Type -- </option>
                                        <option value="0" <?php echo e($candidate->interview_type  == 0 ? 'selected' : ''); ?>>online</option>
                                        <option value="1" <?php echo e($candidate->interview_type  == 1 ? 'selected' : ''); ?>>offline</option>
                                </select>
                            </div>      
                        <?php elseif($Itype == 1): ?>
                            <div class="form-group">
                                <label for="PInterviewer">Practical Interviewer</label>                            
                                <select id="getInterviewer"  class="form-control " style="min-height: 300px display: inline !important;" name="PInterviewer">
                                    <option value="null"  disabled>--- Select Employee --- </option>
                                    <?php $__currentLoopData = $List; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ListItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($ListItem->id); ?>" <?php echo e($candidate->practical_interviewer   == $ListItem->id ? 'selected' : ''); ?> ><?php echo e($ListItem->name); ?> - #<?php echo e($ListItem->id); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="PDate">Practical Interview Date</label>
                                <input type="text" name="PDate" class="datepicker form-control input-text full-width" placeholder="Departure Date"  value="<?php echo e($candidate->practical_date); ?>" />
                            </div>
                            <div class="form-group">
                                <label for="PTime"> Practical Interview Time</label>
                                <input class="timepicker form-control" name="PTime" type="text" placeholder="Departure Start Time" value="<?php echo e($candidate->practical_time); ?>">
                            </div>
                            <div class="form-group">
                                <label for="Ptype">Practical Interview Type</label>
                                <select class="form-control " name="Ptype">
                                        <option disabled="true" selected> -- Select Interview Type -- </option>
                                        <option value="0" <?php echo e($candidate->practical_type  == 0 ? 'selected' : ''); ?>>online</option>
                                        <option value="1" <?php echo e($candidate->practical_type  == 1 ? 'selected' : ''); ?>>offline</option>
                                </select>
                            </div>  
                        <?php endif; ?>            
                </div>
                <div class="col-md-6 text-center  ">
                        <div class="p-5 m-5 h-80 shadow-lg text-center rounded float-right bg-secondery">
                            <h1>Candidate Data</h1>
                            <table class="table table-info rounded">
                                <tr>
                                    <th>Name</th>
                                    <td><?php echo e($candidate->can_name); ?></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><?php echo e($candidate->can_email); ?></td>
                                </tr>
                                <tr>
                                    <th>Contact</th>
                                    <td><?php echo e($candidate->can_contact); ?></td>                           
                                </tr>    
                                <tr>
                                    <th>Designation</th>
                                    <td><?php echo e($candidate->can_designantion); ?></td>              
                                </tr>
                                <tr>
                                    <th>Experience</th>
                                    <td><?php echo e($candidate->can_experience); ?></td>
                                </tr>
                            </table>

                        </div>
                </div>
            </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Submit</button>
        </div>
        </form>
    </div>
</div>
            


<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


  <!-- timepicker links here  -->

  <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.min.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.js"></script>

<!-- JS library for searching of employee Css is also there-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>  
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" type="text/css"> 

<script type="text/javascript">
$(document).ready(function () {

        $("#getInterviewer").select2();  


           var today = new Date();
    $("#end").attr('disabled',true);
    $('.datepicker').datepicker({
        // to format and manage datepicker controller
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose:true,
        startDate: "today",
        daysOfWeekDisabled: '0',
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
            $("#end").attr('disabled',false);
            end =this.value;
            $("#end").datepicker('setStartDate',end); 
        });

        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });

         $('.timepicker').timepicker({
                'disableTextInput' : false,
                'timeFormat' : 'g:i A',
                'step': 15,
                'minTime' : '10:00 AM',
                'maxTime' : '8:00 PM',
                'disableTimeRanges': [
                    ['1:00 PM', '2:00 PM'],
                ]
            }).on('selectTime', function (ev) {
                $(this).timepicker('hide');
                end =this.value;
            }); 



    });





    

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/Recruit/EditSchedule.blade.php ENDPATH**/ ?>