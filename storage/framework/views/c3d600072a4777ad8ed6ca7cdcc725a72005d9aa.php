<!DOCTYPE html>
<html>
<head>
    <title>Leave Apllication</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <h1>This Email is From  :: <?php echo e($details['From']); ?></h1>
    <h1><?php echo e($details['title']); ?></h1>
    <?php if($details['data']['status'] == 1): ?>
        <p>
            This mail is to inform you about you Leave Application that id Applied on date <?php echo e($details['data']['applied_date']); ?> is Approved .

            The details About Your Application is as Follows:

        </p> 
    <?php else: ?>
        <p>
            This mail is to inform you about you Leave Application that id Applied on date <?php echo e($details['data']['applied_date']); ?> is Not Approved.

            The details About Your Application is as Follows:


        </p>
    <?php endif; ?>   
    <table>
        <thead>
            <th>Leave_Type</th>
            <th>Applied Date</th>
            <th>Reason</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Manual Dates</th>
            <th>Total Leaves</th>
            <th>Status</th>
        </thead>
        <tbody>
            <tr>
                <td><?php echo e($details['data']['Leave_Type']); ?></td>
                <td><?php echo e($details['data']['applied_date']); ?></td>
                <td><?php echo e($details['data']['Reason']); ?></td>
                <td><?php echo e($details['data']['start_date']); ?></td>
                <td><?php echo e($details['data']['end_date']); ?></td>
                <td><?php echo e($details['data']['Manual_dates']); ?></td>
                <td><?php echo e($details['data']['Total_Leaves']); ?></td>
                <td><?php echo e($details['data']['status']); ?></td>
            </tr>
        </tbody>
    </table>         

    


   
    <p>Thank you</p>
    <p>HRMS</p>
</body>
</html><?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/Leave/LeaveAction.blade.php ENDPATH**/ ?>