
<?php $__env->startSection('content'); ?>

<div class="container-fluid p-5">
<!-- /.row -->
	<form method="Post" class="formMode" action="<?php echo e(route('HR.AddCandidate',['code' => 1])); ?>">

<!-- do not change name of form connected with Jquery -->
	<div class=" card card-primary">
		<div class="card-header"> 
				<h1><i class="fa fa-edit nav-icon"></i> Edit Candidate</h1>
		</div>
		<div class="card-body ">
			<div class="row">
				<div class="col-md-6 p-4">
		        	<?php if($errors->any()): ?>
		    	    <div class="alert alert-danger">
		    	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
		    	        <ul>
		    		     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		    		        <li><?php echo e($error); ?></li>
		    		      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		    	        </ul>
		    	    </div>
		    		<?php endif; ?>

		    		<?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

						<?php echo csrf_field(); ?>		      	
						    <input type="hidden" name="can_id" value="<?php echo e($candidate->id); ?>">

				      	<div class="form-group">
				            <label for="Name">Candidate Name</label>
				            <input type="text" class="form-control " Name="Name" placeholder="Enter Candidate Name here..." value="<?php echo e($candidate->can_name); ?>">
				      	</div>
				      	<div class="form-group">
				            <label for="Email">Candidate Email</label>
				            <input type="text" class="form-control " Name="Email" placeholder="Enter Candidate Email here..." value="<?php echo e($candidate->can_email); ?>">
				      	</div>
				      	<div class="form-group">
				            <label for="Contact">Candidate Contact</label>
				            <input type="text" class="form-control " Name="Contact" placeholder="Enter Candidate Contact here..." value="<?php echo e($candidate->can_contact); ?>">
				      	</div>
				      	<div class="form-group">
				            <label for="Designantion">Candidate For Designantion</label>
				            <input type="text" class="form-control " Name="Designantion" placeholder="Enter Candidate Designantion here..." value="<?php echo e($candidate->can_designantion); ?>">
				      	</div>
				      	<div class="form-group" >
				            <label for="Type">Candidate Type</label>
				            <select class="form-control " id="type" name="Type">
				            	<option value=null  disabled="true" selected="true">-- Select Candidate Type --</option>
				            	<option value="Fresher" <?php echo e($candidate->can_experience == 0 ? 'selected' : ''); ?>>Fresher</option>
				            	<option value="Experienced" <?php echo e($candidate->can_experience > 0 ? 'selected' : ''); ?>>Experienced</option>
				            </select>
				      	</div>
				      	<div class="form-group" id="experience">
				            <label for="Experience">Candidate Experience (Years)</label>
				            <input type="text" class="form-control " Name="Experience" placeholder="Enter Candidate Experience here..." value="<?php echo e($candidate->can_experience); ?>">
				      	</div>
				    </div>

				     <div class="col-md-6 text-center  ">
				      	<h1 class="p-5 m-5 h-80  shadow-lg text-center rounded float-right m-3 bg-secondery"><i class="fa fa-user-plus text-dark fa-5x nav-icon p-5 m-4 "></i>
				      	</h1>
				     </div>
			  </div>
		      	
		</div>
		<div class="card-footer">
				<button type="submit" class="btn btn-primary">Edit Candidate</button>
	    </div>
	</div>
</form>
</div>

<template id="mytemplate">
  <input type="text" name="Manual_dates[]" class="datepicker bg-white m-2 col-md-5 form-control input-text full-width" placeholder="Departure Date"  readonly/>
</template>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>

<script type="text/javascript">
$(document).ready(function () {
	var exp = "<?php echo e($candidate->can_experience); ?>";
	if(exp == 0)
	{
		$("#experience").hide();
	}


	$("#type").change(function () {
		var type = $(this).val();

		if(type == "Experienced")
		{
			$("#experience").show();
		}
		else if(type == "Fresher")
		{
			$("#experience").hide();
		}
	});

});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/Recruit/EditCandidate.blade.php ENDPATH**/ ?>