
<?php $__env->startSection('content'); ?>
<!-- Main content -->
<div class="container-fluid p-5">
    <div class="card card-warning">
        <div class="card-header">
          		<h1 class="card-title text-xl text-dark" id="title"><i class="fa fa-money-check nav-icon"></i> My Leaves</h1>
        </div>
        <div class="card-body " style="overflow-x: auto;">
            <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php if(Auth::user()->Role == 2): ?>
                <a href="<?php echo e(route('CallApplyLeave',['mode' => 0])); ?>" class="btn btn-primary my-2">Apply</a>
            <?php endif; ?>
            <table class="table table-hover " id="MyLeave">
                <thead>
                    <th>Leave_Type</th>
                    <th>Reason</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Manual Dates</th>
                    <th>Total Leaves</th>
                    <th>Applied Date</th>
                    <th>Action Date</th>
                    <th>Status</th>
                    <th>Final Status</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>

</div>
          <!-- /.card-header -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
    <!-- Public/vendor/...  -->
    <script src="https://datatables.net/extensions/buttons/examples/html5/simple.html"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
     <!-- for buttons --> 

     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">

       
<script type="text/javascript">
  $(function () {
    var table = $('#MyLeave').removeAttr('width').DataTable({
        serverside :true,
        processing: true,
        scrollX: true,
        aaSorting: [],// to stop initial sorting 
        ajax: "<?php echo e(route('getMyLeaveData')); ?>",
        columnDefs: [
            { width: 100, targets: 2 },
            { width: 100, targets: 3 },
            { width: 100, targets: 4 },
            { width: 100, targets: 6 },
        ],
        columns: [
            {data: 'Leave_Type', name: 'Leave_Type'},
            {data: 'Reason', name: 'Reason'},            
            {data: 'Sdate', name: 'Sdate'},
            {data: 'Edate', name: 'Edate'},
            {data: 'Manual_dates', name: 'Manual_dates'},
            {data: 'Total_Leaves', name: 'Total_Leaves'},
            {data: 'Apply_date', name: 'Apply_date'},
            {data: 'Adate', name: 'Adate'},
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
            {
                data: 'status', 
                name: 'status', 
                orderable: true, 
                searchable: true
            },
        ],
        initComplete:function(){
                $.ajax({
                    url:"<?php echo e(route('SaveNotify')); ?>",
                });
        }
    });    
  });


          
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/Leave/MyLeave.blade.php ENDPATH**/ ?>