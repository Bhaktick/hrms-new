
<?php $__env->startSection('content'); ?>
<form method="Post" action="<?php echo e(route('HR.EditLastBal')); ?>">
    <?php echo csrf_field(); ?>
    <!-- Main content -->
    <div class="container-fluid p-5">
            <!-- SELECT2 EXAMPLE -->
        <div class="card card-warning">
            <div class="card-header">
              <h1 class="card-title text-xl text-dark"><i class="fa fa-edit nav-icon"></i> Edit Last Year Pending Leave</h1> 
            </div>
            <div class="card-body " style="overflow-x: auto;">
                <div class="row">
                    <div class="col-md-6 p-6 ">
                        <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                             <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($error); ?></li>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                        <?php endif; ?>
                        <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <div class="form-group">
                            <label for="Employee">Employee Name </label>
                            <select id="getEmployee"  class="form-control " style="min-height: 300px display: inline !important;" name="Employee">
                                <option value="null" selected="true" disabled>--- Select Employee --- </option>
                                <?php $__currentLoopData = $List; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ListItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($ListItem->id); ?>"><?php echo e($ListItem->name); ?> - #<?php echo e($ListItem->id); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>     
                        <div class="form-group">
                                <label for="Number">Last Year Leave Balance</label>
                                <input type="number" class="form-control  last-year"  value="0" Name="Number" disabled="true">
                        </div>  
                    </div>        
                    <div class="col-md-6 text-center ">
                        <h1 class="p-3 m-5 h-80  shadow-lg text-center rounded float-right m-3 bg-secondery"><i class="fa fa-edit text-dark fa-5x nav-icon p-5 m-4 "></i>
                        </h1>
                    </div>    
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-warning">Save Pending Leave</button>
            </div>
        </div>
    </div>
</form>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>
<!-- JS library for searching of employee Css is also there-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>  
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" type="text/css"> 
<script type="text/javascript">

      $(document).ready(function() {   
        $("#getEmployee").select2();  

        $('#getEmployee').on('change', function() {

            // alert('hey');
            var id=$("#getEmployee option:selected").val();
            $.ajax({
                    url: "<?php echo e(route('HR.getLastBalanceById')); ?>",
                    type: "GET",
                    data:{'id':id},
                    dataType: "json",
                    success:function(data) {
                        // alert(data);
                        $('.last-year').prop("disabled", false);// enabling
                        $('.last-year').val(data);// setting value


                    }
                });
        
          

        });

      });  

    
</script> 

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/Leave/LastLeaveBal.blade.php ENDPATH**/ ?>