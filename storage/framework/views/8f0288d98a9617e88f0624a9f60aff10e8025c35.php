
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.RadioCSS', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="container-fluid p-5">
        <div class="card card-warning">
            <div class="card-header">
                <h1 class="card-title text-xl text-dark"><i class="fa fa-user-check nav-icon"></i> Form to Select Candidate</h1>
            </div>
            <div class="card-body " style="overflow-x: auto;">
                 
                <div class="row p-3">
                    <div class=" card col-md-4 ">   
                        <form method="Post" action="<?php echo e(route('HR.EditSelected',['id' => $candidate->id])); ?>">
                            <h1>Joining Details </h1>
                            <?php echo csrf_field(); ?>
                            <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                 <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            <?php endif; ?>
                            <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php if($candidate->hr_status == 1): ?>
                                <div class="form-group">
                                    <label for="notice_period">Notice Period</label>
                                    <input type="text" name="notice_period" class="form-control" placeholder="Enter notice period in months" value="<?php echo e($candidate->notice_period); ?>"> 
                                </div>
                                <div class="form-group">
                                    <label for="Joining_Date">Joining Date</label>
                                    <input type="text" name="Joining_Date" class="datepicker form-control input-text full-width" placeholder="Departure Joining Date" value="<?php echo e($candidate->joining_date); ?>" />
                                </div>
                                <div class="form-group text-center p-2">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Save Action">
                                </div>
                                <?php endif; ?>
                            </form>
                    </div>
                    <div class="card col-md-8">
                        <table class="table table-white rounded m-2">
                            <tr>
                                <th>Candidate Name</th>
                                <td><?php echo e($candidate->can_name); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Email</th>
                                <td><?php echo e($candidate->can_email); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Contact</th>
                                <td><?php echo e($candidate->can_contact); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Designation</th>
                                <td><?php echo e($candidate->can_designantion); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Experience</th>
                                <td><?php echo e($candidate->can_experience); ?></td>
                            </tr>
                            <tr>
                                <th>Candidate Technical Status</th>
                                <td>
                                    <?php if($candidate->technical_status == 1): ?>
                                    <i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Selected'>
                                    <?php elseif($candidate->technical_status == 2): ?>
                                    <i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Candidate Parctical Status</th>
                                <td><?php if($candidate->practical_status == 1): ?>
                                    <i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Selected'>
                                    <?php elseif($candidate->practical_status == 2): ?>
                                    <i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'>
                                    <?php endif; ?></td>
                            </tr>
                            <tr>
                                <th>Candidate HR Status</th>
                                <td><?php if($candidate->hr_status == 1): ?>
                                    <i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Selected'>
                                    <?php elseif($candidate->hr_status == 2): ?>
                                    <i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Rejected'>
                                    <?php endif; ?></td>
                            </tr>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">
$(document).ready(function () {
    var today = new Date();
    $('.datepicker').datepicker({
        // to format and manage datepicker controller
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose:true,
        startDate: "today",
        daysOfWeekDisabled: '0',
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });





    

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/Recruit/EditSelected.blade.php ENDPATH**/ ?>