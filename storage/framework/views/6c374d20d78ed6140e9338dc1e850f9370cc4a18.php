
<?php $__env->startSection('content'); ?>

<div class="col-md-12 "> 
<form method="post" action="<?php echo e(route('admin.changePassword')); ?>">
		<?php echo csrf_field(); ?>
	<div class="container-fluid py-3 col-md-10">
		<!-- /.row -->
		<div class="row py-3">
		    <div class="col-md-12">
		        <div class="card card-default">
		          	<div class="card-header">
		            	<h3 class="card-title text-xl">Reset Password</h3>
		          	</div>
			        <div class="card-body py3">
			        	<?php if($errors->any()): ?>
							    <div class="alert alert-danger">
							      <strong>Whoops!</strong> There were some problems with your input.<br><br>
							        <ul>
								     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								        <li><?php echo e($error); ?></li>
								      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							        </ul>
							    </div>
						<?php endif; ?>
			        	<?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			            <div class="bs-stepper">
			              	<div class="bs-stepper-content">
			                <!-- your steps content here -->
				                <div id="logins-part" class="content" role="tabpanel" aria-labelledby="logins-part-trigger">
					                <div class="form-group">
					                    <label for="exampleInputEmail1">Old Password</label>
					                    <input type="password" name="old_password" class="form-control">
					                </div>
					                <div class="form-group">
					                    <label for="exampleInputPassword1">Password</label>
					                     <input type="password" name="password" class="form-control">
					                </div>
					                <div class="form-group">
					                    <label for="exampleInputPassword1"> Confirm Password</label>
					                     <input type="password" name="confirm" class="form-control">
					                </div>
					                <button type="submit" class="btn-lg btn-primary">change password</button>
				                </div>
				              
			              	</div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>	
	</div>	

</form>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/data/change.blade.php ENDPATH**/ ?>