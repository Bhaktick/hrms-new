
<?php $__env->startSection('content'); ?>
<!-- Main content -->
<div class="container-fluid p-5">    
  <div class="card">
        <div class="card-header">
            <h1 class="card-title text-xl text-dark"> Import EMPLOYEE DATA</h1>
        </div>
        <div class="card-body">
          <!-- Form -->
            <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

             <form method='post' action="<?php echo e(route('HR.ImportEmp')); ?>" enctype='multipart/form-data' >
               <?php echo e(csrf_field()); ?>

               <input type='file' name='file' required="true">
               <input type='submit' name='submit' value='Import'>
             </form>
        </div>
   </div>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/ImportEmp.blade.php ENDPATH**/ ?>