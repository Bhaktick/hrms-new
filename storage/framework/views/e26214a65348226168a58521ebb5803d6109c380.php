<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      	<b>Version</b> 0.0.1-CK
    </div>
    <strong>Copyright &copy; 2020 HRMS.</strong> All rights reserved by <strong class="text-lg">CoderKube Technologies</strong>.
</footer>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/includes/footer.blade.php ENDPATH**/ ?>