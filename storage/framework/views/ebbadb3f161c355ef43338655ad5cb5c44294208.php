
<?php $__env->startSection('content'); ?>

<div class="container-fluid">
<!-- /.row -->
		<div class="row py-5 mx-3">
			<div class="col-md-4">
		        <div class="card card-default ">
		          	<div class="card-header">
		            	<h3 class="card-title text-xl">Your Profile</h3>
		          	</div>
			        <div class="card-body text-center">
			            <div class="bs-stepper">
			              	<div class="bs-stepper-content">

				                <div id="logins-part" class="content" role="tabpanel" aria-labelledby="logins-part-trigger">
					                <div class="form-group">
					                	<img src="<?php echo e(asset('adminlte/dist/img/AdminLTELogo.png')); ?>" class="img-thumbnail img-fluid rounded"><br/>
					                    <label for="exampleInputEmail1"><?php echo e($profile->name); ?></label>  
					                </div>
					                <div class="form-group">
					                    <label for="exampleInputPassword1"><?php echo e($profile->Designation); ?></label>
					                </div>			                
				                </div>
				                <div class="card">
				                	<div class="card-header">
				                		<h3 class="card-title">Your  Social Profile</h3>
				                	</div>
				                	<div class="card-body">
				                		<label for="exampleInputEmail1"><i class="fa fa-skype text-dark"></i>Skype ::</label>
				                		<?php echo e($profile->SkypeID); ?>

				                	</div>
				                	<div class="card-body">
				                		<label for="exampleInputEmail1"><i class="fa fa-skype"></i>TeamLogger ::</label>
				                		<?php echo e($profile->TeamloggerID); ?>

				                	</div>
				                	
				                </div>
			              	</div>
			            </div>
			        </div>
		        </div>
		    </div>

		    <div class="col-md-8">
		        <div class="card card-default ">
		          	<div class="card-header">
		            	<h3 class="card-title">Your Profile</h3>
		          	</div>
			        <div class="card-body ">
			            <div class="bs-stepper">
			              	<div class="bs-stepper-content">

				                <div id="logins-part" class="content" role="tabpanel" aria-labelledby="logins-part-trigger">
					                <div class="form-group">
					                    <label for="exampleInputEmail1">NAME :: </label>
		  								<?php echo e($profile->name); ?>

					               	</div>
					                <div class="form-group">
					                    <label for="exampleInputPassword1">Designation :: </label>
					                     <?php echo e($profile->Designation); ?>

					                </div>	
					                <div class="form-group">
					                    <label for="exampleInputEmail1">DOJ :: </label>
					                    <?php echo e($profile->DOJ); ?>

					                </div>
					                <div class="form-group">
					                    <label for="exampleInputPassword1">Education :: </label>
					                     <?php echo e($profile->Education); ?>

					                </div>	
					                <div class="form-group">
					                    <label for="exampleInputEmail1">Course :: </label>
					                    <?php echo e($profile->Course); ?>

					                </div>
					                <div class="form-group">
					                    <label for="exampleInputPassword1">Experience :: </label>
					                     <?php echo e($profile->Experience); ?>

					                </div>	

				                </div>
				              
			              	</div>
			            </div>
			        </div>
		        </div>
		    </div>


		</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/data/profile.blade.php ENDPATH**/ ?>