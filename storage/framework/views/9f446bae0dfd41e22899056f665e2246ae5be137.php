
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.RadioCSS', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="container-fluid p-5">

   <div class="card card-info ">
          <div class="card-header ">
            <h1 class="card-title text-xl">
              <i class="fas fa-plus"></i>
                Edit Employee                </h1>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-right-tabContent">
                  <div class="tab-pane fade show " id="vert-tabs-right-Primary" role="tabpanel" aria-labelledby="vert-tabs-right-Primary-tab">
                   <?php echo $__env->make('HR.EditEmp.EditPrimary', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                  </div>

                  <div class="tab-pane fade show active" id="vert-tabs-right-Personal2" role="tabpanel" aria-labelledby="vert-tabs-right-Personal2-tab">
                   <?php echo $__env->make('HR.EditEmp.EditPersonal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                  </div>

                  <div class="tab-pane fade" id="vert-tabs-right-Social" role="tabpanel" aria-labelledby="vert-tabs-right-Social-tab">
                   <?php echo $__env->make('HR.EditEmp.EditSocial', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                  </div>

                  <div class="tab-pane fade" id="vert-tabs-right-Formal" role="tabpanel" aria-labelledby="vert-tabs-right-Formal-tab">
                   <?php echo $__env->make('HR.EditEmp.EditFormal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                  </div>

                  <div class="tab-pane fade" id="vert-tabs-right-Bank" role="tabpanel" aria-labelledby="vert-tabs-right-Bank-tab">
                   <?php echo $__env->make('HR.EditEmp.EditBank', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                  </div>

                  <div class="tab-pane fade" id="vert-tabs-right-Salary" role="tabpanel" aria-labelledby="vert-tabs-right-Salary-tab">
                   <?php echo $__env->make('HR.EditEmp.EditSalary', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                  </div>

                </div>
              </div>
              <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs nav-tabs-right h-100" id="vert-tabs-right-tab" role="tablist" aria-orientation="vertical">
                  <a class="nav-link " id="vert-tabs-right-Primary-tab" data-toggle="pill" href="#vert-tabs-right-Primary" role="tab" aria-controls="vert-tabs-right-Primary" aria-selected="true">Primary Details</a>
                  <a class="nav-link active" id="vert-tabs-right-Personal2-tab" data-toggle="pill" href="#vert-tabs-right-Personal2" role="tab" aria-controls="vert-tabs-right-Personal2" aria-selected="false">Personal Details</a>
                  <a class="nav-link" id="vert-tabs-right-Social-tab" data-toggle="pill" href="#vert-tabs-right-Social" role="tab" aria-controls="vert-tabs-right-Social" aria-selected="false">Social Details</a>
                  <a class="nav-link" id="vert-tabs-right-Formal-tab" data-toggle="pill" href="#vert-tabs-right-Formal" role="tab" aria-controls="vert-tabs-right-Formal" aria-selected="false">Formal Details</a>
                  <a class="nav-link" id="vert-tabs-right-Bank-tab" data-toggle="pill" href="#vert-tabs-right-Bank" role="tab" aria-controls="vert-tabs-right-Bank" aria-selected="false">Bank Details</a>
                  <a class="nav-link" id="vert-tabs-right-Salary-tab" data-toggle="pill" href="#vert-tabs-right-Salary" role="tab" aria-controls="vert-tabs-right-Salary" aria-selected="false">Salary Details</a>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card -->
        </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/EditEmp/EditEmp.blade.php ENDPATH**/ ?>