
<?php $__env->startSection('content'); ?>
    <style type="text/css">
    [type=radio] { 
      position: absolute;
      opacity: 0;
      width: 0;
      height: 0;
    }

    /* IMAGE STYLES */
    [type=radio] + i {
      cursor: pointer;
    }

    /* CHECKED STYLES */
    [type=radio]:checked + i {
      margin: 3px;
      border: 4px solid;
      border-radius: 15px;
      animation: blink 1s;
      animation-iteration-count: 3;
    }
    @keyframes  blink { 50% { border-color:#333 ; }  }


</style>
<div class="container-fluid p-5">
        <div class="card card-warning">
            <div class="card-header">
                <h1 class="text-dark">Leave Application of <?php echo e($Emp->name); ?> </h1>
            </div>
            <div class="card-body " style="overflow-x: auto;">
                 
                <div class="row p-3">
                    <div class=" card col-md-4 ">   
                        <form method="Post" action="<?php echo e(route('HR.LeaveAction',['id' => $Leave->id])); ?>">
                            <?php echo csrf_field(); ?>
                            <h1>Form to Take Action</h1>
                            <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                 <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            <?php endif; ?>
                            <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php if($Leave->status == 0 && $Leave->final_status != 1): ?>
                                <div class="form-group">
                                    <label for="remark">Remark</label>
                                    <textarea class="form-control" Name="remark"></textarea>
                                </div>
                                <div class="form-check">
                                    <div class="form-group">
                                    <label>Please select Leave is Approved or Not</label>
                                    </div>
                                    
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=1> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Approved Leave'> Approve</i> 
                                    </label>
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=2> <i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Rejected Leave'> Reject</i> 
                                    </label>
                                </div>
                                <div class="form-group text-center p-2">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Save Action">
                                </div>
                            <?php else: ?>
                            <h2 class="bg-info m-3 p-2 rounded">Action Already Taken !!!</h2>
                            <?php endif; ?>
                        </form>
                    </div>
                    <div class="card col-md-8">
                        <table class="table table-white rounded m-2">
                            <tr>
                                <th>Employee Name</th>
                                <td><?php echo e($Emp->name); ?></td>
                            </tr>
                            <tr>
                                <th>Leave_Type</th>
                                <td><?php echo e($Leave->Leave_Type); ?></td>
                            </tr>
                            <tr>
                                <th>Reason</th>
                                <td><?php echo e($Leave->Reason); ?></td>
                            </tr>
                            <?php if($Leave->Leave_Type != 4 && $Leave->Leave_Type != 5): ?>
                            <tr>
                                <th>Date</th>
                                <td><?php echo e($Leave->start_date); ?></td>
                            </tr>
                            <?php endif; ?>
                            <?php if($Leave->Leave_Type == 5): ?>
                            <tr>
                                <th>Start Date</th>
                                <td><?php echo e($Leave->start_date); ?></td>
                            </tr>
                            <tr>
                                 <th>End Date</th>
                                 <td><?php echo e($Leave->end_date); ?></td>
                            </tr>
                            <?php endif; ?>
                            <?php if($Leave->Leave_Type == 4): ?>
                            <tr>
                                <th>Manual Dates</th>
                                <td><?php echo e($Leave->Manual_dates); ?></td>
                            </tr>
                            <?php endif; ?>
                            <tr>
                                <th>Total Leaves</th>
                                <td><?php echo e($Leave->Total_Leaves); ?></td>
                            </tr>
                            <tr>
                                <th>Applied Date</th>
                                <td><?php echo e($Leave->applied_date); ?></td>
                            </tr>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/Leave/LeaveModel.blade.php ENDPATH**/ ?>