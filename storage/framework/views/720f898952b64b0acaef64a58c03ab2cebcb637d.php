
<?php $__env->startSection('content'); ?>

<div class="container-fluid p-5">
<!-- /.row -->
	<form method="Post" class="formMode" action="<?php echo e(route('HR.AddCandidate',['code' => 0])); ?>">

<!-- do not change name of form connected with Jquery -->
	<div class=" card card-primary">
		<div class="card-header"> 
				<h1 class="card-title text-xl"><i class="fa fa-user-plus nav-icon"></i> Add Candidate</h1>
		</div>
		<div class="card-body ">
			<div class="row">
				<div class="col-md-6 pl-6 ">
		        	<?php if($errors->any()): ?>
		    	    <div class="alert alert-danger">
		    	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
		    	        <ul>
		    		     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		    		        <li><?php echo e($error); ?></li>
		    		      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		    	        </ul>
		    	    </div>
		    		<?php endif; ?>

		    		<?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

						<?php echo csrf_field(); ?>		      	
				      	<div class="form-group">
				            <label for="Name">Candidate Name</label>
				            <input type="text" class="form-control " Name="Name" placeholder="Enter Candidate Name here..." value="<?php echo e(old('Name')); ?>">
				      	</div>
				      	<div class="form-group">
				            <label for="Email">Candidate Email</label>
				            <input type="text" class="form-control " Name="Email" placeholder="Enter Candidate Email here..." value="<?php echo e(old('Email')); ?>">
				      	</div>
				      	<div class="form-group">
				            <label for="Contact">Candidate Contact</label>
				            <input type="text" class="form-control " Name="Contact" placeholder="Enter Candidate Contact here..." value="<?php echo e(old('Contact')); ?>">
				      	</div>
				      	<div class="form-group">
				            <label for="Designantion">Candidate For Designantion</label>
				            <input type="text" class="form-control " Name="Designantion" placeholder="Enter Candidate Designantion here..." value="<?php echo e(old('Designantion')); ?>">
				      	</div>
				      	<div class="form-group" >
				            <label for="Type">Candidate Type</label>
				            <select class="form-control " id="type" name="Type">
				            	<option value=null  disabled="true" selected="true">-- Select Candidate Type --</option>
				            	<option value="Fresher">Fresher</option>
				            	<option value="Experienced">Experienced</option>
				            </select>
				      	</div>
				      	<div class="form-group" id="experience">
				            <label for="Experience">Candidate Experience (Years)</label>
				            <input type="text" class="form-control " Name="Experience" placeholder="Enter Candidate Experience here..." >
				      	</div>
				    </div>

				     <div class="col-md-6 text-center ">
				      	<h1 class="p-3 m-5 h-80  shadow-lg text-center rounded float-right m-3 bg-secondery"><i class="fa fa-user-plus text-dark fa-5x nav-icon p-5 m-4 "></i>
				      	</h1>
				     </div>
			</div>
		</div>
		<div class="card-footer">
				<button type="submit" class="btn btn-primary">Add Candidate</button>
	    </div>
	</div>
</form>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>

<script type="text/javascript">
$(document).ready(function () {
	$("#experience").hide();


	$("#type").change(function () {
		var type = $(this).val();

		if(type == "Experienced")
		{
			$("#experience").show();
		}
		else if(type == "Fresher")
		{
			$("#experience").hide();
		}
	});

});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/Recruit/AddCandidate.blade.php ENDPATH**/ ?>