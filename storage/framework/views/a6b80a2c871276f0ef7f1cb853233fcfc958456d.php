
<?php $__env->startSection('content'); ?>
<!-- Main content -->
<div class="container-fluid p-5">
    <div class="card card-warning">
        <div class="card-header">
                <h1 class="card-title text-xl text-dark" id="title"><i class="fa fa-tasks nav-icon"></i> Candidate Data</h1>
        </div>
        <div class="card-body " style="overflow-x: auto;">
            <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <table class="table table-hover " id="user">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Designation</th>
                    <th>Experience</th>
                    <th>Techincal Interviewer</th>
                    <th>Techincal Type</th>
                    <th>Techincal Date</th>
                    <th>Techincal Time</th>
                    <th>Techincal Status</th>
                    <th>Practical Interviewer</th>
                    <th>Practical Type</th>
                    <th>Practical Date</th>
                    <th>Practical Time</th>
                    <th>Practical Status</th>
                    <th>HR Status</th>
                    <th>Remark</th>
                    <th>Edit HR Status</th>
                    <th>Delete Candidate</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- Datatables JS CDN -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
    <!-- Public/vendor/...  -->
    <script src="https://datatables.net/extensions/buttons/examples/html5/simple.html"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
     <!-- for buttons --> 

     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
     <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
     <script type="text/javascript" href="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">


<script type="text/javascript">
  $(function () {
    var table = $('#user').DataTable({
        serverside :true,
        processing: true,
        aaSorting: [],// to stop initial sorting 
        ajax: "<?php echo e(route('getCandidateStatus')); ?>",
        columns: [
            {data: 'can_name', name: 'can_name'},
            {data: 'can_email', name: 'can_email'},
            {data: 'can_contact', name: 'can_contact'},
            {data: 'can_designantion', name: 'can_designantion'},
            {data: 'can_experience', name: 'can_experience'},
            {data: 'Technical', name: 'Technical'},
            {data: 'Ttype', name: 'Ttype'},
            {data: 'Tdate', name: 'Tdate'},
            {data: 'interview_time', name: 'interview_time'},
            {data: 'Tstatus', name: 'Tstatus'},
            {data: 'Practical', name: 'Practical'},
            {data: 'Ptype', name: 'Ptype'},
            {data: 'Pdate', name: 'Pdate'},
            {data: 'practical_time', name: 'practical_time'},
            {data: 'Pstatus', name: 'Pstatus'},
            {data: 'Hstatus', name: 'Hstatus'},
            {data: 'remark', name: 'remark'},
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
            {
                data: 'delete', 
                name: 'delete', 
                orderable: true, 
                searchable: true
            },
        ],
        
    });    
  });


          
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/Recruit/CandidateStatus.blade.php ENDPATH**/ ?>