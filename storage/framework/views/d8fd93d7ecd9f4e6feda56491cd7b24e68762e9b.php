
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.RadioCSS', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="container-fluid p-5">
        <div class="card card-warning">
            <div class="card-header">
                <h1 class="card-title text-xl text-dark">Form to Exit Employees</h1>
            </div>
            <div class="card-body " style="overflow-x: auto;">
                 
                <div class="row p-3">
                    <div class=" card col-md-4 ">   
                        <form method="Post" action="<?php echo e(route('HR.SaveEmpStatus',['id' => $data->id])); ?>">
                            <?php echo csrf_field(); ?>
                            <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                 <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            <?php endif; ?>
                            <?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php if($data->Status != null): ?>
                                
                                <div class="form-check">
                                    <div class="form-group">
                                        <label for="relieving_date">Relieving  Date</label>
                                        <input type="text" name="relieving_date" class="datepicker form-control input-text full-width" placeholder="Departure Date" value="<?php echo e($data->relieving_date); ?>" />
                                    </div>
                                    <div class="form-group">
                                    <label>Please Select Employee is Exited or Not</label>
                                    </div>
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=1 <?php echo e(($data->Status == 1) ? 'checked' : ''); ?>> <i class='btn fas fa-check-circle text-success text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Approved Leave' > Active</i> 
                                    </label>
                                    <label>
                                        <input type="radio" class="form-check-input" Name="status" value=2 <?php echo e(($data->Status == 2) ? 'checked' : ''); ?>><i class='btn fas fa-times-circle text-danger text-lg' data-toggle='tooltip' style='cursor: pointer;' title='Rejected Leave'  > Exit</i> 
                                    </label>
                                </div>
                                <div class="form-group text-center p-2">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Save Action">
                                </div>
                                <?php endif; ?>
                        </form>
                    </div>
                    <div class="card col-md-8">
                        <table class="table table-white rounded m-2">
                            <tr>
                                <th>Employee Name</th>
                                <td><?php echo e($data->name); ?></td>
                            </tr>
                            <tr>
                                <th>Employee Email</th>
                                <td><?php echo e($data->email); ?></td>
                            </tr>
                            <tr>
                                <th>Employee Contact</th>
                                <td><?php echo e($data->Contact); ?></td>
                            </tr>
                            <tr>
                                <th>Employee Designation</th>
                                <td><?php echo e($data->Designation); ?></td>
                            </tr>
                            <tr>
                                <th>Employee Experience</th>
                                <td><?php echo e($data->Experience); ?></td>
                            </tr>
                            <tr>
                                <th>Employee Joining Date</th>
                                <td><?php echo e($data->DOJ); ?></td>
                            </tr>
                            <tr>
                                <th>Employee Status</th>
                                <td>
                                    <?php if($data->Status == 1): ?>
                                    <i class='fas fa-lg fa-check-circle m-2 text-success' data-toggle='tooltip' title='Active'>
                                    <?php elseif($data->Status == 2): ?>
                                    <i class='fas fa-lg fa-times-circle m-2 text-danger' data-toggle='tooltip' title='Exit'>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


  <!-- timepicker links here  -->

  <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.min.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.16/jquery.timepicker.js"></script>


<script type="text/javascript">
$(document).ready(function () {
    var today = new Date();
    $('.datepicker').datepicker({
        // to format and manage datepicker controller
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose:true,
        daysOfWeekDisabled: '0',
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });





    

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/EditEmp/ExitEmpForm.blade.php ENDPATH**/ ?>