
<?php $__env->startSection('content'); ?>
<!-- Main content -->
<div class="container-fluid p-5">
        <!-- SELECT2 EXAMPLE -->
    <div class="card card-warning">
        <div class="card-header">
        	<h1 class="text-dark">Add Paid Leave</h1> 
        </div>
        <div class="card-body " style="overflow-x: auto;">
            <?php if($errors->any()): ?>
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                         <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
            <?php endif; ?>
        	<table class="table table-white rounded">
                <tr>
                    <th>Employee Name</th>
                    <td>
                    	<select id="getEmployee" name="Employee">
                    		<option value="null" selected="true" disabled>--- Select Employee --- </option>
						    <?php $__currentLoopData = $List; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ListItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
   				 		        <option value="<?php echo e($ListItem->id); ?>"><?php echo e($ListItem->name); ?> - #<?php echo e($ListItem->id); ?></option>
    		      			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					    </select>
					</td>
                </tr>
                <tr>
                    <th>Leave_Type</th>
                    <td></td>
                </tr>
                <tr>
                    <th>Reason</th>
                    <td></td>
                </tr>
                <tr>
                    <th>Date</th>
                    <td></td>
                </tr>
                <tr>
                    <th>Start Date</th>
                    <td></td>
                </tr>
                <tr>
                     <th>End Date</th>
                     <td></td>
                </tr>
                <tr>
                    <th>Manual Dates</th>
                    <td></td>
                </tr>
                <tr>
                    <th>Total Leaves</th>
                    <td></td>
                </tr>
                <tr>
                    <th>Applied Date</th>
                    <td></td>
                </tr>
            </table>
        	
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-warning">Add Leave</button>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_script'); ?>
<!-- JS library for searching of employee Css is also there-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>  
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" type="text/css"> 
<script type="text/javascript">

      $(document).ready(function() {   
        $("#getEmployee").select2();  
      });  

</script> 

 <?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/Leave/AddLeave.blade.php ENDPATH**/ ?>