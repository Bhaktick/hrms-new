<form method="POST" action="<?php echo e(route('HR.EditEmp',['id' => $data->id])); ?>">

  	<?php echo csrf_field(); ?>
    <?php if(Auth::user()->Role != 2): ?>
    <fieldset disabled="disabled">
    <?php endif; ?>
    <div class="card-body">
       <h1>Salary Details</h1>

    	<?php if($errors->any()): ?>
	    <div class="alert alert-danger">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
		     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		        <li><?php echo e($error); ?></li>
		      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	        </ul>
	    </div>
		<?php endif; ?>

		<?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <input type="hidden" name="form_name" value="Salary">
    <input type="hidden" name="form_id" value="<?php echo e($data->id); ?>">
      <div class="form-group">
        <label for="Experience"> Experience</label>
        <input type="text" class="form-control" Name="Experience" placeholder="Enter Experience" value="<?php echo e($data->Experience); ?>">
      </div>
      <div class="form-group">
        <label for="Inc_Time_Period"> Increment Time Period</label>
        <input type="Inc_Time_Period" class="form-control" Name="Inc_Time_Period" placeholder="Enter Increment Time Period" value="<?php echo e($data->Inc_Time_Period); ?>">
      </div>
      <div class="form-group">
        <label for="Last_Inc_Date"> Last Increment Date</label>
        <input type="text" name="Last_Inc_Date" class="datepicker form-control input-text full-width" placeholder="Enter Departure Date" value="<?php echo e($data->Last_Inc_Date); ?>" />
      </div>
      <div class="form-group">
        <label for="Last_Inc_Amount">Last Increment Amount </label>
        <input type="text" class="form-control" Name="Last_Inc_Amount" placeholder="Enter Last Increment Amount" value="<?php echo e($data->Last_Inc_Amount); ?>">
      </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </fieldset>
</form>
<?php $__env->startSection('page_script'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">
$(document).ready(function () {

        var today = new Date();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('.datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
</script>
<?php $__env->stopSection(); ?><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/EditEmp/EditSalary.blade.php ENDPATH**/ ?>