<form method="POST" action="<?php echo e(route('HR.EditEmp',['id' => $data->id])); ?>">

  	<?php echo csrf_field(); ?>
    <?php if(Auth::user()->Role != 2): ?>
    <fieldset disabled="disabled">
    <?php endif; ?>
    <div class="card-body">
           <h1>Social Details</h1>

    	<?php if($errors->any()): ?>
	    <div class="alert alert-danger">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
		     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		        <li><?php echo e($error); ?></li>
		      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	        </ul>
	    </div>
		<?php endif; ?>

		<?php echo $__env->make('layouts.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <input type="hidden" name="form_name" value="Social">
    <input type="hidden" name="form_id" value="<?php echo e($data->id); ?>">
      <div class="form-group">
        <label for="SkypeID">Skype ID </label>
        <input type="text" class="form-control" Name="SkypeID" placeholder="Enter Skype ID" value="<?php echo e($data->SkypeID); ?>">
      </div>
       <div class="form-group">
        <label for="Skype_password">Skype Password</label>
        <input type="text" class="form-control" Name="Skype_password" placeholder="Enter Skype Password" value="<?php echo e($data->Skype_password); ?>">
      </div>
      <div class="form-group">
        <label for="TeamloggerID">Teamlogger ID </label>
        <input type="text" class="form-control" Name="TeamloggerID" placeholder="Enter Teamlogger ID" value="<?php echo e($data->TeamloggerID); ?>">
      </div>
       <div class="form-group">
        <label for="Team_password">Teamlogger Password</label>
        <input type="text" class="form-control" Name="Team_password" placeholder="Enter Teamlogger Password" value="<?php echo e($data->Team_password); ?>">
      </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </fieldset>
</form><?php /**PATH D:\xampp\htdocs\hrms\resources\views/HR/EditEmp/EditSocial.blade.php ENDPATH**/ ?>