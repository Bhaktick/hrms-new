<!DOCTYPE html>
<html lang="en">
<head>
<?php echo $__env->make('layouts.includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent('css'); ?>

</head>
<body class="sidebar-mini">
<div>
	<?php if(Auth::user()->Role == 1): ?>
		<?php echo $__env->make('layouts.role.AdminSlidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php elseif(Auth::user()->Role == 2): ?>
		<?php echo $__env->make('layouts.role.HRSlidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php elseif(Auth::user()->Role == 3): ?>
		<?php echo $__env->make('layouts.role.EmpSlidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php endif; ?>
	 <div id="flash_message_div"></div>
	 <?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


	<div class="content-wrapper">
		<div class="text-lg">
		<?php echo $__env->yieldContent('content'); ?>

		</div>
	</div>

	  <?php echo $__env->make('layouts.includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	  
	  <?php echo $__env->yieldContent('page_script'); ?>

	  <script type="text/javascript">
	  	// for Message Blade
			    setTimeout(function() {
		    $('.alert').fadeOut("slow");
		     }, 8000);
	  </script>
</div>

</body>
</html><?php /**PATH D:\xampp\htdocs\hrms\resources\views/layouts/admin/app.blade.php ENDPATH**/ ?>